<?php
/**
 * @var $this View
 * @var $content string
 */

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@t01/dist');

use yii\helpers\Url;
use yii\web\View; ?>
<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="<?=Url::to(['site/index'])?>">
            <img alt="Logo" src="<?= $directoryAsset ?>/media/logos/logo-4-sm.png"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i>
        </button>
    </div>
</div>

<!-- end:: Header Mobile -->


