<?php

use common\models\DocumentType;
use frontend\models\DocumentTypeSearch;
use frontend\models\MissionSearch;
use yii\helpers\Url;
?>

<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here"
                data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="<?= Yii::$app->homeUrl ?>" class="kt-menu__link"><span class="kt-menu__link-text">หน้าหลัก</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="<?= Url::to(['cm-target-result/index']) ?>" class="kt-menu__link"><span class="kt-menu__link-text">ติดตามเยียวยา</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="<?= Url::to(['site/index-dashboard']) ?>" class="kt-menu__link"><span class="kt-menu__link-text">เอกสาร COVID-19</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel"
                data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="<?= Url::to(['meeting/index']) ?>" class="kt-menu__link"><span class="kt-menu__link-text">ติดตามประเด็น/ข้อสั่งการ</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
            </li>
            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a
                        href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">กลุ่มภารกิจ</span><i
                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                    <ul class="kt-menu__subnav">
                        <?php

                        foreach ($missionSearch as $search) {
                            $txtSubMenu = [];
                            /*foreach ($docTypeSearch as $docType) {
                                $subMenu[] = [
                                    'label'=>''.$docType->doctype_name,
                                    'id' => $search->mission_id,
                                    'url' => ['document/index', 'id' => $search->mission_id, 'type' => $docType->doctype_id]
                                ];

                                $url = Url::to(['document/index', 'id' => $search->mission_id, 'type' => $docType->doctype_id]);


                                $txtSubMenu[] =
                                        "<li class=\"kt-menu__item \" aria-haspopup=\"true\"><a href=\"{$url}\" class=\"kt-menu__link \">
                                            <i class=\"kt-menu__link-bullet kt-menu__link-bullet--dot\"><span></span></i><span              class=\"kt-menu__link-text\">{$docType->doctype_name}</span></a></li>"
                                ;
                            }*/

                            ?>
                            <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                <a href="<?=Url::to(['document/index','id'=>$search->mission_id])?>" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-chevron-circle-right"></i> </span><span
                                            class="kt-menu__link-text"><?= $search->mission_name ?></span><!--<i
                                            class="kt-menu__hor-arrow la la-angle-right"></i><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i>--></a>
                                <!--<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--right">
                                    <ul class="kt-menu__subnav">
                                        <?/*=implode('', $txtSubMenu); */?>
                                    </ul>
                                </div>-->
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </li>

        </ul>
    </div>
</div>

