<?php
/**
 * @var $this View
 * @var $content string
 */

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@t01/dist');

use frontend\models\DocumentTypeSearch;
use frontend\models\MissionSearch;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$mission = new MissionSearch();
$missionSearch = $mission->search([])->getModels();

$docType = new DocumentTypeSearch();
$docTypeSearch = $docType->search([])->getModels();

$user = Yii::$app->session->get('user_session');
?>


<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                <div class="kt-container ">

                    <!-- begin:: Brand -->
                    <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                        <a class="kt-header__brand-logo" href="?page=index">
                            <img alt="Logo" src="<?= $directoryAsset ?>/media/logos/logo-4.png" class="kt-header__brand-logo-default"/>
                            <img alt="Logo" src="<?= $directoryAsset ?>/media/logos/logo-4-sm.png" class="kt-header__brand-logo-sticky"/>
                        </a>
                    </div>

                    <!-- end:: Brand -->

                    <!-- begin: Header Menu -->

                    <?= $this->render('main_menu', ['missionSearch'=>$missionSearch,'docTypeSearch'=>$docTypeSearch]) ?>
                    <!-- end: Header Menu -->

                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar kt-grid__item">

                        <!--begin: Search -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										<span class="kt-header__topbar-icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                                 version="1.1" class="kt-svg-icon">
												<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
													<rect x="0" y="0" width="24" height="24"/>
													<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                          fill="#000000" fill-rule="nonzero" opacity="0.3"/>
													<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                          fill="#000000" fill-rule="nonzero"/>
												</g>
											</svg>

                                            <!--<i class="flaticon2-search-1"></i>-->
										</span>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact"
                                     id="kt_quick_search_dropdown">
                                    <form method="get" class="kt-quick-search__form">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i
                                                            class="flaticon2-search-1"></i></span></div>
                                            <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                                            <div class="input-group-append"><span class="input-group-text"><i
                                                            class="la la-close kt-quick-search__close"></i></span></div>
                                        </div>
                                    </form>
                                    <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325"
                                         data-mobile-height="200">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end: Search -->

                        <!--begin: Notifications -->

                        <!--end: Notifications -->

                        <!--begin: Quick actions -->

                        <!--end: Quick actions -->

                        <!--begin: Language bar -->

                        <!--end: Language bar -->

                        <!--begin: User bar -->

                        <?php
                        if(is_null($user) && (!Yii::$app->user->isGuest)){
                            $userm = Yii::$app->user->identity;
                            Yii::$app->session->set('user_session', [
                                'username' => $userm->username,
                                'fname' => $userm->firstname,
                                'lname' => $userm->lastname,
                                'mission_id' => $userm->mission_id,
                            ]);
                            $user = Yii::$app->session->get('user_session');
                        }
                        ?>

                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-welcome">สวัสดี,</span>
                                <span class="kt-header__topbar-username"><?= (Yii::$app->user->isGuest) ? "Guest" : $user['fname']; ?></span>
                                <span class="kt-header__topbar-icon"><b><?= (Yii::$app->user->isGuest) ? "G" : mb_substr($user['fname'],0,1);?></b></span>
                                <img alt="Pic" src="<?= $directoryAsset ?>/media/users/300_21.jpg" class="kt-hidden"/>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                <?php
                                if (!Yii::$app->user->isGuest) {
                                    ?>
                                    <!--begin: Head -->
                                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                         style="background-image: url(<?= $directoryAsset ?>/media/misc/bg-1.jpg)">
                                        <div class="kt-user-card__avatar">
                                            <img class="kt-hidden" alt="Pic" src="<?= $directoryAsset ?>/media/users/300_25.jpg"/>

                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php
                                                echo $user['fname'].' '.$user['lname'];
                                                ?></span>
                                        </div>
                                        <div class="kt-user-card__name">

                                        </div>
                                        <!--<div class="kt-user-card__badge">
                                            <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
                                        </div>-->
                                    </div>

                                    <!--end: Head -->

                                    <!--begin: Navigation -->
                                    <div class="kt-notification">
                                        <a href="custom/apps/user/profile-1/personal-information.html" class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    ข้อมูลส่วนตัว
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    ตั้งค่า
                                                </div>
                                            </div>
                                        </a>
                                        <a href="custom/apps/user/profile-3.html" class="kt-notification__item">
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-mail kt-font-warning"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    ข้อความ
                                                </div>
                                            </div>
                                        </a>

                                        <div class="kt-notification__custom kt-space-between">
                                            <a href="<?= Url::to(['site/logout',]) ?>" data-method="post"
                                               class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>

                                        </div>
                                    </div>

                                    <!--end: Navigation -->
                                    <?php
                                } else {
                                    ?>
                                    <!--begin: Head -->
                                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                         style="background-image: url(<?= $directoryAsset ?>/media/misc/bg-1.jpg)">
                                        <div class="kt-user-card__avatar">
                                            <img class="kt-hidden" alt="Pic" src="<?= $directoryAsset ?>/media/users/300_25.jpg"/>

                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">G</span>
                                        </div>
                                        <div class="kt-user-card__name">
                                            Guest
                                        </div>

                                    </div>

                                    <!--end: Head -->

                                    <!--begin: Navigation -->
                                    <div class="kt-notification">
                                        <div class="kt-notification__custom kt-space-between">
                                            <a href="<?= Url::to(['site/login',]) ?>" data-method="post"
                                               class="btn btn-label btn-label-brand btn-sm btn-bold">Sign In</a>

                                            <a href="<?= Url::to(['site/login',]) ?>" data-method="post"
                                               class="btn btn-label btn-label-brand btn-sm btn-bold">ลงทะเบียน</a>

                                        </div>
                                    </div>

                                    <!--end: Navigation -->
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <!--end: User bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>
            </div>

            <!-- end:: Header -->
            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <!-- begin:: Subheader -->


                    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                        <div class="kt-container ">
                            <div class="kt-subheader__main">
                                <h3 class="kt-subheader__title"><?= $this->title ?> </h3>

                                <?php
                                echo Breadcrumbs::widget([
                                    'homeLink' => [
                                        'label' => '<i class="flaticon2-shelter"></i>',
                                        'url' => '#',
                                        'class' => 'kt-subheader__breadcrumbs-home',
                                        'encode' => false,
                                    ],
                                    'links' => isset($this->params['breadcrumbs']) ?
                                        $this->params['breadcrumbs'] : [],
                                    'options' => [
                                        'class' => 'kt-subheader__breadcrumbs',
                                    ],
                                    'tag' => 'div',
                                    'itemTemplate' => "{link} <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n",
                                    //'activeItemTemplate' => ""
                                ]);
                                ?>

                                <!--<div class="kt-subheader__breadcrumbs">
                                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="" class="kt-subheader__breadcrumbs-link">
                                        Dashboard </a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="" class="kt-subheader__breadcrumbs-link">
                                        Default Dashboard </a>
                                </div>-->
                            </div>

                            <?php
                            if (!Yii::$app->user->isGuest) :
                                ?>
                                <div class="kt-subheader__toolbar">
                                    <div class="kt-subheader__wrapper">
                                        <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="Quick actions"
                                             data-placement="top">
                                            <a href="#" class="btn btn-danger kt-subheader__btn-options" data-toggle="dropdown"
                                               aria-haspopup="true" aria-expanded="false">
                                                เพิ่มเอกสาร
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <?php foreach ($docTypeSearch as $docType) {
                                                    ?>
                                                    <a class="dropdown-item" href="<?=Url::to(['document/create','type'=>$docType->doctype_id,'mission'=>Yii::$app->user->identity->mission_id])?>"><i class="la la-plus"></i> <?=$docType->doctype_name?></a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>

                    <!-- end:: Subheader -->

                    <!-- begin:: Content -->


                    <!--Begin::Dashboard 3-->

                    <!--Begin::Row-->

                    <?= $content ?>

                    <!--End::Row-->

                    <!--End::Dashboard 3-->


                    <!-- end:: Content -->
                </div>
            </div>

            <?= $this->render('main_footer') ?>
        </div>
    </div>
</div>
