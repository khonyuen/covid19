<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

$this->beginContent('@t01/views/layouts/main_base.php');
?>

<?= $this->render('main_header') ?>

<?= $this->render('main_subheader',['content'=>$content]) ?>

<?= $this->render('main_scroll') ?>

<?php
    $this->endContent();
?>
