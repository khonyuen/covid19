<?php

/* @var $this \yii\web\View */

/* @var $content string */

use t01\assets;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

assets\MainAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@t01/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
          rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- begin::Body -->
<body style="background-image: url(<?= $directoryAsset ?>/media/demos/demo4/header.jpg); background-position: center top; background-size: 100% 350px;"
      class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">

<?= $content ?>

</body>

<?php
\yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal_main',
    //'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
//    'id' => 'modal-lov-dept',
//    'options' => [
//        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
//    ],
    'size' => Modal::SIZE_EXTRA_LARGE,
    'options' => [
        'max-width' => '98%',
        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
    ],
    //'header' => '',
    'class' => 'modal',
    'footer' => Html::button('<i class="fa fa-close"></i> ยกเลิก', ['data-dismiss' => "modal", 'class' => 'btn btn-danger lovClose']),
]);

echo "<div id='modalContent'></div>";
//echo "<div id='modalContent'>{$this->render("//department/index")}</div>";
//{$this->render("//department/_departmentLOV")}
?>

<?php
\yii\bootstrap4\Modal::end();
?>


<?php
\yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader2'],
    'id' => 'modal_main2',
    //'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
//    'id' => 'modal-lov-dept',
//    'options' => [
//        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
//    ],
    'size' => Modal::SIZE_EXTRA_LARGE,
    'options' => [
        'max-width' => '98%',
        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
        'style'=>'z-index:9999 !importance'
    ],
    //'header' => '',
    'class' => 'modal',
    'footer' => Html::button('<i class="fa fa-close"></i> ยกเลิก', ['data-dismiss' => "modal", 'class' => 'btn btn-danger lovClose']),
]);

echo "<div id='modalContent'></div>";
//echo "<div id='modalContent'>{$this->render("//department/index")}</div>";
//{$this->render("//department/_departmentLOV")}
?>

<?php
\yii\bootstrap4\Modal::end();
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
