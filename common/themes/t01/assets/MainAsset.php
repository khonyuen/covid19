<?php

namespace t01\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $sourcePath = '@t01/dist/';
    public $css = [
        'https://fonts.googleapis.com/css2?family=Trirong&display=swap',
        (YII_ENV_PROD) ? './minify/plugins/custom/fullcalendar/fullcalendar.bundle.css' : 'plugins/custom/fullcalendar/fullcalendar.bundle.css',
        (YII_ENV_PROD) ? './minify/plugins/global/plugins.bundle.min.css' : 'plugins/global/plugins.bundle.css',
        (YII_ENV_PROD) ? './minify/css/style.bundle.css' : 'css/style.bundle.css',
        ['media/logos/favicon.ico', 'rel' => 'shortcut icon'],
        (YII_ENV_PROD) ? './minify/css/custom/custom.css' : 'css/custom/custom.css',
    ];
    public $js = [
        //<!--begin::Global Theme Bundle(used by all pages) -->
        (YII_ENV_PROD) ? './minify/plugins/global/plugins.bundle.min.js' : 'plugins/global/plugins.bundle.min.js' ,
        (YII_ENV_PROD) ? './minify/js/scripts.bundle.min.js' : 'js/scripts.bundle.min.js',
        //<!--begin::Page Vendors(used by this page) -->
        //'plugins/custom/fullcalendar/fullcalendar.bundle.js',
        //'//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM',
        //'plugins/custom/gmaps/gmaps.js',
        //<!--begin::Page Scripts(used by this page) -->
        //'js/pages/dashboard.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}