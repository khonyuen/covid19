<?php

namespace t01\assets;

use yii\web\AssetBundle;

class AuthAsset extends AssetBundle
{
    public $sourcePath = '@t01/dist';
    public $css = [
        'https://fonts.googleapis.com/css2?family=Trirong&display=swap',
        'css/pages/login/login-2.css',
        'plugins/global/plugins.bundle.css',
        'css/style.bundle.css',
        ['media/logos/favicon.ico', 'rel' => 'shortcut icon'],
        //'css/custom/custom.css',
    ];
    public $js = [
        //<!--begin::Global Theme Bundle(used by all pages) -->
        'plugins/global/plugins.bundle.js',
        'js/scripts.bundle.js',
        'js/pages/custom/login/login-general.js'
        //<!--begin::Page Vendors(used by this page) -->
        //'plugins/custom/fullcalendar/fullcalendar.bundle.js',
        //'//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM',
        //'plugins/custom/gmaps/gmaps.js',
        //<!--begin::Page Scripts(used by this page) -->
        //'js/pages/dashboard.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapPluginAsset',
    ];
}