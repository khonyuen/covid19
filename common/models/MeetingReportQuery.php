<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MeetingReport]].
 *
 * @see MeetingReport
 */
class MeetingReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MeetingReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MeetingReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
