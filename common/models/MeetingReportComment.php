<?php

namespace common\models;

use Yii;
use \common\models\base\MeetingReportComment as BaseMeetingReportComment;

/**
 * This is the model class for table "meeting_report_comment".
 */
class MeetingReportComment extends BaseMeetingReportComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['report_id', 'created_by', 'updated_by'], 'integer'],
            [['comment_detail', 'comment_date', 'comment_name'], 'required'],
            [['comment_detail'], 'string'],
            [['comment_date', 'created_at', 'updated_at'], 'safe'],
            [['comment_name'], 'string', 'max' => 255]
        ]);
    }
	
}
