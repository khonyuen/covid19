<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "document_type".
 *
 * @property int $doctype_id
 * @property string $doctype_name ประเภทเอกสาร
 * @property string|null $doctype_icon ไอคอน
 *
 * @property Document[] $documents
 */
class DocumentType extends BaseActiveRecord
{
    public $tot_doc;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document_type';
    }

    public static function getList()
    {
        $list = self::find()->select(['doctype_name', 'doctype_id'])->all();
        return ArrayHelper::map($list, 'doctype_id', 'doctype_name');
    }

    /**
     * {@inheritdoc}
     * @return DocumentTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentTypeQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctype_name'], 'required'],
            [['doctype_name'], 'string', 'max' => 255],
            [['doctype_icon'], 'string', 'max' => 50],
            ['tot_doc', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'doctype_id' => 'Doctype ID',
            'doctype_name' => 'ประเภทเอกสาร',
            'doctype_icon' => 'ไอคอน',
            'tot_doc' => 'ประเภทเอกสาร',
        ];
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery|DocumentQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['document_type' => 'doctype_id']);
    }
}
