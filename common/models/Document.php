<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * This is the model class for table "documents".
 *
 * @property int $document_id
 * @property string $title
 * @property string|null $description
 * @property int $document_type
 * @property string|null $tags
 * @property int|null $status
 * @property int|null $has_thumbnail
 * @property string|null $document_name
 * @property int $mission_id
 * @property int|null $created_by
 * @property int|null $created_at
 * @property int|null $updated_by
 * @property int|null $updated_at
 * @property string $files_path
 * @property string $files
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property DocumentType $documentType
 * @property Mission $mission
 */
class Document extends BaseActiveRecord
{
    const UPLOAD_FOLDER = 'uploads';
    const UPLOAD_PRJ_FILE = 'documents';

    const pdf = 'pdf.svg';
    const jpg = 'jpg.svg';
    const media = 'media.svg';
    const doc = 'doc.svg';
    const zip = 'zip.svg';

    public $tot_doc;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'document';
    }

    public static function getUploadPath($field = 'files')
    {
        if ($field == 'files') {
            return Yii::getAlias('@webroot') . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_PRJ_FILE . '/';// . self::UPLOAD_AREA_FOLDER . '/';
        }
    }

    public static function icon(){
        return [
            'css'=>'css.svg',
            'csv'=>'csv.svg',
            'doc'=>'doc.svg',
            'html'=>'html.svg',
            'js'=>'javascript.svg',
            'jpg'=>'jpg.svg',
            'media'=>'mp4.svg',
            'pdf'=>'pdf.svg',
            'xml'=>'xml.svg',
            'zip'=>'zip.svg',
        ];
    }

    public static function getIcon($i){
        $icon = self::icon();
        return isset($icon[$i]) ? $icon[$i] : "";
    }

    /**
     * {@inheritdoc}
     * @return DocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'document_type', 'status', 'files_path',], 'required'],
            [['description', 'files_path','created_at','updated_at'], 'string'],
            [['document_type', 'status', 'mission_id', 'created_by',  'updated_by', ], 'integer'],
            [['title', 'tags', 'document_name','has_thumbnail'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['document_type'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_type' => 'doctype_id']],
            [['files'], 'file', 'extensions' => ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'png', 'jpg', 'jpeg','rar','zip'], 'maxFiles' => 20,'skipOnEmpty' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'title' => 'เรื่อง',
            'description' => 'รายละเอียด',
            'document_type' => 'ประเภทเอกสาร',
            'tags' => 'Tags',
            'status' => 'สถานะ',
            'has_thumbnail' => 'Has Thumbnail',
            'document_name' => 'ชื่อเอกสาร',
            'mission_id' => 'กลุ่มภารกิจ',
            'created_by' => 'ผู้บันทึก',
            'created_at' => 'วันที่บันทึก',
            'updated_by' => 'ผู้แก้ไข',
            'updated_at' => 'วันที่แก้ไข',
            'files_path' => 'ตำแหน่งเอกสาร',
            'files' => 'เอกสาร',

            'documentTypeName' => 'เรื่อง',
            'documentTypeIcon' => 'ประเภทไฟล์',
            'documentTypeTitle' => 'ประเภท',
            'missionName' => 'กล่องภารกิจ',
            'statusDoc' => 'สถานะ',
            'createBy'=>'ผู้บันทึก',
            'tot_doc'=>'จำนวนเอกสาร'
        ];
    }

    /**
     * @param $data
     * @param $field
     * @param string $type
     * @return array
     * แสดงข้อมูลรูปภาพ
     * data ฟิวที่เก็บ json รูปภาพ
     * field ฟิวที่เก้บตำแหน่งรูป
     * type ประเภทของข้อมูลที่จะแสดง
     *
     */
    public function initialPreview($data, $field, $type = 'files')
    {
        $initial = [];
        $files = Json::decode($data);
        //print_r($files);
        if (is_array($files)) {
            foreach ($files as $key => $value) {
                if ($type == 'files') {
//                    $initial[] = "<div class='file-preview-text'><h2><i class='glyphicon glyphicon-file'></i></h2></div>";
                    $initial[] = [
                        self::getUploadUrl($field) . $this->files_path . '/' . $key,
                    ];
                    //"<div class='file-preview-other'><h2><i class='glyphicon
                    // glyphicon-file'></i></h2></div>";
                } elseif ($type == 'config') {
                    $file_name = explode('.', $key);
                    $file_type = end($file_name);
                    $type_error = ['doc', 'docx', 'xls', 'xlsx'];
                    if(in_array($file_type, $type_error)){
                        $mime = 'object';
                    }elseif (in_array($file_type,['png','jpg','jpeg'])){
                        $mime = 'image';
                    }else{
                        $mime = $file_type;
                    }

                    $initial[] = [
                        'caption' => $value,
                        'width' => '140px',
                        'url' => Url::to(['document/deletefile', 'id' => $this->document_id, 'fileName' => $key,
                            'field' => $field]),
                        'downloadUrl' => '../download?id=' . $this->document_id . '&file=' . $key . '&file_name=' . $value . '&field=' . $field,
                        //Url::to(['project/download','id'=>$this->prj_id,'fileName'=>$key,'field'=>$field]),
                        // Url::to(['project/download','id'=>$this->prj_id,'fileName'=>$key,'field'=>$field])
                        'key' => $key,
                        'type' => $mime,
                    ];
                } else {
                    $initial[] = Html::img(self::getUploadUrl($field) . $this->files_path . '/' . $key, ['class' => 'file-preview-image kv-preview-data',
                        //'alt'=>$->file_name, 'title'=>$model->file_name
                    ]);
                }
            }
        } else {

        }

//        print_r($initial);
        return $initial;
    }

    public static function getUploadUrl($field = 'files')
    {
        // area_pic ไม่ได้ อัพโหลดใหม่ แต่ link ตรงจาก area pic
        if ($field == 'files') {
            return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_PRJ_FILE . '/'; //. self::UPLOAD_AREA_FOLDER . '/';
        } elseif ($field == 'conf_pic') {
            return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_CONFIG_FOLDER . '/';
        } else {
            return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_EQUIPMENT_FOLDER . '/';
        }
    }

    public function listDownloadFiles($type)
    {
        $docs_file = '';
        if (in_array($type, ['files', 'conf_manual'])) {
            $data = $type === 'files' ? $this->files : $this->conf_manual;
            $files = Json::decode($data);
            if (is_array($files)) {
                $docs_file = '<ul>';
                foreach ($files as $key => $value) {
                    $docs_file .= '<li>' . Html::a($value, ['/document/download', 'id' => $this->document_id,
                            'file' => $key,
                            'file_name' => $value,
                            'field' => $type,
                        ], ['target' => 'blank']) . '</li>';

                    //$id, $file, $file_name
                }
                $docs_file .= '</ul>';
            }
        }

        return $docs_file;
    }

    public function viewFiles()
    {
        $docs_file = '';
        /* if (in_array($type, ['files', 'conf_manual'])) {
             $data = $type === 'files' ? $this->files : $this->conf_manual;
             $files = Json::decode($data);
             if (is_array($files)) {
                 $docs_file = '<ul>';
                 foreach ($files as $key => $value) {
                     $docs_file .= '<li>' . Html::a($value, ['/document/download', 'id' => $this->document_id,
                             'file' => $key,
                             'file_name' => $value,
                             'field' => $type,
                         ], ['target' => 'blank']) . '</li>';

                     //$id, $file, $file_name
                 }
                 $docs_file .= '</ul>';
             }
         }*/

        // แยกไฟล์ออกจากกันก่อน แต่ละประเภทของไฟล์
        $files = ['pdf'];
        $images = ['png', 'jpg', 'jpeg'];
        $allImages = [];
        $allDocs = [];
        $docs_file = "";
        $allFiles = Json::decode($this->files);
        if (is_array($allFiles)) {
            $docs_file .= '<ul>';
            foreach ($allFiles as $key => $value) {
                $extension = explode('.', $key);
                if (in_array($extension[1], $images)) {
                    $allImages[] = [
                        'content'=>Html::img(self::getUploadUrl('files') . $this->files_path . '/' . $key, ["class"=>"img-thumbnail"]),
                        //'cation'=>'',
                        //'options'=>''
                    ];
                }

                $docs_file .= '<li>' . Html::a($value, ['/document/download', 'id' => $this->document_id,
                        'file' => $key,
                        'file_name' => $value,
                        'field' => 'file',
                    ], ['target' => 'blank']) . '</li>';
            }
            $docs_file .= '</ul>';
        }
        return [$docs_file,$allImages];
    }

    public function getFileLink(){
        $files = $this->files;
        $decode = Json::decode($files);
        if(!is_null($decode) && count($decode)>1){
            return "javascript:void(0)";
        }else{
            $key = @array_keys($decode);
            $icon = @explode('.',$key[0]);
            //var_dump($allFiles);
            return self::getUploadUrl('files').$this->files_path.'/'.$key[0];
        }
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCreateBy()
    {
        return $this->createdBy->firstname.' '.$this->createdBy->lastname;
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[DocumentType]].
     *
     * @return \yii\db\ActiveQuery|DocumentTypeQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(DocumentType::className(), ['doctype_id' => 'document_type']);
    }

    public function getDocumentTypeIcon(){
        // เอกสาร ที่แนบเข้ามา มีกี่ไฟล์ เป็นประเภทอะไรบ้าง
        // หากเป็น 1 ประเภท หรือไฟล์เดียว แสดง icon ตามไฟล์
        // หากมี หลายไฟล์ ให้แสดงไอคอน เป็นไฟล์ซิป
        $allFiles = Json::decode($this->files);
        $countFile = count($allFiles);
        if (is_array($allFiles) && $countFile>1) {
            $icon = self::getIcon('zip');
            return $icon;
        }else{
            $key = @array_keys($allFiles);
            $icon = @explode('.',$key[0]);
            //var_dump($allFiles);
            return self::getIcon($icon[1]);
        }
    }

    public function getDocumentTypeName()
    {
        return $this->title." <br>(".$this->documentType->doctype_name.")";
    }
    public function getDocumentTypeTitle()
    {
        return $this->documentType->doctype_name;
    }

    public function getMissionName()
    {
        return $this->mission->mission_name;
    }

    public function getStatusDoc()
    {
        return self::getStatusName($this->status);
    }

    /**
     * Gets query for [[DocumentType]].
     *
     * @return \yii\db\ActiveQuery|DocumentTypeQuery
     */
    public function getMission()
    {
        return $this->hasOne(Mission::className(), ['mission_id' => 'mission_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            /*[
                'class' => AttributesBehavior::className(),
                'attributes' => [
                    'prj_start_date' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => function ($event, $attribute) {
                            $format = $this->detectFormat($this->prj_start_date);
                            //explode('-', $this->budgetdate);
                            if ($format == $this::FORMAT_TH) {
                                $ex = explode('-', $this->prj_start_date);
                                return $this->prj_start_date = ($ex[2] - 543) . '-' . $ex[1] . '-' . $ex[0];
                            } else {
                                return $this->prj_start_date;
                            }
                        },

                    ],
                    'prj_end_date' => [
                        ActiveRecord::EVENT_BEFORE_VALIDATE => function ($event, $attribute) {
                            $format = $this->detectFormat($this->prj_end_date);
                            //explode('-', $this->budgetdate);
                            if ($format == $this::FORMAT_TH) {
                                $ex = explode('-', $this->prj_end_date);
                                return $this->prj_end_date = ($ex[2] - 543) . '-' . $ex[1] . '-' . $ex[0];
                            } else {
                                return $this->prj_end_date;
                            }
                        },

                    ],
                ],
            ],*/
        ];
    }
}
