<?php

namespace common\models;

use Yii;
use \common\models\base\MeetingCommand as BaseMeetingCommand;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "meeting_command".
 */
class MeetingCommand extends BaseMeetingCommand
{
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['command_name', 'commander_name', 'command_status', 'command_urgency', 'command_enddate', 'meeting_id'], 'required'],
            [['command_name'], 'string'],
            [['command_status', 'command_urgency', 'meeting_id', 'created_user', 'updated_user'], 'integer'],
            [['command_enddate', 'created_at', 'updated_at'], 'safe'],
            [['commander_name'], 'string', 'max' => 255]
        ]);
    }*/

    public static function createCommand($model,$pk,$flag='new'){
        $rows = [];
        if ($flag == 'new') {
            if ($model[0]['command_name'] == '')
                return false;

            foreach ($model as $id) {
                $enddate = BaseActiveRecord::dateEngDb($id['command_enddate']);
                $rows[] = [$pk, $id['command_name'], $id['commander_name'], $id['command_urgency'],$enddate,Yii::$app->user->id, new Expression('NOW()')];
            }
            $command = Yii::$app->db->createCommand();
            $command->batchInsert(self::tableName(),
                ['meeting_id', 'command_name', 'commander_name','command_urgency','command_enddate', 'created_user', 'created_at'],
                $rows
            );
            try {
                //$ts->commit();
                return $command->execute();
            } catch (Exception $exception) {
                //$ts->rollBack();
                throw new Exception($exception->getMessage());
            }
        } else {
            try {
                $ts = Yii::$app->db->beginTransaction();
                $oldModel = self::find()->select('command_id')->where(['meeting_id' => $pk])->asArray()->all();

                foreach ($model as $key => $item) {
                    if ($item['command_id'] == '') {
                        $newmodel = new MeetingCommand();
                        $newmodel->scenario = 'new-model';
                        $newmodel->attributes = $item;
                        $newmodel->meeting_id = $pk;
                        $newmodel->command_status = 1;
                        $newmodel->command_enddate = self::dateEngDb($newmodel->command_enddate);
                        if($newmodel->validate()){
                            $newmodel->save();
                        }else{
                            throw new Exception(implode(',',$newmodel->getErrorSummary(true)));
                        }
                        //$commandModels[] = $newmodel;
                        unset($model[$key]);
                    }
                }

                // filter for update
                //print_r($oldModel);
                //print_r($budgets);

                $oldId = ArrayHelper::getColumn($oldModel, 'command_id');
                $cId = ArrayHelper::getColumn($model, 'command_id');

                $deletes = array_diff($oldId, $cId);

                if ($deletes) {
                    MeetingCommand::deleteAll(['and', 'meeting_id = :meeting_id', ['in', 'command_id', $deletes]], [':meeting_id' => $pk]);

                }
                // loop for update or insert data
                $rows = false;
                foreach ($model as $id) {
                    $enddate = BaseActiveRecord::dateEngDb($id['command_enddate']);
                    $rows[] = [$id['command_id'],$pk, $id['command_name'],$id['commander_name'],$id['command_urgency'],$enddate, Yii::$app->user->id, new Expression('NOW()')];
                }

                if($rows!=false) {
                    $db = Yii::$app->db;
                    $sql = $db->queryBuilder->batchInsert('meeting_command',
                        ['command_id', 'meeting_id', 'command_name', 'commander_name', 'command_urgency', 'command_enddate', 'created_user', 'created_at'], $rows);
                    // try {
                    //$ts->commit();
                    $sql = str_replace('INSERT', 'REPLACE', $sql);
                    if ($db->createCommand($sql)->execute()) {
                        $ts->commit();
                        return 1;
                    } else {
                        throw new Exception(implode(',', $db->pdo->errorInfo()));
                    }
                }
                $ts->commit();

            }catch (Exception $exception){
                $ts->rollBack();
                throw new HttpException(500,$exception->getMessage());
            }
        }
    }

    public function getResName(){
        $ids = $this->getMeetingCommandItems()->select('item_id');
        $mission = MeetingResponsible::find()
            ->select(['meeting_responsible.mission_id','responsible_status','responsible_user','mission.mission_name','meeting_responsible.item_id'])
            ->leftJoin('mission','mission.mission_id=meeting_responsible.mission_id')
            ->where(['in', 'item_id', $ids]);


        //$missionNameMd = Mission::find()->select('mission_name')->where(['in','mission_id',$mission])->asArray()->all();
        //$missionName = ArrayHelper::getColumn($missionNameMd,'mission_name');
        //print_r($missionName);
        //return implode(',',$missionName);

        return $mission->asArray()->all();
    }

    public function commandUrgencyStyle(){
        if ($this->command_urgency == 1) {
            $style = 'danger';
        } elseif ($this->command_urgency == 2) {
            $style = 'warning';
        }
        if ($this->command_urgency == 3) {
            $style = 'secondary';
        }
        $urgency = "<div class='badge badge-{$style}'>" . self::getUrgencyItem($this->command_urgency) . "</div>";
        return $urgency;
    }
	
}
