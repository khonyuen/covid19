<?php

namespace common\models;

use Yii;
use \common\models\base\MeetingResponsible as BaseMeetingResponsible;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "meeting_responsible".
 */
class MeetingResponsible extends BaseMeetingResponsible
{
    /**
     * @inheritdoc
     */
   /* public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['mission_id', 'item_id', 'responsible_status', 'responsible_user'], 'integer'],
            [['item_id', 'responsible_status'], 'required'],
            [['updated_date'], 'safe'],
            [['mission_id', 'item_id'], 'unique', 'targetAttribute' => ['mission_id', 'item_id'], 'message' => 'The combination of ผู้รับผิดขอบ and รายละเอียด has already been taken.']
        ]);
    }*/


    public static function saveRes($models,$pk,$flag='new'){
        //var_dump($models);
        //echo $pk;
        $rows = [];
        if(empty($models))
            return false;

        if ($flag == 'new') {
            foreach ($models as $model) {
                $rows[] = [$pk, $model, 1, new Expression('NOW()')];
            }

            //var_dump($rows);
            $command = Yii::$app->db->createCommand();
            $command->batchInsert(self::tableName(),
                ['item_id', 'mission_id', 'responsible_status', 'updated_date'],
                $rows
            );
            try {
                return $command->execute();
            } catch (Exception $exception) {
                throw new Exception($exception->getMessage());
            }
        } elseif ($flag == 'update') {
            $oldModel = self::findAll(['item_id' => $pk]);
            $oldId = ArrayHelper::getColumn($oldModel, 'mission_id'); // รายการเก่า
            $cId = $models; // รายการใหม่
            $deleteIds = array_diff($oldId, $cId);
            $insertIds = array_diff($cId, $oldId);
            if ($deleteIds) {
                self::deleteAll(['and', 'item_id = :item_id',['in','mission_id',$deleteIds]],[':item_id'=>$pk]);
               // self::updateAll(['deleted_by' => Yii::$app->user->id, 'deleted_at' => new Expression('NOW()')], ['and', 'prj_id = :prj_id', ['in', 'dep_id', $deleteIds]], [':prj_id' => $pk]);
            }
            if ($insertIds) {
                foreach ($insertIds as $id) {
                    $rows[] = [$pk, $id, 1, new Expression('NOW()')];
                }
                $command = Yii::$app->db->createCommand();
                $command->batchInsert(self::tableName(),
                    ['item_id', 'mission_id', 'responsible_status', 'updated_date'],
                    $rows
                );

                try {
                    return $command->execute();
                } catch (Exception $exception) {
                    throw new Exception($exception->getMessage());
                }
            }
        }
    }

    public function meetingResponsibleStyle(){
        if ($this->responsible_status == 1) {
            $style = 'warning';
        } elseif ($this->responsible_status == 2) {
            $style = 'success';
        }elseif ($this->responsible_status == 3) {
            $style = 'dark line-through';
        }elseif ($this->responsible_status == 4) {
            $style = 'secondary text-danger';
        }elseif ($this->responsible_status == 0) {
            $style = 'danger blink';
        }

        $urgency = "<div class='badge badge-{$style}'>" . self::getResponsibleStatusItem($this->responsible_status) ."  </div>";
        return $urgency;
    }

    public function meetingResponsibleMissionStyle(){
        if ($this->responsible_status == 1) {
            $style = 'warning';
        } elseif ($this->responsible_status == 2) {
            $style = 'success';
        }elseif ($this->responsible_status == 3) {
            $style = 'dark';
        }elseif ($this->responsible_status == 4) {
            $style = 'danger';
        }elseif ($this->responsible_status == 0) {
            $style = 'danger';
        }

        $urgency = "<font class='text-{$style}'>".$this->mission->mission_name . " (".self::getResponsibleStatusItem($this->responsible_status) .")  </font>";
        return $urgency;
    }
	
}
