<?php

namespace common\models;

use Yii;
use \common\models\base\MeetingCommandItems as BaseMeetingCommandItems;

/**
 * This is the model class for table "meeting_command_items".
 */
class MeetingCommandItems extends BaseMeetingCommandItems
{
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['item_detail', 'item_urgency'], 'required'],
            [['item_detail'], 'string'],
            [['item_urgency', 'command_id', 'created_by', 'updated_by'], 'integer'],
            [['item_enddate', 'created_at', 'updated_at'], 'safe']
        ]);
    }*/

    public function commandItemUrgencyStyle(){
        if ($this->item_urgency == 1) {
            $style = 'danger';
        } elseif ($this->item_urgency == 2) {
            $style = 'warning';
        }elseif ($this->item_urgency == 3) {
            $style = 'secondary';
        }
        //$urgency = "<div class='badge badge-{$style}'>" . self::getUrgencyItem($this->command_urgency) . "</div>";
        $date = $this->item_enddate ? "(".self::dateEngDb($this->item_enddate).")" : "";
        $urgency = "<div class='badge badge-{$style}'>" . self::getUrgencyItem($this->item_urgency) ." $date </div>";
        return $urgency;
    }
	
}
