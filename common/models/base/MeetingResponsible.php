<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "meeting_responsible".
 *
 * @property integer $responsible_id
 * @property integer $mission_id
 * @property integer $item_id
 * @property integer $responsible_status
 * @property integer $responsible_user
 * @property string $updated_date
 *
 * @property \common\models\MeetingCommandLog[] $meetingCommandLogs
 * @property \common\models\MeetingReport[] $meetingReports
 * @property \common\models\MeetingReport $meetingReportLast
 * @property \common\models\Mission $mission
 * @property \common\models\User $responsibleUser
 * @property \common\models\MeetingCommandItems $item
 */
class MeetingResponsible extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $_meetingReport;
    public $_meetingLog;

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'meetingCommandLogs',
            'meetingReports',
            'mission',
            'responsibleUser',
            'item'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mission_id', 'item_id', 'responsible_status', 'responsible_user'], 'integer'],
            [['item_id', 'responsible_status'], 'required'],
            [['updated_date','_meetingReport'], 'safe'],
            [['mission_id', 'item_id'], 'unique', 'targetAttribute' => ['mission_id', 'item_id'], 'message' => 'The combination of ผู้รับผิดขอบ and รายละเอียด has already been taken.'],
            //[['_meetingReport'],'required','on'=>'create-report'],

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_responsible';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'responsible_id' => 'Responsible ID',
            'mission_id' => 'ผู้รับผิดขอบ',
            'item_id' => 'รายละเอียด',
            'responsible_status' => 'สถานะ',//1 on job 2 end job 3 cancel 4 re-edit
            'responsible_user' => 'ผู้รับผิดชอบ (ระบุ)',
            'updated_date' => 'วันที่อัพเดท',
            '_meetingReport'=>'การรายงาน',
            '_meetingLog'=>'ประวัติ',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingCommandLogs()
    {
        return $this->hasMany(\common\models\MeetingCommandLog::className(), ['responsible_id' => 'responsible_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingReports()
    {
        return $this->hasMany(\common\models\MeetingReport::className(), ['responsible_id' => 'responsible_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingReportLast()
    {
        return $this->hasOne(\common\models\MeetingReport::className(), ['responsible_id' => 'responsible_id'])
            ->orderBy('report_id desc')->limit(1);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMission()
    {
        return $this->hasOne(\common\models\Mission::className(), ['mission_id' => 'mission_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'responsible_user']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(\common\models\MeetingCommandItems::className(), ['item_id' => 'item_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                //'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            /*'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }



    /**
     * @inheritdoc
     * @return \common\models\MeetingResponsibleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingResponsibleQuery(get_called_class());
    }
}
