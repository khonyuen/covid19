<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "meeting_command_log".
 *
 * @property integer $log_id
 * @property integer $responsible_id
 * @property integer $report_id
 * @property integer $old_status
 * @property integer $new_status
 * @property string $updated_at
 *
 * @property \common\models\MeetingResponsible $responsible
 */
class MeetingCommandLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'responsible'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_status', 'new_status'], 'required'],
            [['log_id', 'responsible_id', 'old_status', 'new_status'], 'integer'],
            [['updated_at','report_id','log_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_command_log';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'responsible_id' => 'Responsible ID',
            'old_status' => 'Old Status',
            'new_status' => 'New Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(\common\models\MeetingResponsible::className(), ['responsible_id' => 'responsible_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
//            'timestamp' => [
//                'class' => TimestampBehavior::className(),
//                //'createdAtAttribute' => 'created_at',
//                'updatedAtAttribute' => 'updated_at',
//                'value' => new \yii\db\Expression('NOW()'),
//            ],
            /*'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],*/
            /*'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingCommandLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingCommandLogQuery(get_called_class());
    }
}
