<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the base model class for table "meeting_command_items".
 *
 * @property integer $item_id
 * @property string $item_detail
 * @property integer $item_urgency
 * @property string $item_enddate
 * @property integer $command_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\MeetingCommand $command
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \common\models\MeetingResponsible[] $meetingResponsibles
 */
class MeetingCommandItems extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $_responsible;
    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'command',
            'createdBy',
            'updatedBy',
            'meetingResponsibles'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_detail', 'item_urgency'], 'required'],
            [['item_detail'], 'string'],
            [['item_urgency', 'command_id', 'created_by', 'updated_by'], 'integer'],
            [['item_enddate', 'created_at', 'updated_at'], 'safe'],

            [['_responsible'],'required','on'=>'create-assign-item']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_command_items';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'item_detail' => 'รายละเอียด',
            'item_urgency' => 'ความเร่งด่วน',
            'item_enddate' => 'กำหนดส่ง',
            'command_id' => 'ประเด็น/ข้อสั่งการ',
            '_responsible' => 'กลุ่มงาน/กลุ่มภารกิจที่เกี่ยวข้อง',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommand()
    {
        return $this->hasOne(\common\models\MeetingCommand::className(), ['command_id' => 'command_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingResponsibles()
    {
        return $this->hasMany(\common\models\MeetingResponsible::className(), ['item_id' => 'item_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            /*'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingCommandItemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingCommandItemsQuery(get_called_class());
    }

    public function getResponsibleIds(){
        $models = $this->getMeetingResponsibles()->select('mission_id')->all();
        $ids = ArrayHelper::getColumn($models,'mission_id');
        return $ids;
    }
}
