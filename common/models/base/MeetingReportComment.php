<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "meeting_report_comment".
 *
 * @property integer $comment_id
 * @property integer $report_id
 * @property string $comment_detail
 * @property string $comment_date
 * @property string $comment_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\MeetingReport $report
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 */
class MeetingReportComment extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'report',
            'createdBy',
            'updatedBy'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'created_by', 'updated_by'], 'integer'],
            [['comment_detail', 'comment_date', 'comment_name'], 'required'],
            [['comment_detail'], 'string'],
            [['comment_date', 'created_at', 'updated_at'], 'safe'],
            [['comment_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_report_comment';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Comment ID',
            'report_id' => 'รายงาน',
            'comment_detail' => 'ความเห็น',
            'comment_date' => 'วันที่แสดงความเห็น',
            'comment_name' => 'ผู้แสดงความเห็น',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(\common\models\MeetingReport::className(), ['report_id' => 'report_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingReportCommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingReportCommentQuery(get_called_class());
    }
}
