<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "meeting_command".
 *
 * @property integer $command_id
 * @property string $command_name
 * @property string $commander_name
 * @property integer $command_status
 * @property integer $command_urgency
 * @property string $command_enddate
 * @property integer $meeting_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_user
 * @property integer $updated_user
 *
 * @property \common\models\Meeting $meeting
 * @property \common\models\User $createdUser
 * @property \common\models\User $updatedUser
 * @property \common\models\MeetingCommandItems[] $meetingCommandItems
 */
class MeetingCommand extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'meeting',
            'createdUser',
            'updatedUser',
            'meetingCommandItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['command_name', 'command_status', 'command_urgency', 'command_enddate', 'meeting_id'], 'required'],
            [['command_name'], 'string'],
            [['command_status', 'command_urgency', 'meeting_id', 'created_user', 'updated_user'], 'integer'],
            [['command_enddate', 'created_at', 'updated_at','commander_name'], 'safe'],
            [['commander_name'], 'string', 'max' => 255],

            [['command_id','commander_name'],'safe','on' => 'new-model']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_command';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'command_id' => 'Command ID',
            'command_name' => 'ประเด็น/ข้อสั่งการ',
            'commander_name' => 'ผู้สั่งการ',
            'command_status' => 'สถานะ',
            'command_urgency' => 'ความรีบด่วน',
            'command_enddate' => 'กำหนดส่งภายใน',
            'meeting_id' => 'หัวข้อการประชุม',
            'created_user' => 'Created User',
            'updated_user' => 'Updated User',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(\common\models\Meeting::className(), ['meeting_id' => 'meeting_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_user']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_user']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingCommandItems()
    {
        return $this->hasMany(\common\models\MeetingCommandItems::className(), ['command_id' => 'command_id']);
    }

    public function getMeetingCommandItemsRes()
    {
        return $this->hasMany(\common\models\MeetingCommandItems::className(), ['command_id' => 'command_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
            /*'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingCommandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingCommandQuery(get_called_class());
    }
}
