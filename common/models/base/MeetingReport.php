<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\validators\RequiredValidator;

/**
 * This is the base model class for table "meeting_report".
 *
 * @property integer $report_id
 * @property integer $responsible_id
 * @property string $report_detail
 * @property string $report_date
 * @property string $report_file
 * @property string $report_images
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $report_path_temp
 *
 * @property \common\models\MeetingResponsible $responsible
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \common\models\MeetingReportComment[] $meetingReportComments
 */
class MeetingReport extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $_responsible_id;
//    public $_mission_id;
//    public $_item_id;
    public $_responsible_status;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_report';
    }

    /**
     * @inheritdoc
     * @return \common\models\MeetingReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingReportQuery(get_called_class());
    }

    /**
     * This function helps \mootensai\relation\RelationTrait runs faster
     * @return array relation names of this model
     */
    public function relationNames()
    {
        return [
            'responsible',
            'createdBy',
            'updatedBy',
            'meetingReportComments',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['responsible_id', 'created_by', 'updated_by'], 'integer'],
            [['report_detail', 'report_date'], 'required'],
            [['report_file'], 'file', 'extensions' => ['pdf', 'doc', 'docx', 'xls', 'xlsx'], 'maxFiles' => 20, 'skipOnEmpty' => true,],
            [['report_images'], 'file', 'extensions' => ['jpg', 'png', 'gif', 'bmp'], 'maxFiles' => 20, 'skipOnEmpty' => true,],
            [['report_detail', 'report_path_temp'], 'string'],
            [['report_date', 'created_at', 'updated_at', 'report_path_temp', 'responsible_id', '_responsible_status','report_id'], 'safe'],

            [['_responsible_status'], 'required', 'on' => 'create-stag'],
            [['_responsible_id'], 'required', 'on' => 'create-stag'],
        ];
    }

    /*public function validateResponsible($attribute)
    {
        $requiredValidator = new RequiredValidator();

        $error = null;

        $requiredValidator->validate($this->$attribute, $error);

        $key = $attribute . '[0][command_name]'; ;

        if (!empty($error)) {
            $this->addError($key, "ระบุผู้รับผิดชอบ");
        }
    }*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report_id' => 'Report ID',
            'responsible_id' => 'ข้อสั่งการ (รายการ)',
            'report_detail' => 'รายละเอียด',
            'report_date' => 'วันที่รายงาน',
            'report_file' => 'เอกสารแนบ',
            'report_images' => 'ภาพประกอบ',
            '_responsible_id' => 'ผู้รายงาน',
            '_responsible_status' => 'สถานะ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(\common\models\MeetingResponsible::className(), ['responsible_id' => 'responsible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingReportComments()
    {
        return $this->hasMany(\common\models\MeetingReportComment::className(), ['report_id' => 'report_id']);
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            /*'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }
}
