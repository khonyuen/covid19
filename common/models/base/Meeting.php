<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\validators\RequiredValidator;

/**
 * This is the base model class for table "meeting".
 *
 * @property integer $meeting_id
 * @property string $meeting_name
 * @property string $meeting_no
 * @property string $meeting_date
 * @property string $meeting_time
 * @property integer $meeting_status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_user
 * @property integer $updated_user
 *
 * @property \common\models\User $createdUser
 * @property \common\models\User $updatedUser
 * @property \common\models\MeetingCommand[] $meetingCommands
 */
class Meeting extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $_e;
    public $_m;
    public $_l;
    public $_tot;
    public $_commands;
    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'createdUser',
            'updatedUser',
            'meetingCommands'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_name', 'meeting_no', 'meeting_date', 'meeting_time'], 'required'],
            [['meeting_date', 'meeting_time', 'created_at', 'updated_at'], 'safe'],
            [['meeting_status', 'created_user', 'updated_user'], 'integer'],
            [['meeting_name'], 'string', 'max' => 255],
            [['meeting_no'], 'string', 'max' => 50],
            [['meeting_no'], 'unique'],

            ['_meeting_command','required'],
            [['_meeting_command'],'validateCommand','on'=>'valid-meeting-update'],
            [['_l','_e','_m','_tot','_urgency','_commands','meeting_id'],'safe']
        ];
    }

    public function validateCommand($attribute,$params,$validator){
        $requiredValidator = new RequiredValidator();
        //print_r($this->$attribute);
        foreach($this->$attribute as $index => $row) {
            $error = null;
            $error2 = null;
            $error3 = null;
            $error4 = null;
            $requiredValidator->validate($row['command_name'], $error);
            //$requiredValidator->validate($row['commander_name'], $error2);
            $requiredValidator->validate($row['command_enddate'], $error3);
            $requiredValidator->validate($row['command_urgency'], $error4);

            $key = $attribute . '[' . $index . '][command_name]';
            $key1 = $attribute . '[' . $index . '][commander_name]';
            $key2 = $attribute . '[' . $index . '][command_enddate]';
            $key3 = $attribute . '[' . $index . '][command_urgency]';
            if (!empty($error)) {
                $this->addError($key, "ระบุประเด็น/ข้อสั่งการ");
            }
            if (!empty($error2)) {
                $this->addError($key1, "ระบุผู้สั่งการ");
            }
            if (!empty($error3)) {
                $this->addError($key2, "ระบุวันที่กำหนดส่ง");
            }
            if (!empty($error4)) {
                $this->addError($key3, "ระบุความเร่งด่วน");
            }

            //
        }
        //print_r($error);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meeting_id' => 'Meeting ID',
            'meeting_name' => 'ชื่อการประชุม',
            'meeting_no' => 'ครั้งที่ประชุม',
            'meeting_date' => 'วันที่ประชุม',
            'meeting_time' => 'เวลา',
            'meeting_status' => 'สถานะ',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้แก้ไข',

            '_meeting_command' => 'ประเด็น/ข้อสั่งการ',
            '_urgency' => 'ความเร่งด่วน',
            '_e' => 'ด่วนที่สุด',
            '_m' => 'ด่วน',
            '_l' => 'ปกติ',
            '_commands' => 'ข้อสั่งการ',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_user']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'updated_user']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingCommands()
    {
        return $this->hasMany(\common\models\MeetingCommand::className(), ['meeting_id' => 'meeting_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
            /*'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],*/
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingQuery(get_called_class());
    }

    public function formatCommand(){
        $x = explode(':',$this->_commands);
        $commands = "";
        $commands.='<div class="row">
                        <div class="col-xl-12 col-lg-12"><div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            ประเด็น/ข้อสั่งการ
                                        </h3>
                                    </div>                                
                                </div>
                                <div class="kt-portlet__body">
                                <div class="kt-timeline-v3">
                                <div class="kt-timeline-v3__items">';
        $style = "";
        foreach ($x as $item) {
            $split = explode(',',$item);
            //command_name,',',commander_name,',',command_enddate,',',command_urgency
            if($split[3]==1){
                $style = 'danger';
            }elseif($split[3]==2){
                $style = 'warning';
            }elseif($split[3]==3){
                $style = 'info';
            }
            $commands .= "<div class=\"kt-timeline-v3__item kt-timeline-v3__item--{$style}\">
                                <span class=\"kt-timeline-v3__item-time\">{$split[2]}</span>
                                <div class=\"kt-timeline-v3__item-desc\">
                                    <span class=\"kt-timeline-v3__item-text\">
                                        {$split[0]}
                                    </span><br>
                                    <span class=\"kt-timeline-v3__item-user-name\">
                                        <a href=\"#\" class=\"kt-link kt-link--dark kt-timeline-v3__itek-link\">
                                            {$split[1]}
                                        </a>
                                    </span>
                                </div>
                            </div>";
        }

        $commands.= '</div></div></div></div></div>';
        return "<div class=\"kt-portlet kt-portlet--tabs kt-portlet--height-fluid mt-4\">
											<div class=\"kt-portlet__head\">
												<div class=\"kt-portlet__head-label\">
													<h3 class=\"kt-portlet__head-title\">
														New Users
													</h3>
												</div>
												
											</div>
											<div class=\"kt-portlet__body\">
												<div class=\"tab-content\">
													<div class=\"tab-pane active\" id=\"kt_widget4_tab1_content\">
														<div class=\"kt-widget4\">
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_4.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Anna Strong
																	</a>
																	<p class=\"kt-widget4__text\">
																		Visual Designer,Google Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-brand btn-bold\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_14.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Milano Esco
																	</a>
																	<p class=\"kt-widget4__text\">
																		Product Designer, Apple Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-warning btn-bold\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_11.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Nick Bold
																	</a>
																	<p class=\"kt-widget4__text\">
																		Web Developer, Facebook Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-danger btn-bold\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_1.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Wiltor Delton
																	</a>
																	<p class=\"kt-widget4__text\">
																		Project Manager, Amazon Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-success btn-bold\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_5.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Nick Stone
																	</a>
																	<p class=\"kt-widget4__text\">
																		Visual Designer, Github Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-primary btn-bold\">Follow</a>
															</div>
														</div>
													</div>
													<div class=\"tab-pane\" id=\"kt_widget4_tab2_content\">
														<div class=\"kt-widget4\">
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_2.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Kristika Bold
																	</a>
																	<p class=\"kt-widget4__text\">
																		Product Designer,Apple Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-success\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_13.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Ron Silk
																	</a>
																	<p class=\"kt-widget4__text\">
																		Release Manager, Loop Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-brand\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_9.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Nick Bold
																	</a>
																	<p class=\"kt-widget4__text\">
																		Web Developer, Facebook Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-danger\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_2.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Wiltor Delton
																	</a>
																	<p class=\"kt-widget4__text\">
																		Project Manager, Amazon Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-success\">Follow</a>
															</div>
															<div class=\"kt-widget4__item\">
																<div class=\"kt-widget4__pic kt-widget4__pic--pic\">
																	<img src=\"assets/media/users/100_8.jpg\" alt=\"\">
																</div>
																<div class=\"kt-widget4__info\">
																	<a href=\"#\" class=\"kt-widget4__username\">
																		Nick Bold
																	</a>
																	<p class=\"kt-widget4__text\">
																		Web Developer, Facebook Inc
																	</p>
																</div>
																<a href=\"#\" class=\"btn btn-sm btn-label-info\">Follow</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>";


    }
}
