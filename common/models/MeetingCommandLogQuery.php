<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MeetingCommandLog]].
 *
 * @see MeetingCommandLog
 */
class MeetingCommandLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MeetingCommandLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MeetingCommandLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
