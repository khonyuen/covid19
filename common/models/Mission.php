<?php

namespace common\models;

use Faker\Provider\Base;
use frontend\models\CmTargetResult;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mission".
 *
 * @property int $mission_id
 * @property string|null $mission_name กลุ่มภารกิจ
 * @property string|null $mission_desc
 * @property string|null $mission_pic รูปภาพ
 * @property int|null $order เรียง
 * @property int|null $status สถานะ
 * @property int|null $mission_type ประเภทกล่องภารกิจ
 *
 * @property Document[] $documents
 */
class Mission extends BaseActiveRecord
{
    public $tot_doc;

    const STAG = 1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order', 'status','mission_type'], 'integer'],
            [['mission_name', 'mission_desc', 'mission_pic'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mission_id' => 'Mission ID',
            'mission_name' => 'กลุ่มภารกิจ',
            'mission_desc' => 'Mission Desc',
            'mission_pic' => 'รูปภาพ',
            'mission_type' => 'ประเภทภารกิจ',
            'order' => 'เรียง',
            'status' => 'สถานะ',
            'tot_doc'=>'จำนวนเอกสาร'
        ];
    }

    /**
     * {@inheritdoc}
     * @return MissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MissionQuery(get_called_class());
    }

    public static function getList(){
        $mission = self::find()->select(['mission_name', 'mission_id'])->where(['status'=>1])->orderBy('order')->all(); //->column();
        $x = ArrayHelper::map($mission,'mission_id','mission_name');
        return $x;
    }

    /**
     * Gets query for [[Document]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['mission_id' => 'mission_id']);
    }

    public function actionList(){
        return [
            'view'=>[true],
            'update'=>[1],
            'delete'=>[1],
            'create'=>[true],
        ];
    }
}
