<?php
/**
 * Created by PhpStorm.
 * User: Teerapong
 * Date: 3/29/2018
 * Time: 11:42 PM
 */

namespace common\models;


use yii\helpers\ArrayHelper;

class BaseActiveRecord extends \yii\db\ActiveRecord
{
    const FORMAT_EN = 'en';
    const FORMAT_TH = 'th';

    const STATUS_ENABLE = '1';
    const STATUS_DISABLE = '0';

    const CM = "5";

    protected function detectFormat($field)
    {
        //echo $field;
        $exp = (strpos($field, '-')) ? explode('-', $field) : null;
        if ($exp) {
            if (strlen($exp[0]) == 4) {
                return self::FORMAT_EN;
            } elseif (strlen($exp[2]) == 4) {
                return self::FORMAT_TH;
            } else {
                //echo 1;
                return false;
            }
        } else {
            //echo 2;
            return false;
        }
    }

    public static function dateEngDb($date){
        if($date=='')
            return '';
        $date = explode('-',$date);
        return $date[2].'-'.$date[1].'-'.$date[0];
    }
    public static function dateEngFDb($date){
        $date = explode('-',$date);
        return $date[2].'-'.$date[1].'-'.$date[0];
    }

    public function dateTh($datetime)
    {
        $datetime = explode(' ', $datetime);
        $dates = $datetime[0];
        $time = $datetime[1] != '' ? "เวลา " . $datetime[1] : "";

        $exp_date = explode('-', $dates);
        $year = $exp_date[0] - 543 == date('Y') ? $exp_date[0] : ($exp_date[0] + 543);
        return $exp_date[2] . '-' . $exp_date[1] . '-' . $year . " " . $time;
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ENABLE => 'เผยแพร่',
            self::STATUS_DISABLE => 'ไม่เผยแพร่',
        ];
    }

    public static function getResponsibleStatusList()
    {
        return [
            0=>'ยังไม่ดำเนินการ',
            1=>'มอบ/รับทราบ/กำลังดำเนินการ',
            2=>'เสร็จสิ้น',
            3=>'ยกเลิกภารกิจ',
            4=>'แจ้งแก้ไข',
        ];
    }

    public static function getStatusName($status)
    {
        $list = self::getStatusList();
        return @$list[$status];
    }

    public static function getResponsibleStatusItem($status)
    {
        $list = self::getResponsibleStatusList();
        return @$list[$status];
    }

    public function dateThFormat($date)
    {
        if ($date == '' || $date == '0000-00-00' || $date == '00-00-0000')
            return "";
        //echo $date;
        $date_exp = explode('-', $date);
        if (strlen($date_exp[0]) == 4) {
            if ($date_exp[0] - 543 == date('Y')) {
                // thai
                return $date_exp[2] . '-' . $date_exp[1] . '-' . $date_exp[0];
            } else {
                // en
                return $date_exp[2] . '-' . $date_exp[1] . '-' . ($date_exp[0] + 543);
            }
        } elseif (strlen($date_exp[2]) == 4) {
            if ($date_exp[2] - 543 == date('Y')) {
                // thai
                return $date_exp[0] . '-' . $date_exp[1] . '-' . $date_exp[2];
            } else {
                // en
                return $date_exp[0] . '-' . $date_exp[1] . '-' . ($date_exp[2] + 543);
            }
        }
    }

    public static function dateThFormats($date)
    {
        if ($date == '' || $date == '0000-00-00' || $date == '00-00-0000')
            return "";
        //echo $date;
        $date_exp = explode('-', $date);
        if (strlen($date_exp[0]) == 4) {
            if ($date_exp[0] - 543 == date('Y')) {
                // thai
                return $date_exp[2] . '-' . $date_exp[1] . '-' . $date_exp[0];
            } else {
                // en
                return $date_exp[2] . '-' . $date_exp[1] . '-' . ($date_exp[0] + 543);
            }
        } elseif (strlen($date_exp[2]) == 4) {
            if ($date_exp[2] - 543 == date('Y')) {
                // thai
                return $date_exp[0] . '-' . $date_exp[1] . '-' . $date_exp[2];
            } else {
                // en
                return $date_exp[0] . '-' . $date_exp[1] . '-' . ($date_exp[2] + 543);
            }
        }
    }

    public static function monthInThai()
    {
        return [
            '10' => 'ตุลาคม',
            '11' => 'พฤศจิกายน',
            '12' => 'ธันวาคม',
            '01' => 'มกราคม',
            '02' => 'กุมภาพันธ์',
            '03' => 'มีนาคม',
            '04' => 'เมษายน',
            '05' => 'พฤษภาคม',
            '06' => 'มิถุนายน',
            '07' => 'กรกฏาคม',
            '08' => 'สิงหาคม',
            '09' => 'กันยายน',
        ];
    }

    public static function getMonthInThai($key)
    {
        return (ArrayHelper::keyExists($key, self::monthInThai())) ? self::monthInThai()[$key] : "";
    }

    public function monthList($end, $start = null)
    {
        if ($start == null) {
            $start = date((\Yii::$app->params['defaultBudgetYearInt'] - 1) . '-10');
        }
        $current = $start;
        $ret = [];
        //echo $end.' '.$start. ' '.$current;
        while ($current <= $end) {
            $c = @date('Y-m', strtotime($current));
            $next = @date('Y-m', strtotime($current . "+1 month", time()));
            //$current = @strtotime($next);
            $current = $next;
            $ret[] = $c;
        }

        //print_r($ret);
        return array_reverse($ret);
    }

    public static function getRoleList()
    {
        return [
            1 => 'Admin จังหวัด',
            2 => 'Admin อำเภอ/คัพ/รพ.',
            3 => 'ทั่วไป (กลุ่มฝ่าย, แผน รพ. สสอ. CUP,รพ.สต.)',
            4 => 'Admin รพ.',
        ];
    }

    public static function typeOfficeList(){
        return [
            4=>'รพ.สต.',
            3=>'คปสอ.',
            2=>'สสอ.',
            5=>'รพ.',
            1=>'กลุ่มฝ่าย สสจ.',
        ];
    }

    public static function getTypeOfficeList($level){
        if($level==1){
            return self::typeOfficeList();
        }elseif($level==2){
            return [
                4=>'รพ.สต.',
                3=>'คปสอ.',
                2=>'สสอ.',
            ];
        }elseif ($level==3){
            return [];
        }
    }

    public static function getTypeOffice($type){
        return ArrayHelper::keyExists($type,self::typeOfficeList()) ? self::typeOfficeList()[$type] : null;
    }

    public static function checkPeriod($date)
    {
        $exp = explode('-',$date);
        $newDate = ($exp[0]-543).'-'.$exp[1];
        $currentDate =  date('Y-m');
        // มากกว่า หรือเท่ากับ
        return $currentDate>=$newDate;
    }

    public static function getMeetingStatus(){
        return [
            '0'=>'ปิด',
            '1'=>'เปิด',
        ];
    }

    public static function getUrgency(){
        return [
            1=>'ด่วนที่สุด',
            2=>'ด่วน',
            3=>'ปกติ',
        ];
    }

    public static function getUrgencyItem($id){
        $list= self::getUrgency();
        return ($list[$id]) ? $list[$id] : "";
    }

}