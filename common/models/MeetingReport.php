<?php

namespace common\models;

use common\models\base\MeetingReport as BaseMeetingReport;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * This is the model class for table "meeting_report".
 */
class MeetingReport extends BaseMeetingReport
{
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['responsible_id', 'created_by', 'updated_by'], 'integer'],
            [['report_detail', 'report_date'], 'required'],
            [['report_detail', 'report_file', 'report_images'], 'string'],
            [['report_date', 'created_at', 'updated_at'], 'safe']
        ]);
    }*/

    const UPLOAD_FOLDER = 'uploads';
    const UPLOAD_FILE = 'files';
    const UPLOAD_IMG = 'images';

    public static function getUploadPath($field = 'report_file')
    {
        if ($field == 'report_file') {
            return Yii::getAlias('@webroot') . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_FILE . '/';// . self::UPLOAD_AREA_FOLDER . '/';
        } elseif ($field == 'report_images') {
            return Yii::getAlias('@webroot') . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_IMG . '/';// . self::UPLOAD_AREA_FOLDER . '/';
        }
    }

    /**
     * @param $data
     * @param $field
     * @param string $type
     * @return array
     * แสดงข้อมูลรูปภาพ
     * data ฟิวที่เก็บ json รูปภาพ
     * field ฟิวที่เก้บตำแหน่งรูป
     * type ประเภทของข้อมูลที่จะแสดง
     *
     */
    public function initialPreview($data, $field, $type = 'files')
    {
        $initial = [];
        $files = Json::decode($data);
        //print_r($files);
        if (is_array($files)) {
            foreach ($files as $key => $value) {
                if ($type == 'files') {
//                    $initial[] = "<div class='file-preview-text'><h2><i class='glyphicon glyphicon-file'></i></h2></div>";
                    $initial[] = [
                        self::getUploadUrl($field) . $this->report_path_temp . '/' . $key,
                    ];
                    //"<div class='file-preview-other'><h2><i class='glyphicon
                    // glyphicon-file'></i></h2></div>";
                } elseif ($type == 'config') {
                    $file_name = explode('.', $key);
                    $file_type = end($file_name);
                    $type_error = ['doc', 'docx', 'xls', 'xlsx'];
                    if(in_array($file_type, $type_error)){
                        $mime = 'object';
                    }elseif (in_array($file_type,['png','jpg','jpeg'])){
                        $mime = 'image';
                    }else{
                        $mime = $file_type;
                    }

                    $initial[] = [
                        'caption' => $value,
                        'width' => '140px',
                        'url' => Url::to(['meeting-report/deletefile', 'id' => $this->report_id, 'fileName' => $key,
                            'field' => $field]),
                        'downloadUrl' => Url::to(['meeting-report/download','id'=> $this->report_id,
                            'file'=>$key, 'file_name'=>  $value, 'field' => $field]),
                        'key' => $key,
                        'type' => $mime,
                    ];
                } else {
                    $initial[] = Html::img(self::getUploadUrl($field) . $this->report_path_temp . '/' . $key, ['class' => 'file-preview-image kv-preview-data',
                        //'alt'=>$->file_name, 'title'=>$model->file_name
                    ]);
                }
            }
        } else {

        }

//        print_r($initial);
        return $initial;
    }

    public static function getUploadUrl($field = 'report_file')
    {
        // area_pic ไม่ได้ อัพโหลดใหม่ แต่ link ตรงจาก area pic
        if ($field == 'report_file') {
            return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_FILE . '/'; //. self::UPLOAD_AREA_FOLDER . '/';
        } elseif ($field == 'report_images') {
            return Url::base(true) . '/' . self::UPLOAD_FOLDER . '/' . self::UPLOAD_IMG . '/';
        }
    }

    public function listDownloadFiles($type)
    {
        $docs_file = '';
        if (in_array($type, ['report_file', 'images'])) {
            $data = $type === 'report_file' ? $this->report_file : $this->report_images;
            $files = Json::decode($data);
            if (is_array($files)) {
                $docs_file = '<ul>';
                foreach ($files as $key => $value) {
                    $docs_file .= '<li>' . Html::a($value, Url::to(['meeting-report/download', 'id' => $this->report_id,
                            'file' => $key,
                            'file_name' => $value,
                            'field' => $type,
                        ]), ['target' => 'blank']) . '</li>';

                    //$id, $file, $file_name
                }
                $docs_file .= '</ul>';
            }
        }

        return $docs_file;
    }

    public function viewFiles($type = 'report_file')
    {
        // แยกไฟล์ออกจากกันก่อน แต่ละประเภทของไฟล์
        $files = ['pdf'];
        $images = ['png', 'jpg', 'jpeg'];
        $allImages = [];
        $allDocs = [];
        $docs_file = "";
        $allFiles = Json::decode($this->$type);
        if (is_array($allFiles)) {
            $docs_file .= '<ul>';
            foreach ($allFiles as $key => $value) {
                $extension = explode('.', $key);
                if (in_array($extension[1], $images)) {
                    $allImages[] = [
                        'content' => Html::img(self::getUploadUrl($type) . $this->report_path_temp . '/' . $key, ["class" => "img-thumbnail", 'style' => 'width:80%']),
                        //'cation'=>'',
                        //'options'=>''
                    ];
                }

                $docs_file .= '<li>' . Html::a($value, ['/meeting-report/download', 'id' => $this->report_id,
                        'file' => $key,
                        'file_name' => $value,
                        'field' => $type,
                    ], ['target' => 'blank', 'data-pjax' => 0]) . '</li>';
            }
            $docs_file .= '</ul>';
        }
        return [$docs_file, $allImages];
    }

    public function getFileLink($type = 'report_file')
    {
        $files = $this->$type;
        $decode = Json::decode($files);
        if (!is_null($decode) && count($decode) > 1) {
            return "javascript:void(0)";
        } else {
            $key = @array_keys($decode);
            $icon = @explode('.', $key[0]);
            //var_dump($allFiles);
            return self::getUploadUrl($type) . $this->report_path_temp . '/' . $key[0];
        }
    }

    public function getDocumentTypeIcon($type = 'report_file')
    {
        // เอกสาร ที่แนบเข้ามา มีกี่ไฟล์ เป็นประเภทอะไรบ้าง
        // หากเป็น 1 ประเภท หรือไฟล์เดียว แสดง icon ตามไฟล์
        // หากมี หลายไฟล์ ให้แสดงไอคอน เป็นไฟล์ซิป
        $allFiles = Json::decode($this->$type);
        $countFile = count($allFiles);
        if (is_array($allFiles) && $countFile > 1) {
            $icon = self::getIcon('zip');
            return $icon;
        } else {
            $key = @array_keys($allFiles);
            $icon = @explode('.', $key[0]);
            //var_dump($allFiles);
            return self::getIcon($icon[1]);
        }
    }

    public static function getIcon($i)
    {
        $icon = self::icon();
        return isset($icon[$i]) ? $icon[$i] : "";
    }

    public static function icon()
    {
        return [
            'css' => 'css.svg',
            'csv' => 'csv.svg',
            'doc' => 'doc.svg',
            'html' => 'html.svg',
            'js' => 'javascript.svg',
            'jpg' => 'jpg.svg',
            'media' => 'mp4.svg',
            'pdf' => 'pdf.svg',
            'xml' => 'xml.svg',
            'zip' => 'zip.svg',
        ];
    }

    public function reportDetailFile(){
        $detail =$this->report_detail;
        $file = $this->listDownloadFiles('report_file');
        $image = $this->listDownloadFiles('images');
        if($file=='')
            $file = '-';
        if($image=='')
            $image = '-';
        echo "รายละเอียด : ".$detail.' <br>เอกสาร : '.$file.' รูปภาพ : '.$image;
    }

}
