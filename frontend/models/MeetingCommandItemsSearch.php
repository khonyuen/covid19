<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MeetingCommandItems;

/**
 * frontend\models\MeetingCommandItemsSearch represents the model behind the search form about `common\models\MeetingCommandItems`.
 */
 class MeetingCommandItemsSearch extends MeetingCommandItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'item_urgency', 'command_id', 'created_by', 'updated_by'], 'integer'],
            [['item_detail', 'item_enddate', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingCommandItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'item_id' => $this->item_id,
            'item_urgency' => $this->item_urgency,
            'item_enddate' => $this->item_enddate,
            'command_id' => $this->command_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'item_detail', $this->item_detail]);

        return $dataProvider;
    }
}
