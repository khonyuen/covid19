<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Meeting;
use yii\db\Expression;
use yii\db\Query;

/**
 * frontend\models\MeetingSearch represents the model behind the search form about `common\models\Meeting`.
 */
 class MeetingSearch extends Meeting
{

//    public $_urgency;
//    public $_e;
//    public $_m;
//    public $_l;
//    public $_tot;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'meeting_status', 'created_user', 'updated_user'], 'integer'],
            [['meeting_name', 'meeting_no', 'meeting_date', 'meeting_time', 'created_at', 'updated_at','_urgency','_e','_m','_l','_tot'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Meeting::find()->select([
            'meeting.*',
            '_l',
            '_m',
            '_e',
            '_commands'
        ]); //->joinWith('meetingCommands');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->meeting_status==''){
            $this->meeting_status = 1;
        }

        $command = (new Query())->select([
            'meeting_id',
            new Expression('COUNT(meeting_id) as _tot'),
            new Expression('GROUP_CONCAT(command_id,\',\',command_name,\',\',commander_name,\',\',command_enddate,\',\',command_urgency  SEPARATOR \':\' ) as _commands'),
            new Expression('COUNT(if(command_urgency=1,1,null)) as _e'),
            new Expression('COUNT(if(command_urgency=2,1,null)) as _m'),
            new Expression('COUNT(if(command_urgency=3,1,null)) as _l'),
        ])->from('meeting_command')->groupBy('meeting_id');

        $query->andFilterWhere([
            'meeting_id' => $this->meeting_id,
            'meeting_date' => $this->meeting_date,
            'meeting_time' => $this->meeting_time,
            'meeting_status' => $this->meeting_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_user' => $this->created_user,
            'updated_user' => $this->updated_user,
        ]);

        $query->andFilterWhere(['like', 'meeting_name', $this->meeting_name])
            ->andFilterWhere(['like', 'meeting_no', $this->meeting_no]);

        $query->leftJoin(['command'=>$command],'command.meeting_id = meeting.meeting_id');

        return $dataProvider;
    }
}
