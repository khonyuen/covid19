<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property string $off_id
 * @property string|null $names
 * @property string|null $names_orgi
 * @property string|null $off_type
 * @property string|null $changwat
 * @property string|null $amphur
 * @property string|null $tambon
 * @property string|null $mooban
 * @property string|null $moo
 * @property string|null $cup_code
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['off_id'], 'required'],
            [['off_id', 'cup_code'], 'string', 'max' => 5],
            [['names', 'names_orgi'], 'string', 'max' => 100],
            [['off_type', 'changwat', 'moo'], 'string', 'max' => 2],
            [['amphur'], 'string', 'max' => 4],
            [['tambon'], 'string', 'max' => 6],
            [['mooban'], 'string', 'max' => 9],
            [['off_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'off_id' => 'Off ID',
            'names' => 'Names',
            'names_orgi' => 'Names Orgi',
            'off_type' => 'Off Type',
            'changwat' => 'Changwat',
            'amphur' => 'Amphur',
            'tambon' => 'Tambon',
            'mooban' => 'Mooban',
            'moo' => 'Moo',
            'cup_code' => 'Cup Code',
        ];
    }
}
