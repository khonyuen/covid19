<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\CmTarget;

/**
 * CmTargetSearch represents the model behind the search form of `frontend\models\CmTarget`.
 */
class CmTargetSearch extends CmTarget
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['targetgroup_id', 'status'], 'integer'],
            [['targetname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmTarget::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'targetgroup_id' => $this->targetgroup_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'targetname', $this->targetname]);

        return $dataProvider;
    }
}
