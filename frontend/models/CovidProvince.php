<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\CovidProvince as BaseCovidProvince;

/**
 * This is the model class for table "covid_province".
 */
class CovidProvince extends BaseCovidProvince
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status'], 'integer'],
            [['province_name_detail'], 'string', 'max' => 255]
        ]);
    }
	
}
