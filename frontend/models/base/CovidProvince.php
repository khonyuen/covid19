<?php

namespace frontend\models\base;

use Yii;

/**
 * This is the base model class for table "covid_province".
 *
 * @property integer $province_id
 * @property string $province_name_detail
 * @property integer $status
 */
class CovidProvince extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['province_name_detail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'covid_province';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'province_id' => 'Province ID',
            'province_name_detail' => 'Province Name Detail',
            'status' => 'Status',
        ];
    }


    /**
     * @inheritdoc
     * @return \frontend\models\CovidProvinceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\models\CovidProvinceQuery(get_called_class());
    }
}
