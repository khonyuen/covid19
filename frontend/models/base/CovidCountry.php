<?php

namespace frontend\models\base;

use Yii;

/**
 * This is the base model class for table "covid_country".
 *
 * @property string $covid_country_id
 * @property string $covid_country_name
 * @property integer $status
 */
class CovidCountry extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['covid_country_id'], 'required'],
            [['status'], 'integer'],
            [['covid_country_id'], 'string', 'max' => 10],
            [['covid_country_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'covid_country';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'covid_country_id' => 'Covid Country ID',
            'covid_country_name' => 'Covid Country Name',
            'status' => 'Status',
        ];
    }


    /**
     * @inheritdoc
     * @return \frontend\models\CovidCountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\models\CovidCountryQuery(get_called_class());
    }
}
