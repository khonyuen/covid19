<?php

namespace frontend\models\base;

use Yii;

/**
 * This is the base model class for table "covid_situations".
 *
 * @property integer $covid_id
 * @property string $covid_date
 * @property integer $pui_tot
 * @property integer $pui_active
 * @property integer $high_risk
 * @property string $detail
 * @property integer $no_infection
 * @property integer $infection_tot
 * @property integer $wait_result
 * @property string $last_infection_date
 * @property integer $rs_in_hosp
 * @property integer $rs_cure
 * @property string $risk_country
 * @property integer $risk_country_tot
 * @property integer $risk_country_due
 * @property integer $risk_country_remain
 * @property integer $epidemic_country_due
 * @property integer $epidemic_country_remain
 * @property integer $epidemic_country_hq
 * @property integer $epidemic_country_lq
 * @property integer $from_bangkok_other_tot
 * @property integer $from_bangkok_other_due
 * @property integer $from_bangkok_other_remain
 * @property string $from_bangkok_other_lq15
 * @property string $from_bangkok_other_hq
 * @property integer $labor_tot
 * @property integer $labor_due
 * @property integer $labor_remain
 * @property integer $labor_lao
 * @property integer $lq_all
 * @property integer $hq_all
 * @property integer $created_by
 * @property string $created_at
 */
class CovidSituations extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['covid_date', 'last_infection_date', 'created_at'], 'safe'],
            [['pui_tot', 'pui_active', 'high_risk', 'no_infection', 'infection_tot', 'wait_result', 'rs_in_hosp', 'rs_cure', 'risk_country_tot', 'risk_country_due', 'risk_country_remain', 'epidemic_country_due', 'epidemic_country_remain', 'epidemic_country_hq', 'epidemic_country_lq', 'from_bangkok_other_tot', 'from_bangkok_other_due', 'from_bangkok_other_remain', 'labor_tot', 'labor_due', 'labor_remain', 'labor_lao', 'lq_all', 'hq_all', 'created_by'], 'integer'],
            [['detail', 'risk_country', 'from_bangkok_other_lq15', 'from_bangkok_other_hq'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'covid_situations';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'covid_id' => 'Covid ID',
            'covid_date' => 'ประจำวันที่',
            'pui_tot' => 'ผู้เข้าเกณฑ์เฝ้าระวังฯ สะสม',
            'pui_active' => 'ผู้มีอาการเข้าเกณฑ์ PUI สะสม',
            'high_risk' => 'ผู้สัมผัสเสี่ยงสูง',
            'detail' => 'รายละเอียด',
            'no_infection' => 'ไม่พบเชื้อ',
            'infection_tot' => 'พบเชื้อสะสม',
            'wait_result' => 'รอผลตรวจ',
            'last_infection_date' => 'พบเชื้อล่าสุดวันที่',
            'rs_in_hosp' => 'กำลังรักษาอยู่ในโรงพยาบาล',
            'rs_cure' => 'รักษาหายกลับบ้าน',
            'risk_country' => 'เขตติดโรคติดต่ออันตราย',
            'risk_country_tot' => 'ทั้งหมด',
            'risk_country_due' => 'เฝ้าระวังรบ',
            'risk_country_remain' => 'คงเหลือ',
            'epidemic_country_due' => 'ประเทศที่มีการระบาดต่อเนื่อง เฝ้าระวังครบ',
            'epidemic_country_remain' => 'คงเหลือ',
            'epidemic_country_hq' => 'HQ',
            'epidemic_country_lq' => 'LQ',
            'from_bangkok_other_tot' => 'กทม ทั้งหมด',
            'from_bangkok_other_due' => 'เฝ้าระวังครบ',
            'from_bangkok_other_remain' => 'คงเหลือ',
            'from_bangkok_other_lq15' => 'LQ',
            'from_bangkok_other_hq' => 'HQ',
            'labor_tot' => 'แรงงานต่างด้าว ทั้งหมด',
            'labor_due' => 'เฝ้าระวังครบ',
            'labor_remain' => 'คงเหลือ',
            'labor_lao' => 'แรงงานลาว',
            'lq_all' => 'กักตัวในสถานที่ที่รัฐจัดให้',
            'hq_all' => 'กักตัวที่บ้าน',
        ];
    }


    /**
     * @inheritdoc
     * @return \frontend\models\CovidSituationsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \frontend\models\CovidSituationsQuery(get_called_class());
    }
}
