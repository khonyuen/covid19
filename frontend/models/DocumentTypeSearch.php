<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DocumentType;
use yii\db\Expression;

/**
 * DocumentTypeSearch represents the model behind the search form of `common\models\DocumentType`.
 */
class DocumentTypeSearch extends DocumentType
{
    public $tot_doc;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctype_id'], 'integer'],
            [['doctype_name', 'doctype_icon','tot_doc'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DocumentType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'doctype_id' => $this->doctype_id,
        ]);

        $query->andFilterWhere(['like', 'doctype_name', $this->doctype_name])
            ->andFilterWhere(['like', 'doctype_icon', $this->doctype_icon]);

        return $dataProvider;
    }

    public function searchDocByType()
    {
        $query = DocumentType::find()->joinWith('documents');
        $query->select([
            'doctype_id',
            'doctype_name',
            'doctype_icon',
            new Expression('Count(document.document_id) AS tot_doc'),
        ]);

        $dataPvd = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $query->groupBy('doctype_id,doctype_name,doctype_icon');
        $query->orderBy('tot_doc desc');
        return $dataPvd;
    }
}
