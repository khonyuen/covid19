<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MeetingCommand;

/**
 * frontend\models\MeetingCommandSearch represents the model behind the search form about `common\models\MeetingCommand`.
 */
 class MeetingCommandSearch extends MeetingCommand
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['command_id', 'command_status', 'command_urgency', 'meeting_id', 'created_user', 'updated_user'], 'integer'],
            [['command_name', 'commander_name', 'command_enddate', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingCommand::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'command_id' => $this->command_id,
            'command_status' => $this->command_status,
            'command_urgency' => $this->command_urgency,
            'command_enddate' => $this->command_enddate,
            'meeting_id' => $this->meeting_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_user' => $this->created_user,
            'updated_user' => $this->updated_user,
        ]);

        $query->andFilterWhere(['like', 'command_name', $this->command_name])
            ->andFilterWhere(['like', 'commander_name', $this->commander_name]);

        return $dataProvider;
    }
}
