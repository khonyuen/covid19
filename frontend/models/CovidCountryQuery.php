<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[CovidCountry]].
 *
 * @see CovidCountry
 */
class CovidCountryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CovidCountry[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CovidCountry|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
