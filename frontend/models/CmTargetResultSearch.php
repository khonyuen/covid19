<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * CmTargetResultSearch represents the model behind the search form of `frontend\models\CmTargetResult`.
 */
class CmTargetResultSearch extends CmTargetResult
{

    public $_tg_date1;
    public $_tg_date2;
    public $targetnames;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tg_id', 'tg_num', 'tg_rs', 'tg_l1', 'tg_l2', 'tg_l3', 'tg_l4', 'tg_l5', 'created_user'], 'integer'],
            [['tg_date', 'tg_amphur', 'created_at', '_tg_date1', '_tg_date2'], 'safe'],
            [['tg_amphur', 'tg_date','targetnames'], 'safe', 'on' => 'searchTg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmTargetResult::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tg_id' => $this->tg_id,
            'tg_date' => $this->tg_date,
            'tg_num' => $this->tg_num,
            'tg_rs' => $this->tg_rs,
            'tg_l1' => $this->tg_l1,
            'tg_l2' => $this->tg_l2,
            'tg_l3' => $this->tg_l3,
            'tg_l4' => $this->tg_l4,
            'tg_l5' => $this->tg_l5,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
        ]);

        $query->andFilterWhere(['like', 'tg_amphur', $this->tg_amphur]);

        return $dataProvider;
    }

    public function searchReportPv($params)
    {
        //print_r($params);
        $query = CmTargetResult::find()->joinWith(['target']);

        $query->select([
            'cm_target.targetgroup_id',
            new Expression('cm_target.targetname as targetnames'),
            #crs.tg_amphur,
            new Expression('sum( cm_target_result.tg_num ) tg_num'),
            new Expression('sum( cm_target_result.tg_rs ) tg_rs'),
            new Expression('sum( cm_target_result.tg_l1 ) tg_l1'),
            new Expression('sum( cm_target_result.tg_l2 ) tg_l2'),
            new Expression('sum( cm_target_result.tg_l3 ) tg_l3'),
            new Expression('sum( cm_target_result.tg_l4 ) tg_l4'),
            new Expression('sum( cm_target_result.tg_l5 ) tg_l5'),
        ]);

        // add conditions that should always apply here
        $this->load($params);

        /*if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/

        //echo $this->tg_amphur;
        if($this->tg_amphur!='49'){
            $query->filterWhere([
                'tg_amphur'=>$this->tg_amphur
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cm_target.status' => 1
        ]);

        if($this->tg_date){
            $d = explode('ถึง',$this->tg_date);
            $this->_tg_date1 = trim($d[0]);
            $this->_tg_date2 = trim($d[1]);
        }

        $query->andFilterWhere(
            ['between','cm_target_result.tg_date',$this->_tg_date1,$this->_tg_date2]
        );

        $query->groupBy(['cm_target.targetgroup_id']);
        //$query->orderBy('cm_target_result.tg_target_id');
        //$query->andFilterWhere(['like', 'tg_amphur', $this->tg_amphur]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);



        return $dataProvider;
    }

    public function searchReportDashboard(){
        /**
         * SELECT
        #cm_target_result.tg_target_id,
        MONTH(cm_target_result.tg_date) as m,
        Sum(cm_target_result.tg_num) AS tot,
        Sum(cm_target_result.tg_rs) AS rs,
        Sum(cm_target_result.tg_l1) AS l1,
        Sum(cm_target_result.tg_l2) AS l2,
        Sum(cm_target_result.tg_l3) AS l3,
        Max(cm_target_result.tg_l4) as l4,
        Sum(cm_target_result.tg_l5) as l5
        FROM
        cm_target_result
        GROUP BY
        MONTH(cm_target_result.tg_date)
        ORDER BY  MONTH(cm_target_result.tg_date) asc
         */

        $query = new Query();
        $query->select([
           new Expression('DATE_FORMAT(cm_target_result.tg_date,\'%Y-%m\') as m'),
           new Expression('Sum(cm_target_result.tg_num) AS tot'),
           new Expression('Sum(cm_target_result.tg_rs) AS rs'),
           new Expression('Sum(cm_target_result.tg_l1) AS l1'),
           new Expression('Sum(cm_target_result.tg_l2) AS l2'),
           new Expression('Sum(cm_target_result.tg_l3) AS l3'),
           new Expression('Sum(cm_target_result.tg_l4) AS l4'),
           new Expression('Sum(cm_target_result.tg_l5) AS l5'),
        ])->from('cm_target_result');

        $query->groupBy(new Expression('DATE_FORMAT(cm_target_result.tg_date,\'%Y-%m\')'))
            ->orderBy(new Expression('DATE_FORMAT(cm_target_result.tg_date,\'%Y-%m\') asc'));

        return new ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => false
        ]);
    }
}
