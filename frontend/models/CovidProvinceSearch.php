<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\CovidProvince;

/**
 * frontend\models\CovidProvinceSearch represents the model behind the search form about `frontend\models\CovidProvince`.
 */
 class CovidProvinceSearch extends CovidProvince
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province_id', 'status'], 'integer'],
            [['province_name_detail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CovidProvince::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'province_id' => $this->province_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'province_name_detail', $this->province_name_detail]);

        return $dataProvider;
    }
}
