<?php

namespace frontend\models;

use common\models\Document;
use common\models\DocumentType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * DocumentSearch represents the model behind the search form of `common\models\Document`.
 */
class DocumentSearch extends Document
{
    public $documentTypeName;
    public $documentTypeIcon;
    public $documentTypeTitle;
    public $missionName;
    public $statusDoc;
    public $createBy;
    public $query;

    public $tot_doc;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_id', 'document_type', 'status', 'has_thumbnail', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['title', 'description', 'tags', 'document_name', 'documentTypeName', 'missionName', 'statusDoc', 'query', 'documentTypeIcon', 'createBy', 'documentTypeTitle','tot_doc'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Document::find()->joinWith(['documentType', 'createdBy', 'updatedBy', 'mission']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        if($params['id']!=null)
//            $this->mission_id = $params['id'];

        if($this->document_type!=''){
            $this->documentTypeName = $this->document_type;
        }else if ($this->documentTypeName == null) {
            $this->documentTypeName = "";
        } else if ($params['type'] != null && $this->documentTypeName == null) {
            $this->documentTypeName = $params['type'];
        }

        if ($this->mission_id != '' && $this->missionName == '') {
            $query->andFilterWhere([
                'document.mission_id' => $this->mission_id,
            ]);
        } else {
            $query->andFilterWhere([
                'document.mission_id' => $this->missionName,
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'document_id' => $this->document_id,
            'document_type' => $this->documentTypeName,
            'status' => $this->status,
            'has_thumbnail' => $this->has_thumbnail,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
//            'mission_id' => \Yii::$app->user->identity->mission_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'document_name', $this->document_name]);

        return $dataProvider;
    }

    public function searchDocMission($mission)
    {
        $query = Document::find()->joinWith(['documentType', 'createdBy', 'updatedBy', 'mission']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);


        // grid filtering conditions
        $query->andFilterWhere([
//            'document_id' => $this->document_id,
//            'document_type' => $this->documentTypeName,
//            'status' => $this->status,
//            'has_thumbnail' => $this->has_thumbnail,
//            'created_by' => $this->created_by,
//            'created_at' => $this->created_at,
//            'updated_by' => $this->updated_by,
//            'updated_at' => $this->updated_at,
            'document.mission_id' => $mission,
        ]);

        return $dataProvider;
    }

    public function searchQuick($params)
    {
        $query = Document::find()->joinWith(['documentType', 'createdBy', 'updatedBy']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //$this->load($params);

        // grid filtering conditions
//        $query->andFilterWhere([
//            'document_id' => $this->query,
//            'document_type' => $this->query,
//            'status' => $this->query,
//            'has_thumbnail' => $this->query,
//            'created_by' => $this->query,
//            'created_at' => $this->query,
//            'updated_by' => $this->query,
//            'updated_at' => $this->query,
//            'mission.mission_name' => $this->query,
//            'mission_id' => \Yii::$app->user->identity->mission_id,
//        ]);
        $this->query = $params;

        $query->andFilterWhere(['like', 'title', $this->query])
            ->andFilterWhere(['like', 'description', $this->query])
            ->andFilterWhere(['like', 'tags', $this->query])
            ->andFilterWhere(['like', 'document_name', $this->query]);

        return $dataProvider;
    }


}
