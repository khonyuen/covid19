<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MeetingReportComment;

/**
 * frontend\models\MeetingReportCommentSearch represents the model behind the search form about `common\models\MeetingReportComment`.
 */
 class MeetingReportCommentSearch extends MeetingReportComment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id', 'report_id', 'created_by', 'updated_by'], 'integer'],
            [['comment_detail', 'comment_date', 'comment_name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingReportComment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comment_id' => $this->comment_id,
            'report_id' => $this->report_id,
            'comment_date' => $this->comment_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'comment_detail', $this->comment_detail])
            ->andFilterWhere(['like', 'comment_name', $this->comment_name]);

        return $dataProvider;
    }
}
