<?php

namespace frontend\models;

use common\models\BaseActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "amphur".
 *
 * @property string $ampurcode
 * @property string|null $ampurname
 * @property string $ampurcodefull
 * @property string $changwatcode
 * @property string|null $flag_status สถานนะของพื้นที่ 0=ปกติ 1=เลิกใช้(แยก/ย้ายไปพื้นที่อื่น)
 *
 * @property CmTargetResult[] $cmTargetResults
 */
class Amphur extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amphur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ampurcode', 'ampurcodefull', 'changwatcode'], 'required'],
            [['ampurcode', 'changwatcode'], 'string', 'max' => 2],
            [['ampurname'], 'string', 'max' => 255],
            [['ampurcodefull'], 'string', 'max' => 4],
            [['flag_status'], 'string', 'max' => 1],
            [['ampurcode', 'ampurcodefull', 'changwatcode'], 'unique', 'targetAttribute' => ['ampurcode', 'ampurcodefull', 'changwatcode']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ampurcode' => 'Ampurcode',
            'ampurname' => 'Ampurname',
            'ampurcodefull' => 'Ampurcodefull',
            'changwatcode' => 'Changwatcode',
            'flag_status' => 'สถานนะของพื้นที่ 0=ปกติ 1=เลิกใช้(แยก/ย้ายไปพื้นที่อื่น)',
        ];
    }

    /**
     * Gets query for [[CmTargetResults]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCmTargetResults()
    {
        return $this->hasMany(CmTargetResult::className(), ['tg_amphur' => 'ampurcode']);
    }

    public static function getMukAmphur($op = 0){
        $model = self::find()->where(['changwatcode'=>'49'])->asArray()->all();
        $a = ArrayHelper::map($model,'ampurcodefull','ampurname');
        if($op==0){
            $option = [''=>'เลือกอำเภอ'];
        }else{
            $option = ['49'=>'ระดับจังหวัด'];
        }

        $b= ArrayHelper::merge($option,$a);
        return $b;
    }

    public static function getMukAmphurList($op = 0){
        $model = self::find()->where(['changwatcode'=>'49'])->asArray()->all();
        $a = ArrayHelper::map($model,'ampurcodefull','ampurname');

        return $a;
    }
}
