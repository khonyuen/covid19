<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MeetingResponsible;

/**
 * frontend\models\MeetingResponsibleSearch represents the model behind the search form about `common\models\MeetingResponsible`.
 */
 class MeetingResponsibleSearch extends MeetingResponsible
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['responsible_id', 'mission_id', 'item_id', 'responsible_status', 'responsible_user'], 'integer'],
            [['updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingResponsible::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'responsible_id' => $this->responsible_id,
            'mission_id' => $this->mission_id,
            'item_id' => $this->item_id,
            'responsible_status' => $this->responsible_status,
            'responsible_user' => $this->responsible_user,
            'updated_date' => $this->updated_date,
        ]);

        return $dataProvider;
    }
}
