<?php

namespace frontend\models;

use common\models\Mission;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * MissionSearch represents the model behind the search form of `common\models\Mission`.
 */
class MissionSearch extends Mission
{
    public $tot_doc;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mission_id', 'order', 'status'], 'integer'],
            [['mission_name', 'mission_desc', 'mission_pic','tot_doc'], 'safe'],
        ];
    }

    /**
     * @return array|Mission[]
     */
    public function getAllMission($type=null)
    {
        return Mission::find()->where(['status' => 1])->andFilterWhere(['mission_type'=>$type])->all();
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mission_id' => $this->mission_id,
            'order' => $this->order,
            'status' => 1,
        ]);

        $query->andFilterWhere(['like', 'mission_name', $this->mission_name])
            ->andFilterWhere(['like', 'mission_desc', $this->mission_desc])
            ->andFilterWhere(['like', 'mission_pic', $this->mission_pic]);

        return $dataProvider;
    }

    public function searchByMission()
    {
        $query = Mission::find()->joinWith('documents');

        $query->select([
            'mission.mission_id',
            'mission_name',
            'mission_pic',
            new Expression('Count(document.document_id) AS tot_doc'),
        ]);

        $dataPvd = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $query->andFilterWhere([
            'mission.status' => 1,
        ]);

        $query->groupBy('mission.mission_id,mission_name,mission_pic');
        $query->orderBy('tot_doc desc');

        return $dataPvd;
    }
}
