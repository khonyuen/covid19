<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\CovidCountry;

/**
 * frontend\models\CovidCountrySearch represents the model behind the search form about `frontend\models\CovidCountry`.
 */
 class CovidCountrySearch extends CovidCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['covid_country_id', 'covid_country_name'], 'safe'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CovidCountry::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'covid_country_id', $this->covid_country_id])
            ->andFilterWhere(['like', 'covid_country_name', $this->covid_country_name]);

        return $dataProvider;
    }
}
