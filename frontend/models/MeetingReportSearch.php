<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MeetingReport;

/**
 * frontend\models\MeetingReportSearch represents the model behind the search form about `common\models\MeetingReport`.
 */
 class MeetingReportSearch extends MeetingReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'responsible_id', 'created_by', 'updated_by'], 'integer'],
            [['report_detail', 'report_date', 'report_file', 'report_images', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'report_id' => $this->report_id,
            'responsible_id' => $this->responsible_id,
            'report_date' => $this->report_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'report_detail', $this->report_detail])
            ->andFilterWhere(['like', 'report_file', $this->report_file])
            ->andFilterWhere(['like', 'report_images', $this->report_images]);

        return $dataProvider;
    }
}
