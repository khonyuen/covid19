<?php

namespace frontend\models;

use common\models\BaseActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cm_target".
 *
 * @property int $targetgroup_id กลุ่มเป้าหมาย
 * @property string $targetname กลุ่มเป้าหมาย
 * @property int $status สถานะ
 * @property CmTargetResult $targetResult
 */
class CmTarget extends BaseActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_target';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['targetname'], 'required'],
            [['status'], 'integer'],
            [['targetname'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'targetgroup_id' => 'กลุ่มเป้าหมาย',
            'targetname' => 'กลุ่มเป้าหมาย',
            'status' => 'สถานะ',
        ];
    }

    public static function getList(){
        $models = self::find()->where(['status' => 1])->indexBy('targetgroup_id')->asArray()->all();
        return ArrayHelper::map($models,'targetgroup_id','targetname');
    }

    /**
     * Gets query for [[Cmtargetid]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTargetResult()
    {
        return $this->hasOne(\frontend\models\CmTargetResult::className(), ['tg_target_id' => 'targetgroup_id']);
    }
}
