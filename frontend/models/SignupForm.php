<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;

    public $repeat_password;
    public $title;
    public $firstname;
    public $lastname;
    public $mission_id;
    public $position;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 4, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            [['password', 'repeat_password'], 'required'],
            [['password', 'repeat_password'], 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'repeat_password'],

            [['title', 'mission_id', 'firstname', 'lastname', 'position'], 'string'],
            [['title', 'mission_id', 'firstname', 'lastname', 'position'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'รหัสผ่าน',
            'repeat_password' => 'ยืนยันรหัสผ่าน',
            'title' => 'คำนำหน้า',
            'firstname' => 'ชื่อ',
            'lastname' => 'สกุล',
            'position' => 'ตำแหน่ง',
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

//        print_r($this->getErrorSummary(true));
//
//        print_r($this->attributes);

        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->status = '10';
        $user->title = $this->title;
        $user->firstname = $this->firstname;
        $user->lastname = $this->lastname;
        $user->position = $this->position;
        $user->mission_id = $this->mission_id;
        $user->validate();
//        print_r($user->getErrorSummary(true));

        return $user->save(); // && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
