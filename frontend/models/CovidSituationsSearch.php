<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\CovidSituations;

/**
 * frontend\models\CovidSituationsSearch represents the model behind the search form about `frontend\models\CovidSituations`.
 */
 class CovidSituationsSearch extends CovidSituations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['covid_id', 'pui_tot', 'pui_active', 'high_risk', 'no_infection', 'infection_tot', 'wait_result', 'rs_in_hosp', 'rs_cure', 'risk_country_tot', 'risk_country_due', 'risk_country_remain', 'epidemic_country_due', 'epidemic_country_remain', 'epidemic_country_hq', 'epidemic_country_lq', 'from_bangkok_other_tot', 'from_bangkok_other_due', 'from_bangkok_other_remain', 'labor_tot', 'labor_due', 'labor_remain', 'labor_lao', 'lq_all', 'hq_all', 'created_by'], 'integer'],
            [['covid_date', 'detail', 'last_infection_date', 'risk_country', 'from_bangkok_other_lq15', 'from_bangkok_other_hq', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CovidSituations::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'covid_id' => $this->covid_id,
            'covid_date' => $this->covid_date,
            'pui_tot' => $this->pui_tot,
            'pui_active' => $this->pui_active,
            'high_risk' => $this->high_risk,
            'no_infection' => $this->no_infection,
            'infection_tot' => $this->infection_tot,
            'wait_result' => $this->wait_result,
            'last_infection_date' => $this->last_infection_date,
            'rs_in_hosp' => $this->rs_in_hosp,
            'rs_cure' => $this->rs_cure,
            'risk_country_tot' => $this->risk_country_tot,
            'risk_country_due' => $this->risk_country_due,
            'risk_country_remain' => $this->risk_country_remain,
            'epidemic_country_due' => $this->epidemic_country_due,
            'epidemic_country_remain' => $this->epidemic_country_remain,
            'epidemic_country_hq' => $this->epidemic_country_hq,
            'epidemic_country_lq' => $this->epidemic_country_lq,
            'from_bangkok_other_tot' => $this->from_bangkok_other_tot,
            'from_bangkok_other_due' => $this->from_bangkok_other_due,
            'from_bangkok_other_remain' => $this->from_bangkok_other_remain,
            'labor_tot' => $this->labor_tot,
            'labor_due' => $this->labor_due,
            'labor_remain' => $this->labor_remain,
            'labor_lao' => $this->labor_lao,
            'lq_all' => $this->lq_all,
            'hq_all' => $this->hq_all,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'detail', $this->detail])
            ->andFilterWhere(['like', 'risk_country', $this->risk_country])
            ->andFilterWhere(['like', 'from_bangkok_other_lq15', $this->from_bangkok_other_lq15])
            ->andFilterWhere(['like', 'from_bangkok_other_hq', $this->from_bangkok_other_hq]);

        return $dataProvider;
    }
}
