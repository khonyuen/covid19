<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\CovidSituations as BaseCovidSituations;

/**
 * This is the model class for table "covid_situations".
 */
class CovidSituations extends BaseCovidSituations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['covid_date', 'last_infection_date', 'created_at'], 'safe'],
            [['pui_tot', 'pui_active', 'high_risk', 'no_infection', 'infection_tot', 'wait_result', 'rs_in_hosp', 'rs_cure', 'risk_country_tot', 'risk_country_due', 'risk_country_remain', 'epidemic_country_due', 'epidemic_country_remain', 'epidemic_country_hq', 'epidemic_country_lq', 'from_bangkok_other_tot', 'from_bangkok_other_due', 'from_bangkok_other_remain', 'labor_tot', 'labor_due', 'labor_remain', 'labor_lao', 'lq_all', 'hq_all', 'created_by'], 'integer'],
            [['detail', 'risk_country', 'from_bangkok_other_lq15', 'from_bangkok_other_hq'], 'string']
        ]);
    }
	
}
