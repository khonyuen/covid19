<?php

namespace frontend\models;

use Yii;
use \frontend\models\base\CovidCountry as BaseCovidCountry;

/**
 * This is the model class for table "covid_country".
 */
class CovidCountry extends BaseCovidCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['covid_country_id'], 'required'],
            [['status'], 'integer'],
            [['covid_country_id'], 'string', 'max' => 10],
            [['covid_country_name'], 'string', 'max' => 255]
        ]);
    }
	
}
