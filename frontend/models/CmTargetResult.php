<?php

namespace frontend\models;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\AttributesBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cm_target_result".
 *
 * @property int $tg_id
 * @property string $tg_date วันที่คัดกรอง
 * @property int $tg_target_id กลุ่มเป้าหมาย
 * @property string $tg_amphur อำเภอ
 * @property int $tg_num เป้าหมาย
 * @property int $tg_rs ได้รับการประเมิน
 * @property int $tg_l1 เครียดมาก
 * @property int $tg_l2 มีแนวโน้มซึมเศร้า
 * @property int $tg_l3 ซึมเศร้า
 * @property int $tg_l4 เสี่ยงฆ่าตัวตาย
 * @property int $tg_l5 ติดตาม
 * @property string|null $created_at วันที่บันทึก
 * @property string|null $updated_at วันที่บันทึก
 * @property string|null $updated_user วันที่บันทึก
 * @property int|null $created_user ผู้บันทึก
 *
 * @property Amphur $tgAmphur
 * @property CmTarget $target
 * @property \common\models\User $createdUser
 */
class CmTargetResult extends BaseActiveRecord
{
    public $_tggroupname;
    public $targetnames;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cm_target_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tg_date', 'tg_amphur','tg_target_id','tg_num', 'tg_rs', 'tg_l1', 'tg_l2', 'tg_l3', 'tg_l4', 'tg_l5'], 'required'],
            [['tg_num', 'tg_rs', 'tg_l1', 'tg_l2', 'tg_l3', 'tg_l4', 'tg_l5'],'default','value' => 0],
            [['tg_date', 'created_at','updated_at','updated_by'], 'safe'],
            [['tg_target_id','tg_num', 'tg_rs', 'tg_l1', 'tg_l2', 'tg_l3', 'tg_l4', 'tg_l5', 'created_user'], 'number','min'=>0],
            [['tg_amphur'], 'string', 'max' => 6],

            [['tg_amphur'], 'exist', 'skipOnError' => true, 'targetClass' => Amphur::className(), 'targetAttribute' => ['tg_amphur' => 'ampurcodefull']],

            [['tg_target_id','tg_date','tg_amphur'],'unique','targetAttribute' => ['tg_target_id', 'tg_date','tg_amphur'],'message' => 'มีการบันทึกข้อมูลในวันที่นี้แล้ว'],

            [['tg_target_id'], 'exist', 'skipOnError' => true, 'targetClass' => CmTarget::className(), 'targetAttribute' => ['tg_target_id' => 'targetgroup_id']],

            [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['created_user' => 'id']],
            [['tg_rs', 'tg_l1', 'tg_l2', 'tg_l3', 'tg_l4', 'tg_l5'],'compare','compareAttribute' => 'tg_num','operator' => '<=','type' => 'number','enableClientValidation' => false],
            [['_tggroupname','_tg_date1','_tg_date2','targetnames'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tg_id' => 'Tg ID',
            'tg_target_id' => 'กลุ่มเป้าหมาย',
            'tg_date' => 'วันที่คัดกรอง',
            'tg_amphur' => 'อำเภอ',
            'tg_num' => 'เป้าหมาย',
            'tg_rs' => 'ได้รับการประเมิน',
            'tg_l1' => 'เครียดมาก',
            'tg_l2' => 'มีแนวโน้มซึมเศร้า',
            'tg_l3' => 'ซึมเศร้า',
            'tg_l4' => 'เสี่ยงฆ่าตัวตาย',
            'tg_l5' => 'ติดตาม',
            'created_at' => 'วันที่บันทึก',
            'update_at' => 'วันที่บันทึก',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้บันทึก',
            '_tg_date1'=>'เริ่มต้น',
            '_tg_date2'=>'สิ้นสุด',
            'targetnames'=>'กลุ่มเป้าหมาย'
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }

    /**
     * Gets query for [[TgAmphur]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTgAmphur()
    {
        return $this->hasOne(Amphur::className(), ['ampurcodefull' => 'tg_amphur']);
    }

    /**
     * Gets query for [[CreatedUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'created_user']);
    }

    /**
     * Gets query for [[CreatedUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarget()
    {
        return $this->hasOne(\frontend\models\CmTarget::className(), ['targetgroup_id' => 'tg_target_id']);
    }

    public function getTargetnames(){
        return $this->target->targetname;
    }
}
