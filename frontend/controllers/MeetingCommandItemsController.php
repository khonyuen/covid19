<?php

namespace frontend\controllers;

use common\models\MeetingCommand;
use common\models\MeetingCommandItems;
use common\models\MeetingResponsible;
use frontend\models\MeetingCommandItemsSearch;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * MeetingCommandItemsController implements the CRUD actions for MeetingCommandItems model.
 */
class MeetingCommandItemsController extends Controller
{
    public static function genResponsible(MeetingCommandItems $meetingCommandItems)
    {
        $res = $meetingCommandItems->meetingResponsibles;
        $txt = "";
        foreach ($res as $item) {
            if($item->responsible_status==1){
                $style = 'warning';
            }elseif($item->responsible_status==2){
                $style = 'success';
            }elseif($item->responsible_status==3){
                $style = 'dark text-line-through';
            }elseif($item->responsible_status==4){
                $style = 'secondary text-danger';
            }elseif($item->responsible_status==0){
                $style = 'danger blink';
            }
            $blink = "";
            if($item->mission_id==Yii::$app->user->identity->mission_id && $item->responsible_status!=2)
                $blink = 'blink';
            $url = Url::to(['meeting-responsible/view','id'=>$item->responsible_id]);
            $txt .= "<div data-url='{$url}' style='cursor:pointer;' class='lovOpen badge badge-{$style} mr-2 mb-2 {$blink}'>{$item->mission->mission_name}</div>";
        }
        return $txt;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-meeting-responsible',
                            'validate-command-item'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all MeetingCommandItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingCommandItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MeetingCommandItems model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMeetingResponsible = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingResponsibles,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMeetingResponsible' => $providerMeetingResponsible,
        ]);
    }

    /**
     * Finds the MeetingCommandItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingCommandItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingCommandItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new MeetingCommandItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MeetingCommandItems();
        $model->scenario = 'create-assign-item';
        if ($model->load(Yii::$app->request->post())) {
            //$this->CreateDir($model->files_path, 'files');
            //$model->files = $this->uploadMultipleFile($model, null, 'files');
            try {
                $ts = Yii::$app->db->beginTransaction();
                $model->item_enddate = $model::dateEngDb($model->item_enddate);
                if ($model->save()) {
                    $pk = $model->getPrimaryKey();
                    MeetingResponsible::saveRes($model->_responsible, $pk);
                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'บันทึกข้อมูลเอกสารสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['meeting/view', 'id' => $id]),
                    ]);
                } else {
                    $error = $model->getErrorSummary(true);
                    $ts->rollBack();
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาดในการบันทึกข้อมูล ' . implode(',', $error),
                        'status' => 'error',
                        'url' => Url::to(['meeting/view', 'id' => $id]),
                    ]);
                }
            } catch (\Exception $exception) {
                $ts->rollBack();
                throw new HttpException('500', $exception->getMessage());
            }
        }
        $model->command_id = $id;

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MeetingCommandItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'create-assign-item';
        if ($model->load(Yii::$app->request->post())) {
            //$this->CreateDir($model->files_path, 'files');
            //$model->files = $this->uploadMultipleFile($model, null, 'files');
            try {
                $ts = Yii::$app->db->beginTransaction();
                $model->item_enddate = $model::dateEngDb($model->item_enddate);
                if ($model->save()) {
                    $pk = $model->getPrimaryKey();
                    MeetingResponsible::saveRes($model->_responsible, $pk,'update');
                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'บันทึกข้อมูลเอกสารสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['meeting/view', 'id' => $id]),
                    ]);
                } else {
                    $error = $model->getErrorSummary(true);
                    $ts->rollBack();
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาดในการบันทึกข้อมูล ' . implode(',', $error),
                        'status' => 'error',
                        'url' => Url::to(['meeting/view', 'id' => $id]),
                    ]);
                }
            } catch (\Exception $exception) {
                $ts->rollBack();
                throw new HttpException('500', $exception->getMessage());
            }
        }
        //$model->command_id = $id;
        $model->_responsible = (ArrayHelper::getColumn($model->meetingResponsibles, 'mission_id', false));
        $model->item_enddate = $model::dateEngDb($model->item_enddate);
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MeetingCommandItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($id) {
                $model = MeetingCommandItems::deleteAll(['=', 'item_id', $id]);
                $ts->commit();
            }
            return Json::encode([
                'result' => true,
                'msg' => 'ลบข้อมูลเรียบร้อย',
                'options' => [],
            ]);
        } catch (\Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    /**
     * Action to load a tabular form grid
     * for MeetingResponsible
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMeetingResponsible()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingResponsible');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingResponsible', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidateCommandItem()
    {
        $data = Yii::$app->request->post('MeetingCommandItems');
        if (null != $data['item_id']) {
            $model = MeetingCommandItems::findOne($data['item_id']);

        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new MeetingCommandItems();

            $model->scenario = 'create-assign-item';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            //$model->created_by = Yii::$app->user->id;

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
