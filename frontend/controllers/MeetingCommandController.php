<?php

namespace frontend\controllers;

use Yii;
use common\models\MeetingCommand;
use frontend\models\MeetingCommandSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MeetingCommandController implements the CRUD actions for MeetingCommand model.
 */
class MeetingCommandController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-meeting-command-items'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all MeetingCommand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingCommandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MeetingCommand model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMeetingCommandItems = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingCommandItems,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMeetingCommandItems' => $providerMeetingCommandItems,
        ]);
    }

    /**
     * Creates a new MeetingCommand model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MeetingCommand();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->command_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MeetingCommand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->command_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MeetingCommand model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($id) {
                $model = MeetingCommand::deleteAll(['=', 'command_id', $id]);
                $ts->commit();
            }
            return Json::encode([
                'status' => 'success',
                'msg' => 'ลบข้อมูลเรียบร้อย',
                'options' => [],
            ]);
        } catch (\Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'status' => 'false',
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    
    /**
     * Finds the MeetingCommand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingCommand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingCommand::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MeetingCommandItems
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMeetingCommandItems()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingCommandItems');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingCommandItems', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
