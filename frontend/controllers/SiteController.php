<?php

namespace frontend\controllers;

use common\models\LoginForm;
use common\models\Mission;
use frontend\models\CmTargetResultSearch;
use frontend\models\ContactForm;
use frontend\models\DocumentSearch;
use frontend\models\DocumentTypeSearch;
use frontend\models\MissionSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\bootstrap4\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index','index-dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndexSub($id = null, $type_id = null)
    {
        $searchModel = new DocumentSearch();
        $searchModel->mission_id = $id;
        $searchModel->document_type = $type_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexDashboard()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $missionSearch = new MissionSearch();
        $missionProvider = $missionSearch->getAllMission(1);

        return $this->render('dashboard', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'missionProvider' => $missionProvider,
        ]);
    }

    public function actionIndex()
    {
        $documentByMission = new MissionSearch();
        $documentByMissionPvd = $documentByMission->searchByMission();

        /*$searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);*/

        $searchModel1 = new DocumentTypeSearch();
        $documentByDocTypePvd = $searchModel1->searchDocByType();

        $missionSearch = new MissionSearch();
        $missionProvider = $missionSearch->getAllMission();

        $targetRs = new CmTargetResultSearch();
        $targetRsPvd = $targetRs->searchReportDashboard();

        //print_r($targetRsPvd);

        $dataReport = $this->genData($targetRsPvd);

        return $this->render('dashboard-new', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
            'missionProvider' => $missionProvider,
            'docByMissionPvd' => $documentByMissionPvd,
            'docByTypePvd' => $documentByDocTypePvd,
            'targetRs' => $targetRsPvd,
            'data1'=>$dataReport
        ]);
    }

    public function genData(ArrayDataProvider $datas)
    {
        #start 01-2020 to 12-2020
        $startdate = strtotime('2020-01-01');
        $enddate = strtotime('2020-12-01');

//        echo $startdate." ";
//        echo $enddate." ";
//
//        echo $startdate = strtotime('+1 month', $startdate);

        $data = $datas->getModels();

        $list = ArrayHelper::index($data, 'm');
        //var_dump($list);
        $dataLabel = [];
        $sum  = [];
        while ($startdate <= $enddate) {
            $m = date('Y-m', $startdate);
            $mm = date('m', $startdate);
            $dataLabel[$m] = [
                'tot'=>(int)$list[$m]['tot'],
                'rs'=>(int)$list[$m]['rs'],
                'l1'=>(int)$list[$m]['l1'],
                'l2'=>(int)$list[$m]['l2'],
                'l3'=>(int)$list[$m]['l3'],
                'l4'=>(int)$list[$m]['l4'],
                'l5'=>(int)$list[$m]['l5'],
                'label'=>$mm,
            ];

            $sum['tot'] += (int)$list[$m]['tot'];
            $sum['rs'] += (int)$list[$m]['rs'];
            $sum['l1'] += (int)$list[$m]['l1'];
            $sum['l2'] += (int)$list[$m]['l2'];
            $sum['l3'] += (int)$list[$m]['l3'];
            $sum['l4'] += (int)$list[$m]['l4'];
            $sum['l5'] += (int)$list[$m]['l5'];


            $startdate = strtotime('+1 month', $startdate);
        }

        $sum['label']= 'รวม';
        $dataLabel['sum'] = $sum;

        return $dataLabel;
    }

    public function actionQuickSearch()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->searchQuick(Yii::$app->request->getQueryParam('query'));

        return $this->renderPartial('_quick_search', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'query' => Yii::$app->request->getQueryParam('query'),
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'auth';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $signup = new SignupForm();
        $mission = Mission::getList();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['document/index']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
                'signup' => $signup,
                'mission' => $mission,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //$model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'ลงทะเบียนเรียบร้อย สามารถเข้าระบบเพื่อใช้งานได้ทันที.');
            return $this->redirect(['site/login']);
        } else {
            //print_r($model->getErrorSummary(true));
            //exit();
        }
        return $this->redirect(['site/login']);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model,
        ]);
    }
}
