<?php

namespace frontend\controllers;

use common\models\MeetingCommandItems;
use common\models\MeetingCommandLog;
use common\models\MeetingReport;
use common\models\MeetingResponsible;
use frontend\models\MeetingResponsibleSearch;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * MeetingResponsibleController implements the CRUD actions for MeetingResponsible model.
 */
class MeetingResponsibleController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeetingResponsible models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingResponsibleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MeetingResponsible model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMeetingCommandLog = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingCommandLogs,
        ]);
        $providerMeetingReport = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingReports,
        ]);
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
            'providerMeetingCommandLog' => $providerMeetingCommandLog,
            'providerMeetingReport' => $providerMeetingReport,
        ]);
    }

    /**
     * Finds the MeetingResponsible model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingResponsible the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingResponsible::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new MeetingResponsible model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * id of meetingcommanditem
     *
     */
    public function actionCreate($id, $mission_id = null)
    {
        $model = MeetingResponsible::find()->where('mission_id=:mission and item_id=:item', [
            ':mission' => $mission_id, ':item' => $id,
        ])->one();
        $reportModel = new MeetingReport();
        //$model->scenario = 'create-report';
        //$model->item_id = $id;
        //$model->mission_id = $mission_id;
        if ($model->load(Yii::$app->request->post()) && $reportModel->load(Yii::$app->request->post())) {
            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($reportModel->report_path_temp, 'report_path_temp');
                $reportModel->report_file = $this->uploadMultipleFile($reportModel, null, 'report_file');
                $reportModel->report_images = $this->uploadMultipleFile($reportModel, null, 'report_images');

                $log = new MeetingCommandLog();
                $log->old_status = $model->getOldAttribute('responsible_status');
                $log->new_status = $model->responsible_status;

                if ($model->save()) {
                    //$pk = $model->getPrimaryKey();
                    $reportModel->responsible_id = $model->responsible_id;
                    $reportModel->report_date = $model::dateEngDb($reportModel->report_date);
                    $reportModel->save();

                    $log->report_id = $reportModel->getPrimaryKey();
                    $log->responsible_id = $model->responsible_id;
                    $log->updated_at = new Expression('NOW()');
                    $log->save();

                    //var_dump($log->getErrorSummary(true));

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['meeting/view', 'id' => $mission_id]),
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }
        } else {
            $commandItem = MeetingCommandItems::findOne($id);

            $reportProvider = new ActiveDataProvider();
            $reportModel->report_path_temp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            return $this->renderAjax('create', [
                'model' => $model,
                'item' => $commandItem,
                'reportModel' => $reportModel,
            ]);
        }
    }

    public function actionCreateStag($id, $mission_id = null)
    {

        $reportModel = new MeetingReport();
        $reportModel->scenario = 'create-stag';
        //$model->scenario = 'create-report';
        //$model->item_id = $id;
        //$model->mission_id = $mission_id;
        if (Yii::$app->request->isPost && $reportModel->load(Yii::$app->request->post())){
            $model = MeetingResponsible::findOne($reportModel->_responsible_id);

            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($reportModel->report_path_temp, 'report_path_temp');
                $reportModel->report_file = $this->uploadMultipleFile($reportModel, null, 'report_file');
                $reportModel->report_images = $this->uploadMultipleFile($reportModel, null, 'report_images');

                $model->responsible_status = $reportModel->_responsible_status;
                $model->updated_date = new Expression('NOW()');

                $log = new MeetingCommandLog();
                $log->old_status = $model->getOldAttribute('responsible_status');
                $log->new_status = $model->responsible_status;



                if ($model->save()) {
                    //$pk = $model->getPrimaryKey();
                    $reportModel->responsible_id = $model->responsible_id;
                    $reportModel->report_date = $model::dateEngDb($reportModel->report_date);
                    $reportModel->save();

                    $log->report_id = $reportModel->getPrimaryKey();
                    $log->responsible_id = $model->responsible_id;
                    $log->updated_at = new Expression('NOW()');
                    $log->save();

                    //var_dump($log->getErrorSummary(true));

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['meeting/view', 'id' => $mission_id]),
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }
        } else {
            $model = MeetingResponsible::find()->joinWith('mission')->select([
                'responsible_id',
                'meeting_responsible.mission_id','mission_name','item_id','responsible_status'
            ])->where('item_id=:item', [
               ':item' => $id,
            ])->all();
            $commandItem = MeetingCommandItems::findOne($id);


            $reportModel->report_path_temp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            return $this->renderAjax('createStag', [
                'model' => $model,
                'item' => $commandItem,
                'reportModel' => $reportModel,
            ]);
        }
    }

    private function CreateDir($folderName, $attribute = 'report_path_temp')
    {
        //echo $folderName;
        $basePath = MeetingReport::getUploadPath('report_file');
        $basePath2 = MeetingReport::getUploadPath('report_images');
        if ($folderName != null && !is_dir($basePath . $folderName)) {

            BaseFileHelper::createDirectory($basePath . $folderName, 0777);
        }
        if ($folderName != null && !is_dir($basePath2 . $folderName)) {

            BaseFileHelper::createDirectory($basePath2 . $folderName, 0777);
        }
        //echo $folderName;
        return;
    }

    private function uploadMultipleFile($model, $tempFile = null, $attribute)
    {
        //echo $attribute;
        $files = [];
        $json = '';
        $tempFile = Json::decode($tempFile);
        $UploadedFiles = UploadedFile::getInstances($model, $attribute);

        //print_r($UploadedFiles);
        //print_r($tempFile);

        if ($UploadedFiles !== null) {
            foreach ($UploadedFiles as $file) {
                try {
                    $oldFileName = $file->basename . '.' . $file->extension;
                    $newFileName = md5($file->basename . time()) . '.' . $file->extension;

                    $file->saveAs(MeetingReport::getUploadPath($attribute) . $model->report_path_temp . '/' .
                        $newFileName);

                    //$fileThumbnailPath = Project::getUploadPath($attribute) . $model->{$attribute . '_ref'} . '/thumbnail';

                    /*@Image::getImagine()->open(Project::getUploadPath($attribute) .
                        $model->{$attribute . '_ref'} . '/' . $newFileName)->thumbnail(new Box(120, 120))->save($fileThumbnailPath . '/' . $newFileName, ['quality' => 90]);*/

                    $files[$newFileName] = $oldFileName;
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            $json = Json::encode(ArrayHelper::merge($tempFile, $files));
        } else {
            $json = $tempFile;
        }
        //print_r($json);
        return $json;
    }

    public function actionDeletefile($id, $field, $fileName)
    {
        $status = ['success' => false];
        if (in_array($field, ['prj_file'])) {
            $model = $this->findModel($id);
            //echo  $field;
            $files = Json::decode($model->{$field});
            //print_r($files);
            //echo $fileName;
            if (array_key_exists($fileName, $files)) {
                //print_r($model->attributes);
                if ($this->deleteFile('file', $model->prj_file_tmp, $fileName, $field)) {
                    $status = ['success' => true];
                    unset($files[$fileName]);
                    $model->{$field} = Json::encode($files);
                    if (!$model->save(false)) {
                        print_r($model->getErrorSummary(true));
                    }
                }
            }
        }
        echo json_encode($status);
    }

    /**
     * @param $id
     * @param $file
     * @param $file_name
     * @param string $type
     * @throws NotFoundHttpException
     * download file manual only
     */
    public function actionDownload($id, $file, $file_name, $type = 'prj_file')
    {
        //echo $id.' '.$file.' '.$file_name;
        $model = $this->findModel($id);
        if (!empty($model->{$type})) {
            Yii::$app->response->sendFile($model->getUploadPath($type) . '/' . $model->prj_file_tmp . '/' . $file, $file_name, ['inline' => true]);
        } else {
            $this->redirect(['project/view', 'id' => $id]);
        }
    }

    /**
     * Updates an existing MeetingResponsible model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->responsible_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MeetingResponsible model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
     * Action to load a tabular form grid
     * for MeetingCommandLog
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMeetingCommandLog()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingCommandLog');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingCommandLog', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid
     * for MeetingReport
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMeetingReport()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingReport');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingReport', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidateMeetingResponsible()
    {
        $data = Yii::$app->request->post('MeetingResponsible');
        $data2 = Yii::$app->request->post('MeetingReport');
        $model = null;
        $model2 = null;
        if (null != $data['responsible_id']) {
            $model = MeetingResponsible::findOne($data['responsible_id']);
        }else{
            $model = new MeetingResponsible();
        }
        if (null != $data2['report_id']) {
            $model2 = MeetingReport::findOne($data['responsible_id']);
        }else{
            $model2 = new MeetingReport();
            //$model2->scenario = 'create';
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post())) {
//            $model->prj_user = Yii::$app->user->id;
//            $model->prj_pm = $model->prj_dep_con;
            //print_r($model->_budget_source);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validateMultiple([$model,$model2]);
        }
    }

    public function actionValidateMeetingResponsibleStag()
    {
        //$data = Yii::$app->request->post('MeetingResponsible');
        $data2 = Yii::$app->request->post('MeetingReport');
        //$model = null;
        $model2 = null;

        if (null != $data2['report_id']) {
            $model2 = MeetingReport::findOne($data2['responsible_id']);
        }else{
            $model2 = new MeetingReport();
            $model2->scenario = 'create-stag';
        }

        if (Yii::$app->request->isAjax && $model2->load(Yii::$app->request->post())) {
//            $model->prj_user = Yii::$app->user->id;
//            $model->prj_pm = $model->prj_dep_con;
            //print_r($model->_budget_source);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validateMultiple([$model2]);
        }
    }

    private function removeUploadDir($model, $dir)
    {
        if ($dir == 'report_file') {
            BaseFileHelper::removeDirectory(MeetingReport::getUploadPath($dir) . $model->report_file);
        }
    }
}
