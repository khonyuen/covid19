<?php

namespace frontend\controllers;

use common\models\Document;
use common\models\Project;
use frontend\models\DocumentSearch;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'download-zip', 'validate-create-document',  'deletefile', 'download'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view', 'download-zip', 'download',],
                        'allow' => true,
                        'roles' => ['?'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex($id = null, $type = null)
    {
//        echo $id;
//        echo $type;
        $searchModel = new DocumentSearch();
        $searchModel->mission_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewFile($id){
        /*$model = $this->findModel($id);
        if (!empty($model->{$type})) {
            Yii::$app->response->sendFile($model->getUploadPath($type) . '/' . $model->files_path . '/' . $file, $file_name, ['inline' => true]);
        }*/
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type, $mission)
    {

        $model = new Document();

        $model->files_path = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
        if ($model->load(Yii::$app->request->post())) {
            $this->CreateDir($model->files_path, 'files');
            $model->files = $this->uploadMultipleFile($model, null, 'files');

            if ($model->save()) {
                return Json::encode([
                    'title' => 'ผลการบันทึกข้อมูล',
                    'message' => 'บันทึกข้อมูลเอกสารสำเร็จ',
                    'status' => 'success',
                    'url' => Url::to(['document/index']),
                ]);
            } else {
                $error = $model->getErrorSummary(true);
                return Json::encode([
                    'title' => 'ผลการบันทึกข้อมูล',
                    'message' => 'พบข้อผิดพลาดในการบันทึกข้อมูลเอกสาร ' . implode(',', $error),
                    'status' => 'error',
                    'url' => Url::to(['document/index']),
                ]);
            }
        }

        $model->document_type = $type;
        $model->mission_id = $mission;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    private function CreateDir($folderName, $attribute = 'files')
    {
        //echo $folderName;
        $basePath = Document::getUploadPath($attribute);
        if ($folderName != null && !is_dir($basePath . $folderName)) {

            //echo $basePath;
            BaseFileHelper::createDirectory($basePath . $folderName, 0777);
//            if (BaseFileHelper::createDirectory($basePath . $folderName, 0777)) {
//                BaseFileHelper::createDirectory($basePath . $folderName . '/thumbnail', 0777);
//            }else{

            //}
            //echo $basePath . $folderName;
        }
        //echo $folderName;
        return;
    }

    private function uploadMultipleFile($model, $tempFile = null, $attribute)
    {
        //echo $attribute;
        $files = [];
        $json = '';
        $tempFile = Json::decode($tempFile);
        $UploadedFiles = UploadedFile::getInstances($model, $attribute);

//        print_r($UploadedFiles);
//        print_r($tempFile);

        if ($UploadedFiles !== null) {
            foreach ($UploadedFiles as $file) {
                try {
                    $oldFileName = $file->basename . '.' . $file->extension;
                    $newFileName = md5($file->basename . time()) . '.' . $file->extension;

                    $file->saveAs(Document::getUploadPath($attribute) . $model->{'files_path'} . '/' .
                        $newFileName);

                    //$fileThumbnailPath = Project::getUploadPath($attribute) . $model->{$attribute . '_ref'} . '/thumbnail';

                    /*@Image::getImagine()->open(Project::getUploadPath($attribute) .
                        $model->{$attribute . '_ref'} . '/' . $newFileName)->thumbnail(new Box(120, 120))->save($fileThumbnailPath . '/' . $newFileName, ['quality' => 90]);*/

                    $files[$newFileName] = $oldFileName;
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            $json = Json::encode(ArrayHelper::merge($tempFile, $files));
        } else {
            $json = $tempFile;
        }
        //echo $json;
        return $json;
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tempFile = $model->files;
        //$model->files_path = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
        if ($model->load(Yii::$app->request->post())) {
            $this->CreateDir($model->files_path, 'files');
            $model->files = $this->uploadMultipleFile($model, $tempFile, 'files');

            if ($model->save()) {
                return Json::encode([
                    'title' => 'ผลการบันทึกข้อมูล',
                    'message' => 'บันทึกข้อมูลเอกสารสำเร็จ',
                    'status' => 'success',
                    'url' => Url::to(['document/index']),
                ]);
            } else {
                $error = $model->getErrorSummary(true);
                return Json::encode([
                    'title' => 'ผลการบันทึกข้อมูล',
                    'message' => 'พบข้อผิดพลาดในการบันทึกข้อมูลเอกสาร ' . implode(',', $error),
                    'status' => 'error',
                    'url' => Url::to(['document/index']),
                ]);
            }
        }

        //echo $model->files;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($id) {
                $model = Document::deleteAll(['=', 'document_id', $id]);
                $ts->commit();
            } else {
                $ids = Yii::$app->request->post('key');
                if (!is_null($ids)) {
                    $ids = explode(',', $ids);
                    $model = Document::deleteAll(['in', 'document_id', $ids]);
                    $ts->commit();
                }
            }
            return Json::encode([
                'result' => true,
                'msg' => 'ลบข้อมูลเรียบร้อย',
                'options' => [],
            ]);
        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    public function actionValidateCreateDocument()
    {
        $data = Yii::$app->request->post('Document');
        if (null != $data['document_id']) {
            $model = Document::findOne($data['document_id']);

        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new Document();
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionDeletefile($id, $field, $fileName)
    {
        $status = ['success' => false];
        if (in_array($field, ['document_id', 'files'])) {
            $model = $this->findModel($id);
            //echo  $field;
            $files = Json::decode($model->{$field});
            //print_r($files);
            //echo $fileName;
            if (array_key_exists($fileName, $files)) {
                //print_r($model->attributes);
                if ($this->deleteFile('files', $model->files_path, $fileName, $field)) {
                    $status = ['success' => true];
                    unset($files[$fileName]);
                    $model->{$field} = Json::encode($files);
                    if (!$model->save(false)) {
                        print_r($model->getErrorSummary(true));
                    }
                }
            }
        }
        echo json_encode($status);
    }

    private function deleteFile($type = 'files', $ref, $fileName, $field)
    {

        if (in_array($type, ['files', 'thumbnail', 'other'])) {
            if (in_array($type, ['files', 'thumbnail'])) {
                $filePath = Document::getUploadPath($field) . $ref . '/' . $fileName;
                $fileTPath = Document::getUploadPath($field) . $ref . '/thumbnail/' . $fileName;
                @unlink($filePath);
                @unlink($fileTPath);
            } else {
                $filePath = Document::getUploadPath($field) . $ref . '/' . $fileName;
                @unlink($filePath);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @param $file
     * @param $file_name
     * @param string $type
     * @throws NotFoundHttpException
     * download file manual only
     */
    public function actionDownload($id, $file, $file_name, $type = 'files')
    {
        //echo $id.' '.$file.' '.$file_name;
        $model = $this->findModel($id);
        if (!empty($model->{$type})) {
            Yii::$app->response->sendFile($model->getUploadPath($type) . '/' . $model->files_path . '/' . $file, $file_name, ['inline' => true]);
        } else {
            $this->redirect(['document/view', 'id' => $id]);
        }
    }

    public function actionDownloadZip($id)
    {

        $model = $this->findModel($id);
        $path = $model->files_path;
        $rootPath = ($model::getUploadUrl() . $path);
        $downloadPath = $model::getUploadPath() . $path;

        //echo $downloadPath.'.zip';
//        exit();
        if (is_file($downloadPath . '.zip')) {
            return Yii::$app->response->sendFile($downloadPath . '.zip', 'download.zip', ['inline' => true]);
            //echo 1;
            exit();
        }

        $zip = new ZipArchive();
        $filename = "./uploads/documents/" . $path . ".zip";

        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            exit("cannot open <$filename>\n");
        }

        $all = Json::decode($model->files);
        foreach ($all as $key => $item) {
//            echo $item.' '.$key;
            $file = $item;
            $relPath = $model::getUploadPath('files') . $model->files_path . '/' . $key;
            $zip->addFile($relPath, $file);
        }

        $zip->close();

        return Yii::$app->response->sendFile($model->getUploadPath() . $model->files_path . '.zip', 'download.zip', ['inline' => true]);
    }

    private function removeUploadDir($model, $dir)
    {
        if ($dir == 'files') {
            BaseFileHelper::removeDirectory(Document::getUploadPath($dir) . $model->files_path);
        }
    }
}
