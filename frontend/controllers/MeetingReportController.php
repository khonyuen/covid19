<?php

namespace frontend\controllers;

use common\models\MeetingCommandLog;
use common\models\MeetingReport;
use common\models\MeetingResponsible;
use frontend\models\MeetingReportSearch;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * MeetingReportController implements the CRUD actions for MeetingReport model.
 */
class MeetingReportController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeetingReport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MeetingReport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMeetingReportComment = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingReportComments,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMeetingReportComment' => $providerMeetingReportComment,
        ]);
    }

    /**
     * Finds the MeetingReport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingReport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingReport::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new MeetingReport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MeetingReport();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->report_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    private function CreateDir($folderName, $attribute = 'report_path_temp')
    {
        //echo $folderName;
        $basePath = MeetingReport::getUploadPath('report_file');
        $basePath2 = MeetingReport::getUploadPath('report_images');
        if ($folderName != null && !is_dir($basePath . $folderName)) {

            BaseFileHelper::createDirectory($basePath . $folderName, 0777);
        }
        if ($folderName != null && !is_dir($basePath2 . $folderName)) {

            BaseFileHelper::createDirectory($basePath2 . $folderName, 0777);
        }
        //echo $folderName;
        return;
    }

    private function uploadMultipleFile($model, $tempFile = null, $attribute)
    {
        //echo $attribute;
        $files = [];
        $json = '';
        $tempFile = Json::decode($tempFile);
        $UploadedFiles = UploadedFile::getInstances($model, $attribute);

        //print_r($UploadedFiles);
        //print_r($tempFile);

        if ($UploadedFiles !== null) {
            foreach ($UploadedFiles as $file) {
                try {
                    $oldFileName = $file->basename . '.' . $file->extension;
                    $newFileName = md5($file->basename . time()) . '.' . $file->extension;

                    $file->saveAs(MeetingReport::getUploadPath($attribute) . $model->report_path_temp . '/' .
                        $newFileName);

                    //$fileThumbnailPath = Project::getUploadPath($attribute) . $model->{$attribute . '_ref'} . '/thumbnail';

                    /*@Image::getImagine()->open(Project::getUploadPath($attribute) .
                        $model->{$attribute . '_ref'} . '/' . $newFileName)->thumbnail(new Box(120, 120))->save($fileThumbnailPath . '/' . $newFileName, ['quality' => 90]);*/

                    $files[$newFileName] = $oldFileName;
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            $json = Json::encode(ArrayHelper::merge($tempFile, $files));
        } else {
            $json = $tempFile;
        }
        //print_r($json);
        return $json;
    }

    /**
     * Updates an existing MeetingReport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $reportModel =$this->findModel($id);
        $tempFile = $reportModel->report_file;
        $tempImg = $reportModel->report_images;
        //$model->scenario = 'create-report';
        //$model->item_id = $id;
        //$model->mission_id = $mission_id;
        if ($reportModel->load(Yii::$app->request->post())) {
            $ts = Yii::$app->db->beginTransaction();
            try {
                $responsible = MeetingResponsible::findOne($reportModel->responsible_id);

                $this->CreateDir($reportModel->report_path_temp, 'report_path_temp');
                $reportModel->report_file = $this->uploadMultipleFile($reportModel, $tempFile, 'report_file');
                $reportModel->report_images = $this->uploadMultipleFile($reportModel, $tempImg, 'report_images');
                $reportModel->report_date = $reportModel::dateEngDb($reportModel->report_date);

                $log = new MeetingCommandLog();
                $log->old_status = $responsible->getOldAttribute('responsible_status');
                $responsible->attributes = Yii::$app->request->post('MeetingResponsible');
                $log->new_status = $responsible->responsible_status;

                if ($reportModel->save()) {
                    //$pk = $model->getPrimaryKey();

                    $log->report_id = $reportModel->report_id;
                    $log->responsible_id = $reportModel->responsible_id;
                    $log->updated_at = new Expression('NOW()');
                    $log->save();

                    $responsible->save();

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['meeting-responsible/view', 'id' => $reportModel->responsible_id]),
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $reportModel->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }
        } else {
            //$reportModel->report_path_temp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            $model = MeetingResponsible::findOne($reportModel->responsible_id);
            $reportModel->report_date = $reportModel::dateEngDb($reportModel->report_date);
            return $this->renderAjax('update', [
                'model' => $model,
//                'item' => $commandItem,
                'reportModel' => $reportModel,
            ]);
        }
    }

    public function actionDeletefile($id, $field, $fileName)
    {
        $status = ['success' => false];
        if (in_array($field, ['report_file', 'report_images'])) {
            $model = $this->findModel($id);
            //echo  $field;
            $files = Json::decode($model->{$field});
            //print_r($files);
            //echo $fileName;
            if (array_key_exists($fileName, $files)) {
                //print_r($model->attributes);
                if ($this->deleteFile('files', $model->report_path_temp, $fileName, $field)) {
                    $status = ['success' => true];
                    unset($files[$fileName]);
                    $model->{$field} = Json::encode($files);
                    if (!$model->save(false)) {
                        print_r($model->getErrorSummary(true));
                    }
                }
            }
        }
        echo json_encode($status);
    }

    private function deleteFile($type = 'files', $ref, $fileName, $field)
    {

        if (in_array($type, ['files', 'thumbnail', 'other'])) {
            if (in_array($type, ['files', 'thumbnail'])) {
                $filePath = MeetingReport::getUploadPath($field) . $ref . '/' . $fileName;
                $fileTPath = MeetingReport::getUploadPath($field) . $ref . '/thumbnail/' . $fileName;
                @unlink($filePath);
                @unlink($fileTPath);
            } else {
                $filePath = MeetingReport::getUploadPath($field) . $ref . '/' . $fileName;
                @unlink($filePath);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes an existing MeetingReport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $report = $this->findModel($id);
        try {
            $ts = Yii::$app->db->beginTransaction();
            $path = $report->report_path_temp;
            $res_id =$report->responsible_id;
            if ($report->delete()) {
                // remove file
                $this->removeUploadDir($report, 'report_file','report_images', $path);
                $ts->commit();
                return Json::encode([
                    'result' => true,
                    'msg' => 'ลบข้อมูลเรียบร้อย',
                    'url'=>Url::to(['meeting-responsible/view','id'=>$res_id])
                ]);
            }
        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    private function removeUploadDir($model, $attribute,$attribute2, $path)
    {
        if ($attribute != '') {
            BaseFileHelper::removeDirectory(MeetingReport::getUploadPath($attribute) . $path);
            BaseFileHelper::removeDirectory(MeetingReport::getUploadPath($attribute2) . $path);
        }
    }

    /**
     * Action to load a tabular form grid
     * for MeetingReportComment
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMeetingReportComment()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingReportComment');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingReportComment', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @param $file
     * @param $file_name
     * @param string $type
     * @throws NotFoundHttpException
     * download file manual only
     */
    public function actionDownload($id, $file, $file_name, $field)
    {
        //echo $id.' '.$file.' '.$file_name;
        $model = $this->findModel($id);
        if (!empty($model->{$field})) {
            Yii::$app->response->sendFile($model->getUploadPath($field) . '' . $model->report_path_temp . '/' . $file, $file_name, ['inline' => true]);
        }
    }

    public function actionDownloadZip($id, $type = 'report_file')
    {

        $model = $this->findModel($id);
        $path = $model->report_path_temp;
        $rootPath = ($model::getUploadUrl() . $path);
        $downloadPath = $model::getUploadPath() . $path;

        //echo $downloadPath.'.zip';
//        exit();
        if (is_file($downloadPath . '.zip')) {
            return Yii::$app->response->sendFile($downloadPath . '.zip', 'download.zip', ['inline' => true]);
            //echo 1;
            exit();
        }

        $zip = new ZipArchive();
        $filename = "$downloadPath" . ".zip";

        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            exit("cannot open <$filename>\n");
        }

        $all = Json::decode($model->$type);
        foreach ($all as $key => $item) {
//            echo $item.' '.$key;
            $file = $item;
            $relPath = $model::getUploadPath($type) . $model->report_path_temp . '/' . $key;
            $zip->addFile($relPath, $file);
        }

        $zip->close();

        return Yii::$app->response->sendFile($model->getUploadPath() . $model->report_path_temp . '.zip', 'download.zip', ['inline' => true]);
    }

    public function actionValidateMeetingReport()
    {
        //$data = Yii::$app->request->post('MeetingResponsible');
        $data2 = Yii::$app->request->post('MeetingReport');
        //$model = null;
        $model2 = null;

        if (null != $data2['report_id']) {
            $model2 = MeetingReport::findOne($data2['report_id']);
        }else{
            $model2 = new MeetingReport();
            //$model2->scenario = 'create-stag';
        }

        if (Yii::$app->request->isAjax && $model2->load(Yii::$app->request->post())) {
//            $model->prj_user = Yii::$app->user->id;
//            $model->prj_pm = $model->prj_dep_con;
            //print_r($model->_budget_source);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model2);
        }
    }
}
