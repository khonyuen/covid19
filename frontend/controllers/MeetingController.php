<?php

namespace frontend\controllers;

use common\models\BaseActiveRecord;
use common\models\Meeting;
use common\models\MeetingCommand;
use frontend\models\MeetingSearch;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use t01\assets\MainAsset;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * MeetingController implements the CRUD actions for Meeting model.
 */
class MeetingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-meeting-command', 'validate-create-meeting', 'validate-update-meeting', 'print','print-report','minify'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view','print','print-report'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Meeting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meeting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMeetingCommand = new \yii\data\ArrayDataProvider([
            'allModels' => $model->meetingCommands,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMeetingCommand' => $providerMeetingCommand,
        ]);
    }

    /**
     * Finds the Meeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meeting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meeting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Meeting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Meeting();

        if ($model->load(Yii::$app->request->post())) {
            $ts = Yii::$app->db->beginTransaction();
            try {
                $model->meeting_date = $model::dateEngDb($model->meeting_date);
                if ($model->save()) {
                    $pk = $model->getPrimaryKey();
                    MeetingCommand::createCommand($model->_meeting_command, $pk);
                    $ts->commit();
                    return $this->redirect(['view', 'id' => $model->meeting_id]);
                } else {
                    $ts->rollBack();
                    throw new \Exception(implode(',', $model->getErrorSummary(true)));
                }
            } catch (\Exception $exception) {
                throw new HttpException(500, $exception->getMessage());
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Meeting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $ts = Yii::$app->db->beginTransaction();
            try {
                $model->meeting_date = $model::dateEngDb($model->meeting_date);
                if ($model->save()) {
                    MeetingCommand::createCommand($model->_meeting_command, $model->meeting_id, 'update');
                    $ts->commit();
                    return $this->redirect(['view', 'id' => $model->meeting_id]);
                } else {
                    $ts->rollBack();
                    throw new \Exception(implode(',', $model->getErrorSummary(true)));
                }
            } catch (\Exception $exception) {
                throw new HttpException(500, $exception->getMessage());
            }
        } else {
            $model->_meeting_command = $model->meetingCommands;
            $model->meeting_date = $model::dateEngDb($model->meeting_date);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Meeting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($id) {
                $meeting = Meeting::findOne($id);

                if($meeting::deleteAll(['=', 'meeting_id', $id])){
                    $ts->commit();
                }
            } else {
                $ids = Yii::$app->request->post('key');
                if (!is_null($ids)) {
                    $ids = explode(',', $ids);
                    $model = Meeting::deleteAll(['in', 'meeting_id', $ids]);
                    $ts->commit();
                }
            }
            return Json::encode([
                'result' => true,
                'msg' => 'ลบข้อมูลเรียบร้อย',
                'options' => [],
            ]);
        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    /**
     * Action to load a tabular form grid
     * for MeetingCommand
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMeetingCommand()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MeetingCommand');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMeetingCommand', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidateCreateMeeting()
    {
        $flag = 'new';
        $data = Yii::$app->request->post('Meeting');
        if (null != $data['meeting_id']) {
            $model = Meeting::findOne($data['meeting_id']);
            $flag = 'update';
        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new Meeting();
            //$budget = [new ProjectDetailBudget()];
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->_meeting_command = Yii::$app->request->post('Meeting')['_meeting_command'];
            if ($flag == 'update') {
                $model->scenario = 'valid-meeting-update';
            }
            //print_r($model->_meeting_command);
            /*$data = Yii::$app->request->post('ProjectDetailBudget',[]);
            foreach (array_keys($data) as $index) {
                $budget[$index] = new ProjectDetailBudget();
            }
            Model::loadMultiple($budget,Yii::$app->request->post());*/

            Yii::$app->response->format = Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple($budget);
            return ActiveForm::validate($model);
        }
    }

    public function actionValidateUpdateMeeting()
    {
        $data = Yii::$app->request->post('Meeting');
        if (null != $data['dat_id']) {
            $model = Meeting::findOne($data['meeting_id']);
        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new Meeting();
            //$budget = [new ProjectDetailBudget()];
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->_meeting_command = Yii::$app->request->post('Meeting')['_meeting_command'];
            //print_r($model->_meeting_command);
            /*$data = Yii::$app->request->post('ProjectDetailBudget',[]);
            foreach (array_keys($data) as $index) {
                $budget[$index] = new ProjectDetailBudget();
            }
            Model::loadMultiple($budget,Yii::$app->request->post());*/

            Yii::$app->response->format = Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple($budget);
            return ActiveForm::validate($model);
        }
    }

    public function getResName(MeetingCommand $model)
    {
        $data = $model->getResName();
        $txt = "<ul>";

        //var_dump($data);
        foreach ($data as $datum) {
            $status = BaseActiveRecord::getResponsibleStatusItem($datum['responsible_status']);
            $style = "";
            if ($datum['responsible_status'] == 1) {
                $style = 'warning';
            } elseif ($datum['esponsible_status'] == 1) {
                $style = 'success';
            } elseif ($datum['responsible_status'] == 1) {
                $style = 'default';
            } elseif ($datum['responsible_status'] == 1) {
                $style = 'danger';
            }
            $url = Url::to(['meeting-responsible/create', 'id' => $datum['item_id']]);
            $txt .= "<li><a href='#' data-url='{$url}' class='lovOpen'> " . $datum['mission_name'] . "<div class='badge badge-{$style}'>{$status}</div></a></li>";
        }
        $txt .= "</ul>";
        return $txt;
    }

    public function actionPrint($id)
    {
        $meeting = Meeting::findOne($id); //->joinWith(['meetingCommands']);
        $providerMeetingCommand = new \yii\data\ArrayDataProvider([
            'allModels' => $meeting->meetingCommands,
        ]);
        //report
        return $this->render('print_view', [
            'model' => $meeting,
            'providerMeetingCommand'=>$providerMeetingCommand

        ]);
    }

    public function actionPrintReport($id){
        $meeting = Meeting::findOne($id); //->joinWith(['meetingCommands']);
        $providerMeetingCommand = new \yii\data\ArrayDataProvider([
            'allModels' => $meeting->meetingCommands,
        ]);
        //report
        $report = $this->renderPartial('print_view', [
            'model' => $meeting,
            'providerMeetingCommand'=>$providerMeetingCommand

        ]);
        return $this->renderPdf($report, \kartik\mpdf\Pdf::FORMAT_A4, 'L','',false,'I',12);
    }

    public function renderPdf($content, $format = 'A4', $layout = 'L', $header = '', $line = false, $file = 'I', $fs = 16)
    {
        $pdfHeader = [
            'L' => [
                'content' => '',
                'font-size' => ($fs+2),
                'font-style' => 'B',
                'font-family' => 'trirong',
                'color' => '#333333',
            ],
            'C' => [
                'font-size' => $fs,
                'font-style' => 'B',
                'font-family' => 'trirong',
                'color' => '#333333',
                'content' => $header,
            ],
            'R' => [
                'content' => '',
            ],
            'line' => $line,
        ];

        $pdfFooter = [
            'L' => [
                'content' => "วันที่พิมพ์ " . date('d-m-Y H:i:s'), // . " โดย " . \Yii::$app->user->identity->offName,
                'font-size' => 10,
                'color' => '#333333',
                'font-family' => 'trirong',
            ],
            'C' => [
                'content' => '',
            ],
            'R' => [
                'content' => '[{PAGENO}/{nb}]',
                'font-size' => 10,
                'color' => '#333333',
                'font-family' => 'trirong',
            ],
            'line' => false,
        ];

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => $layout,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'defaultFontSize' => 14,
            'defaultFont' => 'trirong',
            //'cssFile' => \Yii::getAlias('@smartadmin').'/dist/css/your_style.css',
            'cssFile' => '@frontend/web/css/kv-mpdf-bootstrap.css',
            /*'cssInline' => 'table{border-collapse:collapse;width:100%}td{border:1px solid #000} @media all{
            .font_next{font-family:DoodlePen}table{border-collapse:collapse;width:100%}td{border:1px solid #000}.page-break {display: none;}
        }
        @media print{
            .page-break{display: block;page-break-before: always;}
        }',*/
            'cssInline' => 'body{ font-family: "trirong"; } .kv-wrap{padding:10px;}' .
                '.kv-align-center{text-align:center;}' .
                '.kv-align-left{text-align:left;}' .
                '.kv-align-right{text-align:right;}' .
                '.kv-align-top{vertical-align:top!important;}' .
                '.kv-align-bottom{vertical-align:bottom!important;}' .
                '.kv-align-middle{vertical-align:middle!important;}' .
                '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                '.show-grid [class^="col-"] {padding-top: 10px;  padding-bottom: 10px; background-color: white; border: 1px solid black; }' .
                '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
            //'options' => ['title' => \Yii::$app->name,'keepColumns'=>true],
            'methods' => [
                //'keepColumns'=>true,
                'SetHeader' => [
                    ['odd' => $pdfHeader, 'even' => $pdfHeader],
                ],
                'SetFooter' => [
                    ['odd' => $pdfFooter, 'even' => $pdfFooter],
                ],
                //'useSubstitutions'=>false,
                //'simpleTables'=>true
            ],
            //'tempPath' => \Yii::getAlias('@webroot').'/reports',
            'filename' => date('dmYhis') . '.pdf',
        ]);
        //$path = \Yii::getAlias('@webroot');
        //\Yii::setAlias('@pdf', '@webroot/reports/pdf/');
        //$pdf->tempPath = \Yii::getAlias('@pdf');
        //echo $pdf->tempPath;

        //$pdf->filename = date('dmYhis').'.pdf';
        //$pdf->output($content,$pdf->filename,'F');
        ini_set("pcre.backtrack_limit", "500000000");

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf->options['fontDir'] = array_merge($fontDirs, [
            Yii::getAlias('@webroot').'/fonts'
        ]);



        $pdf->options['fontdata'] = $fontData + [
                /*'Trirong' => [
                    'R' => 'Trirong-Regular.ttf',
                    'TTCfontID' => [
                        'R' => 1,
                    ],
                ],*/
                'trirong' => [
                    'R' => 'Trirong-Regular.ttf',
                    'B' => 'Trirong-Bold.ttf',
                    'I' => 'Trirong-Italic.ttf',
                    'BI' => 'Trirong-BoldItalic.ttf',
                ]
            ];

//        var_dump($pdf->options);
        return $pdf->render();
        //return \Yii::$app->response->sendFile($path.'/'.$pdf->filename,['inline'=>true]);
    }

    public function actionMinify(){
        $mainAsset = new MainAsset();
        $path = $mainAsset->sourcePath;
        $js = $mainAsset->js;
        $minifyPath = $path.'/'.'minify';
        //print_r($js);
        $fileJs = [];

        //$stringPath = implode(',',$fileJs);

//        foreach ($js as $j) {
//            $file = $path.'/'.$j;
//            $minifier = new JS();
//            $minifier->add($file);
//            //$minifier->minify($minifyPath.'/'.$j);
//            $minifier->minify($minifyPath.'/'.$j);
//        }

        $css = $mainAsset->css;
        foreach ($css as $c) {
            $file = $path.'/'.$c;
            $minifier = new CSS();
            $minifier->add($file);
            //$minifier->minify($minifyPath.'/'.$j);
            $minifier->minify($minifyPath.'/'.$c);
        }
    }
}
