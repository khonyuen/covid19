<?php

namespace frontend\controllers;

use common\models\Document;
use common\models\ProjectPayment;
use frontend\models\Amphur;
use frontend\models\CmTarget;
use Yii;
use frontend\models\CmTargetResult;
use frontend\models\CmTargetResultSearch;
use yii\base\Model;
use yii\bootstrap4\ActiveForm;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CmTargetResultController implements the CRUD actions for CmTargetResult model.
 */
class CmTargetResultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => [ 'index','create', 'update', 'delete', 'check-update', 'validate-create-target',  'deletefile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index','view',],
                        'allow' => true,
                        'roles' => ['?'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CmTargetResult models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CmTargetResultSearch();
        $searchModel->scenario = 'searchTg';
        $dataProvider = $searchModel->searchReportPv(Yii::$app->request->queryParams);
        $amp = Amphur::getMukAmphur(1);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'amp'=>$amp
        ]);
    }

    /**
     * Displays a single CmTargetResult model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCheckUpdate($d,$a){
        // check old data
        $check = CmTargetResult::find()->where(['tg_amphur' => $a, 'tg_date' => $d])->exists();
        if($check){
            //$cmTarget = CmTarget::getList();
            $targetModels = [];
            $models = CmTargetResult::find()->select(['cm_target_result.*','cm_target.targetname'])->joinWith('target')->where(['tg_amphur' => $a, 'tg_date' => $d])->indexBy('tg_target_id')->all();
            foreach ($models as $index => $item){
//            echo $item.$index;
                $targetModels[$index] = $item;
            }
            $html = $this->renderAjax('_update',[
                //'model' => $model,
                'targetRsModels'=>$targetModels,
                'd'=>$d,
                'a'=>$a
            ]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status'=>true,
                'message'=>$html
            ];
        }else{
            $cmTarget = CmTarget::getList();
            $targetModels = [];
            foreach ($cmTarget as $index => $item){
//            echo $item.$index;
                $m = new CmTargetResult();
                $m->tg_target_id = $index;
                $m->_tggroupname = $item;
                $m->tg_num = 0;
                $m->tg_rs = 0;
                $m->tg_l1 = 0;
                $m->tg_l2 = 0;
                $m->tg_l3 = 0;
                $m->tg_l4 = 0;
                $m->tg_l5 = 0;
                $targetModels[] = $m;
            }

            $amphurModel = Amphur::getMukAmphur();

            $html =  $this->renderAjax('_create', [
                //'model' => $model,
                'targetRsModels'=>$targetModels,
                'd'=>$d,
                'a'=>$a
                //'amphurModel'=>$amphurModel
            ]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status'=>false,
                'message'=>$html
            ];
        }
    }

    /**
     * Creates a new CmTargetResult model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CmTargetResult();
        $flag = Yii::$app->request->post('flag');
        if($flag=='new'){
            if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
                $data = Yii::$app->request->post('CmTargetResult',[]);
                //print_r($data);
                $date = $data['tg_date'];
                $amphur = $data['tg_amphur'];

                ArrayHelper::remove($data,'tg_date');
                ArrayHelper::remove($data,'tg_amphur');

                foreach (array_keys($data) as $index) {
                    $a = new CmTargetResult();
                    $a->tg_date = $date;
                    $a->tg_amphur = $amphur;
                    $a->created_user = Yii::$app->user->id;
                    $models[$index] = $a;
                }

                Model::loadMultiple($models, Yii::$app->request->post());
                if(Model::validateMultiple($models)) {
                    $ts = Yii::$app->db->beginTransaction();
                    try {
                        $success = 0;
                        foreach ($models as $model) {
                            $model->tg_date = $date;
                            $model->tg_amphur = $amphur;
                            $model->created_user = Yii::$app->user->id;
                            if($model->validate() &&$model->save()){
                                $success++;
                            }else{
                                throw new Exception(implode(',',$model->getErrorSummary(true)));
                            }

                        }
                        $ts->commit();
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status'=>true,
                            'message'=>'บันทึกข้อมูลสำเร็จ'
                        ];
                    }catch (\Exception $exception){
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status'=>false,
                            'message'=>'ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก '.$exception->getMessage()
                        ];
                    }
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'status'=>false,
                        'message'=>'ไม่สามารถบันทึกข้อมูลได้'
                    ];
                }
            }
        }elseif($flag=='update'){
            if (Yii::$app->request->post() && Yii::$app->request->isAjax) {
                $data = Yii::$app->request->post('CmTargetResult',[]);
                //print_r($data);
                $date = $data['tg_date'];
                $amphur = $data['tg_amphur'];

                ArrayHelper::remove($data,'tg_date');
                ArrayHelper::remove($data,'tg_amphur');

                $models = CmTargetResult::find()->where(['tg_amphur' => $amphur, 'tg_date' => $date])->indexBy('tg_target_id')->all();

                /*foreach (array_keys($data) as $index) {
                    $a = new CmTargetResult();
                    $a->tg_date = $date;
                    $a->tg_amphur = $amphur;
                    $a->created_user = Yii::$app->user->id;
                    $models[$index] = $a;
                }*/

                Model::loadMultiple($models, Yii::$app->request->post());

                //var_dump($models);

                if(Model::validateMultiple($models)) {
                    $ts = Yii::$app->db->beginTransaction();
                    try {
                        $success = 0;
                        foreach ($models as $model) {
                            if($model->validate() &&$model->save()){
                                $success++;
                            }else{
                                throw new Exception(implode(',',$model->getErrorSummary(true)));
                            }

                        }
                        $ts->commit();
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status'=>true,
                            'message'=>'อัพเดทข้อมูลสำเร็จ'
                        ];
                    }catch (\Exception $exception){
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status'=>false,
                            'message'=>'ไม่สามารถอัพเดทข้อมูลได้ เนื่องจาก '.$exception->getMessage()
                        ];
                    }
                }else{
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'status'=>false,
                        'message'=>'ไม่สามารถอัพเดทข้อมูลได้'
                    ];
                }
            }
        }


        ////////////////////////////////////////////////////////////////////////////
        $cmTarget = CmTarget::getList();
        $targetModels = [];
        /*foreach ($cmTarget as $index => $item){
//            echo $item.$index;
            $m = new CmTargetResult();
            $m->tg_target_id = $index;
            $m->_tggroupname = $item;
            $m->tg_num = 0;
            $m->tg_rs = 0;
            $m->tg_l1 = 0;
            $m->tg_l2 = 0;
            $m->tg_l3 = 0;
            $m->tg_l4 = 0;
            $m->tg_l5 = 0;
            $targetModels[] = $m;
        }*/

        $amphurModel = Amphur::getMukAmphur();

        return $this->render('create', [
            'model' => $model,
            'targetRsModels'=>$targetModels,
            'amphurModel'=>$amphurModel
        ]);
    }

    /**
     * Updates an existing CmTargetResult model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CmTargetResult model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CmTargetResult model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmTargetResult the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CmTargetResult::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidateCreateTarget()
    {
        $flag = Yii::$app->request->post('flag');
        $data = Yii::$app->request->post('CmTargetResult',[]);
        //print_r($data);
        if($flag=='new') {
            $date = $data['tg_date'];
            $amphur = $data['tg_amphur'];

            ArrayHelper::remove($data, 'tg_date');
            ArrayHelper::remove($data, 'tg_amphur');

            foreach (array_keys($data) as $index) {
                $a = new CmTargetResult();
                $a->tg_date = $date;
                $a->tg_amphur = $amphur;
                $models[$index] = $a;
            }

            Model::loadMultiple($models, Yii::$app->request->post());

            //print_r($models[0]);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($models);
            }
        }else{
            // check old model
            $date = $data['tg_date'];
            $amphur = $data['tg_amphur'];

            ArrayHelper::remove($data, 'tg_date');
            ArrayHelper::remove($data, 'tg_amphur');

            $models = CmTargetResult::find()->where(['tg_amphur' => $amphur, 'tg_date' => $date])->indexBy('tg_target_id')->all();
            /*foreach (array_keys($models) as $index) {
                $a = new CmTargetResult();
                $a->scenario = 'update';
                $a->tg_date = $date;
                $a->tg_amphur = $amphur;
                $models[$index] = $a;
            }*/

            Model::loadMultiple($models, Yii::$app->request->post());

            //print_r($models[0]);

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validateMultiple($models);
            }
        }

        /*if (null != $data['document_id']) {
            $model = CmTargetResult::findOne($data['tg_id']);

        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new CmTargetResult();
            //$model->scenario = 'insert';
            $v1 = Model::loadMultiple($model, Yii::$app->request->post());
            $v2 = Model::validateMultiple($settings);


        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->created_user = Yii::$app->user->id;

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }*/
    }
}
