<?php

use yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl()); // replace text for url manager
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'ระบบติดตามสถานการณ์ COVID-19 สสจ.มุกดาหาร',
    'language' => 'TH-th',
    'aliases' => [
        '@t01' => '@common/themes/t01',
        '@source' => '@t01/source',
    ],

    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'frontend\controllers',
    'container' => [
//        'definitions' => [
//            \yii\widgets\LinkPager::class => \yii\bootstrap4\LinkPager::class,
//        ],
//        'singletons' => [
//            // Dependency Injection Container singletons configuration
//        ]
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    //'@app/views'=>'@common/themes/modernadmin/views'
                    //'@app/views' => '@modernadmin/views',
                    '@app/views' => '@t01/views',
                ],
            ],
//            'class' => '\rmrevin\yii\minify\View',
//            'enableMinify' => !YII_DEBUG,
//            'concatCss' => true, // concatenate css
//            'minifyCss' => true, // minificate css
//            'concatJs' => true, // concatenate js
//            'minifyJs' => true, // minificate js
//            'minifyOutput' => true, // minificate result html page
//            'webPath' => '@web', // path alias to web base
//            'basePath' => '@webroot', // path alias to web base
//            'minifyPath' => '@webroot/minify', // path alias to save minify result
//            'jsPosition' => [\yii\web\View::POS_END], // positions of js files to be minified
//            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
//            'expandImports' => true, // whether to change @import on content
//            'compressOptions' => ['extra' => true], // options for compress
//            'excludeFiles' => [
//                'jquery.js', // exclude this file from minification
//                'app-[^.].js', // you may use regexp
//            ],
        ],
        //////////////////////
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl, // add url manager
            'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'baseUrl' => $baseUrl, // add url manager
            'showScriptName' => false,   // Disable index.php
            'enablePrettyUrl' => true,   // Disable r= routes
            'enableStrictParsing' => false,
            'rules' => [
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
        //'excludeBundles' => [
            //\app\helloworld\AssetBundle::class, // exclude this bundle from minification

        //],

    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
    ],
    'params' => $params,
];
