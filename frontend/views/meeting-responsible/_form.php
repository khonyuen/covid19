<?php

use common\models\MeetingCommandItems;
use kartik\date\DatePicker;
use marqu3s\summernote\Summernote;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingResponsible */
/* @var $form yii\widgets\ActiveForm */
/* @var $reportModel common\models\MeetingReport */
/* @var $item common\models\MeetingCommandItems */

/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MeetingCommandLog', 
        'relID' => 'meeting-command-log', 
        'value' => \yii\helpers\Json::encode($model->meetingCommandLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MeetingReport', 
        'relID' => 'meeting-report', 
        'value' => \yii\helpers\Json::encode($model->meetingReports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);*/
?>

<div class="meeting-responsible-form">

    <?php $form = \yii\bootstrap4\ActiveForm::begin([
        'id' => 'create-meeting-responsible',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validationUrl' => Url::to(['validate-meeting-responsible']),
        'options' => [
            'class' => 'kt-form kt-form--fit kt-form--label-right',
            'enctype' => 'multipart/form-data',
        ],
    ]);


    echo Html::activeHiddenInput($model, 'responsible_id');
    echo Html::activeHiddenInput($model, 'mission_id');
    echo Html::activeHiddenInput($model, 'item_id');
    echo Html::activeHiddenInput($reportModel, 'report_id');
    echo Html::activeHiddenInput($reportModel, 'report_path_temp');
    //echo Html::activeHiddenInput($reportModel, 'images_tmp');
    ?>

    <div class="well-sm"><h4><?=$item->item_detail?></h4></div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($reportModel, 'report_detail')->widget(Summernote::className(),[]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_file[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    'uploadUrl' => Url::to(['meeting-responsible/create']),
                ],
                //''
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_images[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    'uploadUrl' => Url::to(['meeting-responsible/create']),
                ],
                //''
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_date')->widget(DatePicker::className(), [
                'options' => [
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'size' => 'xs',
                    'autoclose' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'responsible_status')->dropDownList($model::getResponsibleStatusList()) ?>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-sm-12">
        <?php
/*        $forms = [
            [
                'label' => '<i class="fa fa-file"></i> ' . Html::encode('ประวัติการรายงาน'),
                'content' => $this->render('_viewMeetingReport', [
                    'row' => \yii\helpers\ArrayHelper::toArray($model->meetingReports),
                ]),
            ],
        ];
        echo kartik\tabs\TabsX::widget([
            'items' => $forms,
            'position' => kartik\tabs\TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'pluginOptions' => [
                'bordered' => true,
                'sideways' => true,
                'enableCache' => false,
            ],
        ]);
        */?>
        </div>
    </div>-->

    <?= $form->errorSummary([$model, $reportModel]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'บันทึก', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
