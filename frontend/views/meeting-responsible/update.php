<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingResponsible */

$this->title = 'Update Meeting Responsible: ' . ' ' . $model->responsible_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Responsible', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->responsible_id, 'url' => ['view', 'id' => $model->responsible_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="meeting-responsible-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
