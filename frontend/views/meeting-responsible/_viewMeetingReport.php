<div class="form-group" id="add-meeting-report">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$gridColumnMeetingReport = [
    ['class' => 'yii\grid\SerialColumn'],
    'report_id',
    'report_detail:ntext',
    'report_date',
    'report_file:ntext',
    'report_images:ntext',
];
echo Gridview::widget([
    'dataProvider' => $providerMeetingReport,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-report']],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Meeting Report'),
    ],
    'export' => false,
    'columns' => $gridColumnMeetingReport
]);
?>

