<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MeetingResponsible */
/* @var $item common\models\MeetingCommandItems */
/* @var $reportModel common\models\MeetingReport */

$this->title = 'รายงานข้อสั่งการ ';

?>
<div class="meeting-responsible-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'item'=>$item,
        'reportModel'=>$reportModel
    ]) ?>

</div>
