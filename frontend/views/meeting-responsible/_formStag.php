<?php

use kartik\date\DatePicker;
use kartik\grid\GridView;
use marqu3s\summernote\Summernote;
use yii\bootstrap4\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingResponsible */
/* @var $form yii\widgets\ActiveForm */
/* @var $reportModel common\models\MeetingReport */
/* @var $item common\models\MeetingCommandItems */

/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MeetingCommandLog', 
        'relID' => 'meeting-command-log', 
        'value' => \yii\helpers\Json::encode($model->meetingCommandLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MeetingReport', 
        'relID' => 'meeting-report', 
        'value' => \yii\helpers\Json::encode($model->meetingReports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);*/
?>

<div class="meeting-responsible-form">

    <?php $form = \yii\bootstrap4\ActiveForm::begin([
        'id' => 'create-meeting-responsible-stag',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validationUrl' => Url::to(['validate-meeting-responsible-stag']),
        'options' => [
            'class' => 'kt-form kt-form--fit kt-form--label-right',
            'enctype' => 'multipart/form-data',
        ],
    ]);


    //echo Html::activeHiddenInput($model, 'responsible_id');
    //echo Html::activeHiddenInput($model, 'mission_id');
    //echo Html::activeHiddenInput($model, 'item_id');
    echo Html::activeHiddenInput($reportModel, 'report_id');
    echo Html::activeHiddenInput($reportModel, 'report_path_temp');
    //echo Html::activeHiddenInput($reportModel, 'images_tmp');


    //$mission_ids = ArrayHelper::getColumn($model,'mission_id');
    $list = ArrayHelper::merge([''=>'เลือกผู้รับผิดชอบ'],ArrayHelper::map($model,'responsible_id','mission.mission_name'))
    ?>

    <div class="well well-sm">
        <h4><?= $item->item_detail ?></h4>
        <p>กลุ่มงาน/กล่องภารกิจ ที่เกี่ยวข้อง</p>
        <?php
        $responsible = new ActiveDataProvider([
            'models' => $model,
            'pagination' => false,
        ]);
        $gridColumn = [
            /*[
                'class' => '\yii\grid\RadioButtonColumn',
                'radioOptions' => function ($model) {
                    return [
                        'value' => $model['responsible_id'],
                        'id'=> "meetingreport-{$model['responsible_id']}-_responsible_id",
                        'name'=>'MeetingReport[_responsible_id]'
                    ];
                },
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'name' => 'MeetingReport[_responsible_id]',
            ],*/
            [
                'attribute' => 'mission_id',
                'value' => function ($model) {
                    return $model->mission->mission_name;
                },
            ], [
                'attribute' => 'responsible_status',
                'label' => 'สถานะการดำเนินการ',
                'value' => function ($model) {
                    return $model->meetingResponsibleStyle();
                },
                'format' => 'raw',
            ],
        ];
        echo Gridview::widget([
            'dataProvider' => $responsible,
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-command']],
            'panel' => false,
            'export' => false,
            'columns' => $gridColumn,
        ]);
        ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($reportModel, 'report_detail')->widget(Summernote::className(), []) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_file[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    'uploadUrl' => Url::to(['meeting-responsible/create']),
                ],
                //''
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_images[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    'uploadUrl' => Url::to(['meeting-responsible/create']),
                ],
                //''
            ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($reportModel, '_responsible_id')->dropDownList($list) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($reportModel, '_responsible_status')->dropDownList($reportModel::getResponsibleStatusList()) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($reportModel, 'report_date')->widget(DatePicker::className(), [
                'options' => [
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'size' => 'xs',
                    'autoclose' => true,
                ],
            ]) ?>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-sm-12">
        <?php
    /*        $forms = [
                [
                    'label' => '<i class="fa fa-file"></i> ' . Html::encode('ประวัติการรายงาน'),
                    'content' => $this->render('_viewMeetingReport', [
                        'row' => \yii\helpers\ArrayHelper::toArray($model->meetingReports),
                    ]),
                ],
            ];
            echo kartik\tabs\TabsX::widget([
                'items' => $forms,
                'position' => kartik\tabs\TabsX::POS_ABOVE,
                'encodeLabels' => false,
                'pluginOptions' => [
                    'bordered' => true,
                    'sideways' => true,
                    'enableCache' => false,
                ],
            ]);
            */ ?>
        </div>
    </div>-->

    <?= $form->errorSummary([$reportModel]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'บันทึก', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
