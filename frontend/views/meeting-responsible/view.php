<?php

use common\models\Mission;
use kartik\grid\GridView;
use yii\bootstrap4\Carousel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingResponsible */
$user = Yii::$app->user->identity;
$this->title = $model->item->item_detail;
?>
<div class="meeting-responsible-view">
    <div class="row">
        <div class="col-sm-12">
            <?php
            $title = $this->title;
            $gridColumn = [

                [
                    'label' => 'รายละเอียด',
                    'value' => function ($model) use ($title) {
                        return $title;
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'mission_id',
                    'value' => function ($model) {
                        return $model->mission->mission_name;
                    },
                ],
                [
                    'attribute' => 'responsible_status',
                    'value' => function ($model) {
                        return $model::getResponsibleStatusItem($model->responsible_status);
                    },
                ],
                'updated_date:dateTime',
            ];
            echo DetailView::widget([
                'model' => $model,
                'attributes' => $gridColumn,
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php
            if ($providerMeetingReport->totalCount) {
                $gridColumnMeetingReport = [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'options' => [
                            'width' => '50px',
                        ],
                    ],
                    [
                        'attribute' => 'report_detail',
                        'format' => 'raw',
                        'width' => '30%',
                        'value' => function ($model) {
                            $status = $model::getResponsibleStatusItem($model->responsible->responsible_status);
                            return $model->report_detail . '<br>สถานะ ' . $status . '<br> วันที่รายงาน ' . Yii::$app->formatter->asDate($model->report_date);
                        },
                    ],
                    [
                        'label' => 'เอกสารแนบ',
                        'format' => 'raw',
                        'width' => '30%',
                        'value' => function ($model) {
                            $a = $model->viewFiles('report_file');
                            //print_r($a[1]);
                            $carousel = "";
                            if ($a[1] != []) {
                                $c = Carousel::widget([
                                    'id' => 'report_file' . $model->report_id,
                                    'items' =>
                                    // the item contains only the image
                                        $a[1],
                                ]);

                                $carousel = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ภาพ
                                                                </h3>
                                                            </div>                                                      
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$c}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                            }

                            if ($a[0] != "") {
                                $files = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ดาวน์โหลด
                                                                </h3>
                                                            </div>   
                                                            <div class=\"kt-portlet__head-toolbar\">
                                                                " . Html::a('<i class="flaticon2-download"></i>', ['meeting-report/download-zip', 'id' => $model->report_id], ['class' => 'btn btn-outline-danger btn-sm btn-icon btn-icon-sm', 'title' => 'ดาวน์โหลดทั้งหมด', 'target' => '_blank', 'data-pjax' => 0]) . "
                                                            </div>                                                   
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$a[0]}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                            }
                            //var_dump($a[0]);
                            return "<div class=\"row\">{$carousel}\t{$files}</div>";
                        },
                    ],
                    [
                        'label' => 'ภาพกิจกรรม',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $a = $model->viewFiles('report_images');
                            //print_r($a[1]);
                            $carousel = "";
                            if ($a[1] != []) {
                                $c = Carousel::widget([
                                    'id' => 'report_images' . $model->report_id,
                                    'items' =>
                                    // the item contains only the image
                                        $a[1],
                                ]);

                                $carousel = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ภาพ
                                                                </h3>
                                                            </div>                                                      
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$c}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                            }

                            if ($a[0] != "") {
                                $files = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ดาวน์โหลด
                                                                </h3>
                                                            </div>   
                                                            <div class=\"kt-portlet__head-toolbar\">
                                                                " . Html::a('<i class="flaticon2-download"></i>', ['meeting-report/download-zip', 'id' => $model->report_id], ['class' => 'btn btn-outline-danger btn-sm btn-icon btn-icon-sm', 'title' => 'ดาวน์โหลดทั้งหมด', 'target' => '_blank', 'data-pjax' => 0]) . "
                                                            </div>                                                   
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$a[0]}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                            }
                            //var_dump($a[0]);
                            return "<div class=\"row\">{$carousel}\t{$files}</div>";
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '<div  class="btn-group btn-group-sm text-center" role="group">{update} {delete}</div>',
                        'header' => 'จัดการ',

                        'vAlign' => GridView::ALIGN_TOP,
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'viewOptions' => [
                            'class' => 'btn btn-primary',
                            //'data-toggle' => 'tooltip',
                        ],
                        'updateOptions' => [
                            'class' => 'updateReport btn btn-warning',
                            //'data-toggle' => 'tooltip',
                            'title' => 'แก้ไข',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-danger deleteReport',
                            //'data-toggle' => 'tooltip',
                            //'data-confirm' => 'คุณต้องการลบข้อมูลอุปกรณ์นี้ ?',
                            //'data-pjax'=>1
                        ],
                        'noWrap' => true,
                        'contentOptions' => [
                            'noWrap' => true,
                            // 'style' => 'width:160px;',
                        ],
                        'buttons' => [
                            'delete' => function ($url, $model) use ($user) {
                                if ($user->mission_id == Mission::STAG || $user->mission_id == $model->responsible->mission_id) {
                                    return Html::a('<span class="fa fa-trash"></span>', 'javascript:(void)', [
                                        'title' => 'ลบ',
                                        'class' => 'btn btn-danger deleteReport',
                                        'data-url' => Url::to(['meeting-report/delete', 'id' => $model->report_id]),
                                    ]);
                                    //
                                } else {
                                    return '';
                                }
                            },

                            'update' => function ($url, $model) use ($user){
                                if ($user->mission_id == Mission::STAG || $user->mission_id == $model->responsible->mission_id) {
                                    return Html::a('<span class="fa fa-edit"></span>', 'javascript:(void)', [
                                        'title' => 'แก้ไข',
                                        'class' => 'btn btn-warning lovOpen-2',
                                        //'data-pjax' => 0,
//                                        'data-url'=> Url::to(['meeting-command-items/update', 'id' => $model->command_id])
                                        'data-url'=>Url::to(['meeting-report/update', 'id' => $model->report_id])
                                    ]);
                                }
                            },

                        ],
                    ],
                ];
                echo Gridview::widget([
                    'dataProvider' => $providerMeetingReport,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-report']],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<span class="fa fa-tasks"></span> ' . Html::encode('รายละเอียดการรายงาน'),
                    ],
                    'export' => false,
                    'columns' => $gridColumnMeetingReport,
                ]);
            } else {
                echo "<div class='alert alert-danger text-center' role='alert'><h4>ยังไม่มีการรายงานผล</h4></div>";
            }
            ?>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-sm-12">
            <?php
    /*            if ($providerMeetingCommandLog->totalCount) {
                    $gridColumnMeetingCommandLog = [
                        ['class' => 'yii\grid\SerialColumn'],
                        'log_id',
                        'old_status',
                        'new_status',
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerMeetingCommandLog,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-command-log']],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Meeting Command Log'),
                        ],
                        'export' => false,
                        'columns' => $gridColumnMeetingCommandLog,
                    ]);
                }
                */ ?>
        </div>
    </div>-->


</div>
