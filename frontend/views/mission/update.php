<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Mission */

$this->title = 'Update Mission: ' . $model->mission_id;
$this->params['breadcrumbs'][] = ['label' => 'Missions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mission_id, 'url' => ['view', 'id' => $model->mission_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mission-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
