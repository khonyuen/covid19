<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DocumentSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
        //'layout' => 'inline',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
            'class' => 'kt-form kt-form--label-right',
        ],
    ]); ?>
    <div class="kt-portlet__body">
        <div class="form-group row">
            <div class="col-lg-4">
                <?= $form->field($model, 'title') ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'description') ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'document_type') ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <?= $form->field($model, 'tags') ?>
            </div>
            <div class="col-lg-6">
                <?php echo $form->field($model, 'document_name') ?>
            </div>
        </div>

        <?php // echo $form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'has_thumbnail') ?>



        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

