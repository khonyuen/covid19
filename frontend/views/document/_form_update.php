<?php

use common\models\DocumentType;
use kartik\file\FileInput;
use marqu3s\summernote\Summernote;
use t01\assets\TagsInputAsset;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Document */
/* @var $form yii\bootstrap4\ActiveForm */

TagsInputAsset::register($this);
?>

<?php $form = ActiveForm::begin([
//        'layout' => '',
    'id' => 'create-document',
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'enableAjaxValidation' => true,
    'validationUrl' => Url::to(['validate-create-document']),
    'options' => [
        'class' => 'kt-form kt-form--fit kt-form--label-right',
        'enctype' => 'multipart/form-data',
    ],
]);

echo Html::activeHiddenInput($model, 'files_path');
echo Html::activeHiddenInput($model, 'mission_id');

?>
    <div class="kt-portlet__body">
        <?php $form->errorSummary($model); ?>
        <div class="form-group row form-group-marginless kt-margin-t-20">
            <div class="col-lg-12">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="form-group row form-group-marginless kt-margin-t-20">
            <div class="col-lg-12">
                <?= $form->field($model, 'description')->widget(Summernote::className(), []) ?>
            </div>
        </div>

        <!--<div class="form-group row form-group-marginless kt-margin-t-20">
            <div class="col-lg-12">
                <?/*= $form->field($model, 'document_name')->textInput(['maxlength' => true]) */?>
            </div>
        </div>-->

        <div class="form-group row form-group-marginless kt-margin-t-20">
            <div class="col-lg-3">
                <?= $form->field($model, 'document_type')->dropDownList(DocumentType::getList()) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'tags', [
                    'inputOptions' => ['data-role' => 'tagsinput'],
                ])->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-lg-3">
                <?= $form->field($model, 'status')->radioList(['1' => 'เผยแพร่', '0' => 'จำกัด']) ?>
            </div>
        </div>

        <div class="form-group row form-group-marginless kt-margin-t-20">
            <div class="col-lg-12">
                <?= $form->field($model, 'files')->widget(FileInput::className(), [
                    'language' => 'th',
                    'options' => [
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'initialPreview' => $model->initialPreview($model->files, 'files', 'files'),
                        'previewFileType' => ['image', 'html', 'text', 'video', 'audio', 'flash', 'object','rar','zip'], //any
                        //'allowedFileExtensions' => ['jpg', 'png'],
                        'showPreview' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                        'overwriteInitial' => false,
                        'autoReplace' => true,
                        'maxFileCount' => 20,
                        'maxFileSize'=>28000,
                        'purifyHtml' => true,
                        //'deleteUrl'=>Url::to(['area/deletefile','id'=>$model->area_id, 'field'=>'area_pic','fileName'=>'']),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => $model->initialPreview($model->files, 'files', 'config'),
                        'filepredelete' => new \yii\web\JsExpression(
                            'function(jqXHR) {
                                        var abort = true;
                                        if (confirm("ต้องการลบรูปภาพนี้ ?")) {
                                            abort = false;
                                        }
                                        return abort;
                                    }'
                        ),
                    ],

                ]) ?>
            </div>
        </div>


        <?php // $form->field($model, 'created_by')->textInput() ?>

        <?php // $form->field($model, 'created_at')->textInput() ?>

        <?php // $form->field($model, 'updated_by')->textInput() ?>

        <?php // $form->field($model, 'updated_at')->textInput() ?>

        <div class="kt-portlet__foot kt-portlet__foot--fit-x">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
                        <?= Html::submitButton('ยกเลิก', ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>


<?php

$this->registerJs("
$(document).on(\"beforeSubmit\", \"form#create-document\", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();

        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);
        KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'กำลังบันทึกข้อมูล...'
            });
        $.ajax({
                    cache: false,
                    contentType: false,
                    processData: false,
                    url: $(this).attr(\"action\"),
                    type: \"post\",
                    data: formData,//form.serialize(),
                    success: function (response) {
                        // do something with response
                        console.log(response);
                        //unblockUi('blockPanel');
                        response = JSON.parse(response);                      
                       swal.fire(
                            response.title,
                            response.message,
                            response.status
                        );
                        if(response.status=='success'){
                            //$(\"form#create-document\")[0].reset();
                        }
                        KTApp.unblockPage();
                    },
                    complete: function () {
                        //unblockUi('blockPanel');
                        KTApp.unblockPage();
                    },
                    error: function (error) {
                        //unblockUi('blockPanel');
                        console.info(error);
                        KTApp.unblockPage();
                    }
                });
        
        return false;
    });
");

?>