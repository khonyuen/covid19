<?php

use yii\bootstrap4\Carousel;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Document */

$this->title = $model->title;

if (Yii::$app->user->isGuest) {
    $this->params['breadcrumbs'][] = ['label' => 'เอกสารทั้งหมด', 'class' => 'kt-subheader__breadcrumbs-link', 'url' => ['site/index-dashboard']];
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'class' => 'kt-subheader__breadcrumbs-link',];
} else {

    $this->params['breadcrumbs'][] = ['label' => 'เอกสารทั้งหมด', 'class' => 'kt-subheader__breadcrumbs-link', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'class' => 'kt-subheader__breadcrumbs-link',];
}
\yii\web\YiiAsset::register($this);
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-information kt-font-brand"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <?php
                            if (Yii::$app->user->id == $model->created_by) :
                                ?>
                                <?= Html::a('<i class="flaticon2-edit"></i>', ['document/update', 'id' => $model->document_id], ['class' => 'btn btn-outline-success btn-sm btn-icon btn-icon-sm']) ?>
                                <?= Html::a('<i class="flaticon2-trash"></i>', ['delete', 'id' => $model->document_id], [
                                'class' => 'btn btn-outline-danger btn-sm btn-icon btn-icon-sm deleteDoc',
                                'data' => [
                                    'confirm' => 'ยืนยันการลบข้อมูลนี้ ?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
													<span class="kt-section__info">
													</span>
                        <div class="kt-section__content">
                            <div class="table-responsive">

                                <?= DetailView::widget([
                                    'model' => $model,
                                    'template' => '<tr ><th style="width: 20%" {captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>',
                                    'attributes' => [
//                                        'document_id',
                                        'title',
                                        //'document_name',
                                        'description:raw',
                                        'documentTypeName:raw',
                                        [
                                            'label' => 'เอกสารแนบ',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                $a = $model->viewFiles();
                                                //print_r($a[1]);
                                                $carousel = "";
                                                if ($a[1] != []) {
                                                    $c = Carousel::widget([
                                                        'items' =>
                                                        // the item contains only the image
                                                            $a[1],
                                                    ]);

                                                    $carousel = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ภาพ
                                                                </h3>
                                                            </div>                                                      
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$c}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                                                }

                                                if ($a[0] != "") {
                                                    $files = "<div class='col-md-12'><div class=\"kt-portlet kt-portlet--head-noborder\">
                                                        <div class=\"kt-portlet__head\">
                                                            <div class=\"kt-portlet__head-label\">
                                                                <h3 class=\"kt-portlet__head-title  kt-font-danger\">
                                                                    ดาวน์โหลด
                                                                </h3>
                                                            </div>   
                                                            <div class=\"kt-portlet__head-toolbar\">
                                                                " . Html::a('<i class="flaticon2-download"></i>', ['document/download-zip', 'id' => $model->document_id], ['class' => 'btn btn-outline-danger btn-sm btn-icon btn-icon-sm', 'title' => 'ดาวน์โหลดทั้งหมด', 'target' => '_blank']) . "
                                                            </div>                                                   
                                                        </div>
                                                        <div class=\"kt-portlet__body kt-portlet__body--fit-top\">
                                                                <div class=\"kt-section kt-section--space-sm\">
                                                                   {$a[0]}
                                                                </div>                                                           
                                                                </div>
                                                            </div>
                                                      </div>";
                                                }
                                                //var_dump($a[0]);
                                                return "<div class=\"row\">{$carousel}\t{$files}</div>";
                                            },
                                        ],

                                        'tags',
                                        'statusDoc',
//                                        'has_thumbnail',
                                        [
                                            'label' => 'ผู้บันทึก',
                                            'value' => function ($model) {
                                                return $model->createdBy->getFullname();
                                            },
                                        ],
                                        'created_at:date',
                                        [
                                            'label' => 'ผู้แก้ไข',
                                            'value' => function ($model) {
                                                return $model->updatedBy->getFullname();
                                            },
                                        ],
                                        'updated_at:date',
                                    ],
                                ]) ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>