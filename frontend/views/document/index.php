<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เอกสารทั้งหมด';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

use common\models\DocumentType;
use common\models\Mission;
use yii\bootstrap4\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$user = Yii::$app->user->identity;

?>
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-information kt-font-brand"></i></div>
                <div class="alert-text">
                    ระบบจัดเก็บและเผยแพร่เอกสารเกี่ยวกับกรณีโรคระบาด COVID-19 สสจ.มุกดาหาร
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-document kt-font-brand"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">

                        </span>
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <?php Pjax::begin([
                                    'id' => 'document-pjax-container',
                                ]); ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'pager' => ['class'=> LinkPager::className()],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        //'document_id',
                                        [
                                            'attribute' => 'documentTypeName',
                                            'filter' => DocumentType::getList(),
                                            'format' => 'raw'
                                        ],
                                        'title',
                                        //'document_name',
                                        //'description:raw',

                                        /*[
                                            'attribute' => 'documentType',
                                            'value' => function ($model){

                                            }
                                        ],*/
                                        'tags',
                                        //'status',
                                        //'has_thumbnail',

                                        //'created_by',
                                        [
                                            'attribute' => 'missionName',
                                            'filter' => Mission::getList(),
                                        ],
                                        [
                                            'attribute' => 'created_at',
                                            'format' => 'datetime',
                                            'filter' => false,
                                        ],
                                        //'updated_by',
                                        //'updated_at',

                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{view} {update} {delete}</div>',
                                            'header' => 'จัดการ',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-search-plus"></i>', $url, [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'update' => function ($url, $model) use ($user) {
                                                    if ($model->mission_id == $user->mission_id) {
                                                        return Html::a('<i class="la la-edit"></i>', $url, [
                                                            'title' => 'แก้ไขข้อมูล',
                                                            'class' => 'btn btn-warning',
                                                            'data-pjax' => 0,
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                }, 'delete' => function ($url, $model) use ($user) {
                                                    if ($model->mission_id == $user->mission_id) {
                                                        return Html::a('<i class="la la-trash"></i>', $url, [
                                                            'title' => 'ลบข้อมูล',
                                                            'class' => 'btn btn-danger deleteItem',
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>


<?php

$this->registerJs("$(\"body\").on(\"click\", '.deleteItem', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        
        swal.fire({
                title: 'ยืนยันการลบข้อมูล ?',
                text: \"คุณต้องการลบข้อมูลนี้ !\",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ใช่, ลบมันซะ!'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: url,
                        type: \"post\",
                        //data: {key: id},
                        success: function (response) {
                            var res = JSON.parse(response);
                            console.log(res.msg);
                            
                            swal.fire(
                            'ลบข้อมูลเรียบร้อย!',                           
                            'success');
                            
                            $.pjax.reload({container: '#document-pjax-container'});
                            
                        },
                        error: function (error) {
                            console.log(error)
                        }
                    });
                }
            });
        });
");