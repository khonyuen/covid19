<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommand */

$this->title = $model->command_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Command', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-command-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting Command'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->command_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->command_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'command_id',
        'command_name:ntext',
        'commander_name',
        'command_status',
        'command_urgency',
        'command_enddate',
        [
            'attribute' => 'meeting.meeting_id',
            'label' => 'Meeting',
        ],
        [
            'attribute' => 'createdUser.username',
            'label' => 'Created User',
        ],
        [
            'attribute' => 'updatedUser.username',
            'label' => 'Updated User',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Meeting<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeeting = [
        'meeting_name',
        'meeting_no',
        'meeting_date',
        'meeting_time',
        'meeting_status',
        [
            'attribute' => 'createdUser.username',
            'label' => 'Created User',
        ],
        [
            'attribute' => 'updatedUser.username',
            'label' => 'Updated User',
        ],
    ];
    echo DetailView::widget([
        'model' => $model->meeting,
        'attributes' => $gridColumnMeeting    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->createdUser,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->updatedUser,
        'attributes' => $gridColumnUser    ]);
    ?>
    
    <div class="row">
<?php
if($providerMeetingCommandItems->totalCount){
    $gridColumnMeetingCommandItems = [
        ['class' => 'yii\grid\SerialColumn'],
            'item_id',
            'item_detail:ntext',
            'item_urgency',
            'item_enddate',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerMeetingCommandItems,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-command-items']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Meeting Command Items'),
        ],
        'export' => false,
        'columns' => $gridColumnMeetingCommandItems
    ]);
}
?>

    </div>
</div>
