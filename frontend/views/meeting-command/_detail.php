<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommand */

?>
<div class="meeting-command-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->command_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'command_id',
        'command_name:ntext',
        'commander_name',
        'command_status',
        'command_urgency',
        'command_enddate',
        [
            'attribute' => 'meeting.meeting_id',
            'label' => 'Meeting',
        ],
        [
            'attribute' => 'createdUser.username',
            'label' => 'Created User',
        ],
        [
            'attribute' => 'updatedUser.username',
            'label' => 'Updated User',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>