<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;

/*$items = [
    [
        'label' => '<i class="fa fa-tasks"></i> ' . Html::encode('รายละเอียดของข้อสั่งการ'),
        'content' => $this->render('_dataMeetingCommandItems', [
            'model' => $model,
            'row' => $model->meetingCommandItems,
        ]),
    ],
];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    //'bordered' => true,
    'encodeLabels' => false,
   // 'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false,
    ],
]);*/
if($model->meetingCommandItems) {
    echo $this->context->renderPartial('//meeting-command/_dataMeetingCommandItems', [
        'model' => $model,
        'row' => $model->meetingCommandItems,
    ]);
}
?>
