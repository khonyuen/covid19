<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommand */

$this->title = 'Update Meeting Command: ' . ' ' . $model->command_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Command', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->command_id, 'url' => ['view', 'id' => $model->command_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="meeting-command-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
