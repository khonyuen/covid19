<?php

use common\models\Mission;
use frontend\controllers\MeetingCommandItemsController;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;


$user = Yii::$app->user->identity;

$dataProvider = new ArrayDataProvider([
    'allModels' => $model->meetingCommandItems,
    'key' => 'item_id',
]);
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    //'item_id',
    [
        'label'=>'ความเร่งด่วน',
        'value'=>function($model){
                $urgency = $model->commandItemUrgencyStyle();
            return $urgency;
        },
        'format'=>'raw'
    ],
    [
        'attribute' => 'item_detail',
        'format' => 'ntext',
        'value' => function ($model) {
            return $model->item_detail; //." ".$urgency;
        },
        'width'=>'70%'
    ],
    [
        'attribute' => '_responsible',
        'value' => function ($model) {
            $dep = MeetingCommandItemsController::genResponsible($model);
            return $dep;
        },
        'format' => 'raw',
        'width'=>'15%'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '<div class="btn-group btn-group-sm text-center" role="group">{report} {update} {delete}</div>', // approve
        //'template' => '{view} {update} {delete}', //{save-as-new}
        'header' => '',
        //'width' => '200px;',
        //                'headerOptions' => [
        //                        'style'=>'width:140px;',
        //                    'width'=>'140px'
        //                ],
        //'options' => ['style' => 'width:15%;'],
        'vAlign' => GridView::ALIGN_TOP,
        'buttonOptions' => ['class' => 'btn btn-default'],
        'viewOptions' => [
            'class' => 'btn btn-primary',
            //'data-toggle' => 'tooltip',
        ],
        'updateOptions' => [
            'class' => 'updateUser btn btn-warning',
            //'data-toggle' => 'tooltip',
            'title' => 'แก้ไข',
        ],
        'deleteOptions' => [
            'class' => 'btn btn-danger deleteCommandItem',
            //'data-toggle' => 'tooltip',
            //'data-confirm' => 'คุณต้องการลบข้อมูลอุปกรณ์นี้ ?',
            //'data-pjax'=>1
        ],
        'noWrap' => true,
        'contentOptions' => [
            'noWrap' => true,
            // 'style' => 'width:160px;',
        ],
        'buttons' => [
            'delete' => function ($url, $model) use ($user) {
                if ($user->mission_id == Mission::STAG) {
                    return Html::a('<span class="fa fa-trash"></span>', '#', [
                        'title' => 'ลบ',
                        'class' => 'btn btn-danger deleteCommandItem',
                        'data-url'=>Url::to(['meeting-command-items/delete', 'id' => $model->item_id])
                    ]);
                } else {
                    return '';
                }
            },
            'report' => function ($url, $model) use ($user) {
                if (in_array($user->mission_id,$model->getResponsibleIds())) {
                    return Html::a('<span class="fa fa-chevron-circle-right"></span>', '#', [
                        'title' => 'รายงานความก้าวหน้า',
                        'class' => 'btn btn-primary lovOpen',
                        'data-url' => Url::to(['meeting-responsible/create', 'id' => $model->item_id,'mission_id'=>$user->mission_id]),
                    ]);
                } elseif($user->mission_id==Mission::STAG) {
                    return Html::a('<span class="fa fa-chevron-circle-right"></span>', '#', [
                        'title' => 'รายงานความก้าวหน้า',
                        'class' => 'btn btn-primary lovOpen',
                        'data-url' => Url::to(['meeting-responsible/create-stag', 'id' => $model->item_id,'mission_id'=>$user->mission_id]),
                    ]);
                }
            },
            'update' => function ($url, $model) use ($user) {
                if ($user->mission_id == Mission::STAG) {
                return Html::a('<span class="fa fa-edit"></span>', 'javascript:(void)', [
                    'title' => 'แก้ไขรายละเอียด',
                    'class' => 'btn btn-warning lovOpen',
                    'data-pjax' => 0,
                    'data-url'=>Url::to(['meeting-command-items/update', 'id' => $model->item_id])
                ]);
                //} else {
                //    return '';
                }
            },
            'view' => function ($url, $model) {

                return Html::a('<span class="fa fa-eye"></span>', '#', [
                    'title' => 'ดูรายงาน',
                    'class' => 'btn btn-info',
                    'data-pjax' => 0,
                ]);
                //Url::to(['meeting-command/view', 'id' => $model->command_id])

            },
        ],
    ]
];

echo GridView::widget([
    //'id'=>'table-',
    'dataProvider' => $dataProvider,
//        'panel' => [
//            'type'=>GridView::TYPE_PRIMARY,
//            'heading'=>false
//        ],
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'pjax' => true,
    'beforeHeader' => [
        [
            'options' => ['class' => 'skip-export'],
        ],
    ],
    'export' => [
        'fontAwesome' => true,
    ],

    'bordered' => true,
    'striped' => false,
    //'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'persistResize' => true,
    'exportConfig' => false,
]);