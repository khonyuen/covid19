<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidCountry */

$this->title = 'Update Covid Country: ' . ' ' . $model->covid_country_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Country', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->covid_country_id, 'url' => ['view', ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="covid-country-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
