<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CovidCountry */

$this->title = 'Create Covid Country';
$this->params['breadcrumbs'][] = ['label' => 'Covid Country', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-country-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
