<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidCountry */

$this->title = $model->covid_country_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Country', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-country-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Covid Country'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'covid_country_id',
        'covid_country_name',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
