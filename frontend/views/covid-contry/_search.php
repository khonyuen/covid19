<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidCountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-covid-country-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'covid_country_id')->textInput(['maxlength' => true, 'placeholder' => 'Covid Country']) ?>

    <?= $form->field($model, 'covid_country_name')->textInput(['maxlength' => true, 'placeholder' => 'Covid Country Name']) ?>

    <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
