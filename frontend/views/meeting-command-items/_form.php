<?php

use common\models\BaseActiveRecord;
use common\models\Mission;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use marqu3s\summernote\Summernote;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommandItems */
/* @var $form yii\widgets\ActiveForm */

/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MeetingResponsible',
        'relID' => 'meeting-responsible',
        'value' => \yii\helpers\Json::encode($model->meetingResponsibles),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);*/
?>

<div class="meeting-command-items-form">

    <?php $form = ActiveForm::begin([
        'id' => 'create-command-item',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validationUrl' => Url::to(['validate-command-item']),
        'options' => [
            'class' => 'kt-form kt-form--fit kt-form--label-right',
            'enctype' => 'multipart/form-data',
        ],
    ]);

    echo Html::activeHiddenInput($model,'command_id');
    ?>

    <?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-12">
            <p><?=$model->command->command_name?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'item_detail')->textarea(['rows'=>10])//widget(Summernote::className(), []) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, '_responsible[]')->widget(Select2::className(), [
                'data' => Mission::getList(),
                'options' => [
                    'multiple' => true,
                    'class'=>'form-control-m'
                ],
                'theme' => Select2::THEME_DEFAULT
            ]); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'item_urgency')->dropDownList(BaseActiveRecord::getUrgency()) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'item_enddate')->widget(DatePicker::classname(), [
                //'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                //'saveFormat' => 'php:Y-m-d',
                //'ajaxConversion' => true,
                'options' => [
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'size' => 'xs',
                    'autoclose' => true,
                ],
            ]); ?>
        </div>
    </div>

    <?php
    /*$forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MeetingResponsible'),
            'content' => $this->render('_formMeetingResponsible', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->meetingResponsibles),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);*/
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'แก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
