<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MeetingCommandItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-meeting-command-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'item_id')->textInput(['placeholder' => 'Item']) ?>

    <?= $form->field($model, 'item_detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'item_urgency')->textInput(['placeholder' => 'Item Urgency']) ?>

    <?= $form->field($model, 'item_enddate')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Item Enddate',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'command_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\MeetingCommand::find()->orderBy('command_id')->asArray()->all(), 'command_id', 'command_id'),
        'options' => ['placeholder' => 'Choose Meeting command'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
