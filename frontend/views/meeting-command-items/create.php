<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommandItems */

$this->title = 'บันทึกรายละเอียดประเด้น ข้อสั่งการ และมอบหมายภารกิจ';

?>
<div class="meeting-command-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
