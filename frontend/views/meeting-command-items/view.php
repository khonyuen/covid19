<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingCommandItems */

$this->title = $model->item_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Command Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-command-items-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting Command Items'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->item_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->item_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'item_id',
        'item_detail:ntext',
        'item_urgency',
        'item_enddate',
        [
            'attribute' => 'command.command_id',
            'label' => 'Command',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MeetingCommand<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeetingCommand = [
        'command_name',
        'commander_name',
        'command_status',
        'command_urgency',
        'command_enddate',
        'meeting_id',
        'created_user',
        'updated_user',
    ];
    echo DetailView::widget([
        'model' => $model->command,
        'attributes' => $gridColumnMeetingCommand    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
    
    <div class="row">
<?php
if($providerMeetingResponsible->totalCount){
    $gridColumnMeetingResponsible = [
        ['class' => 'yii\grid\SerialColumn'],
            'responsible_id',
            [
                'attribute' => 'mission.mission_id',
                'label' => 'Mission'
            ],
                        'responsible_status',
            [
                'attribute' => 'responsibleUser.username',
                'label' => 'Responsible User'
            ],
            'updated_date',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMeetingResponsible,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-responsible']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Meeting Responsible'),
        ],
        'export' => false,
        'columns' => $gridColumnMeetingResponsible
    ]);
}
?>

    </div>
</div>
