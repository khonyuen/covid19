<?php
use yii\helpers\Url;

?>
<script>
    $(document).on("beforeSubmit", "form#create-command-item", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();

        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });
        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    $("form#create-command-item")[0].reset();
                    $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                }
                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });
</script>
