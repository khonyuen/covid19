<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTarget */

$this->title = 'Create Cm Target';
$this->params['breadcrumbs'][] = ['label' => 'Cm Targets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cm-target-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
