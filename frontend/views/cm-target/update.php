<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTarget */

$this->title = 'Update Cm Target: ' . $model->targetgroup_id;
$this->params['breadcrumbs'][] = ['label' => 'Cm Targets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->targetgroup_id, 'url' => ['view', 'id' => $model->targetgroup_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cm-target-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
