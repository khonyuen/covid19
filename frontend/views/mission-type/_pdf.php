<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MissionType */

$this->title = $model->type_id;
$this->params['breadcrumbs'][] = ['label' => 'Mission Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mission-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Mission Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'type_id',
        'type_name',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
