<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MissionType */

$this->title = 'Update Mission Type: ' . ' ' . $model->type_id;
$this->params['breadcrumbs'][] = ['label' => 'Mission Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->type_id, 'url' => ['view', 'id' => $model->type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mission-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
