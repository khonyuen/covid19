<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MissionType */

$this->title = 'Create Mission Type';
$this->params['breadcrumbs'][] = ['label' => 'Mission Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mission-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
