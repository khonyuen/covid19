<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\frontend\models\MissionTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'ประเภทของภารกิจ';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12 mission-type-index">
            <p>
                <?= Html::a('เพิ่มประเภทภารกิจ', ['create'], ['class' => 'btn btn-warning']) ?>
            </p>
            <?php
            $gridColumn = [
                ['class' => 'yii\grid\SerialColumn'],
                'type_id',
                'type_name',
                [
                    'class' => 'yii\grid\ActionColumn',
                ],
            ];
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mission-type']],
                'panel' => [
                    'type' => GridView::TYPE_SUCCESS,
                    'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                ],
                // your toolbar can include the additional full export menu
                'toolbar' => [
                    '{export}',
                    ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumn,
                        'target' => ExportMenu::TARGET_BLANK,
                        'fontAwesome' => true,
                        'dropdownOptions' => [
                            'label' => 'Full',
                            'class' => 'btn btn-default',
                            'itemsBefore' => [
                                '<li class="dropdown-header">Export All Data</li>',
                            ],
                        ],
                    ]) ,
                ],
            ]); ?>

        </div>
    </div>
</div>
