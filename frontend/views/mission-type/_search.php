<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\frontend\models\MissionTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-mission-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'type_id')->textInput(['placeholder' => 'Type']) ?>

    <?= $form->field($model, 'type_name')->textInput(['maxlength' => true, 'placeholder' => 'Type Name']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
