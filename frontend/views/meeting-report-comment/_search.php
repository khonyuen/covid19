<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MeetingReportCommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-meeting-report-comment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comment_id')->textInput(['placeholder' => 'Comment']) ?>

    <?= $form->field($model, 'report_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\MeetingReport::find()->orderBy('report_id')->asArray()->all(), 'report_id', 'report_id'),
        'options' => ['placeholder' => 'Choose Meeting report'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'comment_detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Comment Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'comment_name')->textInput(['maxlength' => true, 'placeholder' => 'Comment Name']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
