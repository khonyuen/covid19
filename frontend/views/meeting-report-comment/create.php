<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MeetingReportComment */

$this->title = 'Create Meeting Report Comment';
$this->params['breadcrumbs'][] = ['label' => 'Meeting Report Comment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-report-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
