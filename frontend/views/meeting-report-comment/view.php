<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingReportComment */

$this->title = $model->comment_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Report Comment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-report-comment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting Report Comment'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->comment_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->comment_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'comment_id',
        [
            'attribute' => 'report.report_id',
            'label' => 'Report',
        ],
        'comment_detail:ntext',
        'comment_date',
        'comment_name',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MeetingReport<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeetingReport = [
        'responsible_id',
        'report_detail',
        'report_date',
        'report_file',
        'report_images',
    ];
    echo DetailView::widget([
        'model' => $model->report,
        'attributes' => $gridColumnMeetingReport    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
</div>
