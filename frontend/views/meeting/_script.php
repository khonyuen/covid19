<?php

?>

<script>
    $("body").on("click", '.deleteItem', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');

        swal.fire({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "คุณต้องการลบข้อมูลนี้ !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ใช่, ลบมันซะ!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "post",
                    //data: {key: id},
                    success: function (response) {
                        var res = JSON.parse(response);
                        console.log(res.msg);

                        if(res.result) {
                            swal.fire(
                                'ลบข้อมูลเรียบร้อย!',
                                'success');

                            $.pjax.reload({container: '#kv-pjax-container-meeting'});
                        }else {
                            swal.fire(
                                'ไม่สามารถลบได้! '+res.msg,
                                'warning');
                        }
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }
        });
    });

    $("body").on("click", '.deleteCommand', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        //var _this = $(this);
        swal.fire({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "คุณต้องการลบข้อมูลนี้ !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ใช่, ลบมันซะ!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "post",
                    //data: {key: id},
                    success: function (response) {
                        var res = JSON.parse(response);
                        console.log(res.status);

                        swal.fire(
                            'ลบข้อมูลเรียบร้อย!',
                            'success');

                        //if(response.status=='success'){
                            $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                        //}
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }
        });
        return;
    });
    $("body").on("click", '.deleteCommandItem', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        var _this = $(this);
        swal.fire({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "คุณต้องการลบข้อมูลนี้ !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ใช่, ลบมันซะ!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "post",
                    //data: {key: id},
                    success: function (response) {
                        var res = JSON.parse(response);
                        console.log(res.msg);

                        swal.fire(
                            'ลบข้อมูลเรียบร้อย!',
                            'success');

                        var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                        if(id_pjax){
                            id_pjax = id_pjax.replace('container','pjax');
                            $.pjax.reload({container: '#'+id_pjax});
                            console.log('reload '+id_pjax);
                        }

                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }
        });
        return;
    });
    $("body").on("click", '.deleteReport', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        var _this = $(this);
        swal.fire({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "คุณต้องการลบข้อมูลนี้ !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ใช่, ลบมันซะ!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "post",
                    //data: {key: id},
                    success: function (response) {
                        var res = JSON.parse(response);
                        console.log(res.msg);

                        swal.fire(
                            'ลบข้อมูลเรียบร้อย!',
                            'success');
                        $.pjax.reload({container: '#kv-pjax-container-meeting-report',url:res.url,"push": false,
                            "replace": false,
                            "pushRedirect": true,
                            "replaceRedirect": false});

                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }
        });
        return;
    });

    $("body").on("click", '.createCommandItem', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');

        swal.fire({
            title: 'ยืนยันการลบข้อมูล ?',
            text: "คุณต้องการลบข้อมูลนี้ !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ใช่, ลบมันซะ!'
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "post",
                    //data: {key: id},
                    success: function (response) {
                        var res = JSON.parse(response);
                        console.log(res.msg);

                        swal.fire(
                            'ลบข้อมูลเรียบร้อย!',
                            'success');

                        $.pjax.reload({container: '#kv-pjax-container-meeting'});

                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            }
        });
    });

    //var lovOpen = function () {
    $('body').on('click', '.lovOpen', function (e) {
        e.preventDefault();
        $('#modal_main').find('#modalContent').html('');
        $('#modal_main').modal('show').find('#modalContent')
            .load($(this).attr('data-url'));
        //dynamiclly set the header for the modal
        //document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('data-text') + '</h4>';

        if ($(this).attr('data-title')) {
            $("#modalHeader").html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button><h4>" + $(this).attr('data-title') + "</h4>");
        } else {
            $("#modalHeader").remove();
        }
    });
    $('body').on('click', '.lovOpen-2', function (e) {
        e.preventDefault();
        $('#modal_main2').find('#modalContent').html('');
        $('#modal_main2').modal('show').find('#modalContent')
            .load($(this).attr('data-url'));
        //dynamiclly set the header for the modal
        //document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('data-text') + '</h4>';

        if ($(this).attr('data-title')) {
            $("#modalHeader2").html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button><h4>" + $(this).attr('data-title') + "</h4>");
        } else {
            $("#modalHeader2").remove();
        }
    });

    /*$("#modal").on('hidden.bs.modal', function (e) {
        $(this).find('#modalContent').html('');
        $(this).data('bs.modal', null);
        console.info(e, 'close clear modal');
    });*/
    //};

    $(document).on("beforeSubmit", "form#create-command-item", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();

        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });
        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    $("form#create-command-item")[0].reset();
                    $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                }
                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });

    $(document).on("beforeSubmit", "form#update-command-item", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();

        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });
        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    $("form#update-command-item")[0].reset();
                    $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                }
                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });

    $(document).on("beforeSubmit", "form#create-meeting-responsible", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();
        var _this = $(this);
        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });

        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                    // if(id_pjax){
                    //     id_pjax = id_pjax.replace('container','pjax');
                        $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                    //     console.log('reload '+id_pjax);
                    // }
                }

                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });

    $(document).on("beforeSubmit", "form#update-meeting-report", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();
        var _this = $(this);
        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });

        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                    // if(id_pjax){
                    //     id_pjax = id_pjax.replace('container','pjax');
                    //     $.pjax.reload({container: '#kv-pjax-container-meeting-command',async:true});
                    //     $.pjax.reload({container: '#kv-pjax-container-meeting-report',
                    //         url:response.url,
                    //         "push": false,
                    //         "replace": false,
                    //         "pushRedirect": true,
                    //         "replaceRedirect": false,
                    //         //async:false
                    //     });
                    $.pjax.reload({container: '#kv-pjax-container-meeting-command'}).done(function () {
                        $.pjax.reload({container: '#kv-pjax-container-meeting-report',
                            url:response.url,
                            "push": false,
                            "replace": false,
                            "pushRedirect": true,
                            "replaceRedirect": false,});
                    });
                    //     console.log('reload '+id_pjax);
                    // }
                    $('#modal_main2').modal('hide')
                }

                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });

    $(document).on("beforeSubmit", "form#create-meeting-responsible-stag", function (e) {
        e.preventDefault();
        e.isImmediatePropagationStopped();
        var _this = $(this);
        //var form = document.getElementById('baseForm');
        var formData = new FormData($(this)[0]);

        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'primary',
            message: 'กำลังบันทึกข้อมูล...'
        });

        $.ajax({
            cache: false,
            contentType: false,
            processData: false,
            url: $(this).attr("action"),
            type: "post",
            data: formData,//form.serialize(),
            success: function (response) {
                // do something with response
                console.log(response);
                //unblockUi('blockPanel');
                response = JSON.parse(response);
                swal.fire(
                    response.title,
                    response.message,
                    response.status
                );
                if(response.status=='success'){
                    // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                    // if(id_pjax){
                    //     id_pjax = id_pjax.replace('container','pjax');
                        $.pjax.reload({container: '#kv-pjax-container-meeting-command'});
                    //     console.log('reload '+id_pjax);
                    // }
                }

                KTApp.unblockPage();
            },
            complete: function () {
                //unblockUi('blockPanel');
                KTApp.unblockPage();
            },
            error: function (error) {
                //unblockUi('blockPanel');
                console.info(error);
                KTApp.unblockPage();
            }
        });

        return false;
    });

    $('.modal').on('hidden.bs.modal', function (e) {
        $('body').addClass('modal-open');
    });
    /*$('#modal_main2').on('hidden.bs.modal', function(){
        $('#modal_main').focus();
    });*/
</script>


