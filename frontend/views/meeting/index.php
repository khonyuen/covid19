<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MeetingSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Mission;
use kartik\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;

$this->title = 'การประชุม';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');
$user = Yii::$app->user->identity;
//echo $user->mission_id;

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,]);
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-infographic"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <?php
                            if ($user->mission_id == Mission::STAG) :
                                ?>
                                <?= Html::a('<i class="flaticon2-add-square"></i> เพิ่มหัวข้อการประชุม', ['meeting/create'], ['class' => 'btn btn-danger']) ?>

                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">
                        </span>
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <div class="meeting-index">
                                    <?php
                                    $gridColumn = [
                                        [
                                            'class' => 'yii\grid\SerialColumn',
                                            'headerOptions' => [
                                                'style' => 'width:50px;',
                                            ],
                                        ],
//                                        'meeting_id',

                                        [
                                            'attribute' => 'meeting_name',
                                            'value' => function ($model) {
                                                $low = "<span class='badge badge-secondary' title='ปกติ'>{$model->_l}</span>";
                                                $md = "<span class='badge badge-warning' title='ด่วน'>{$model->_m}</span>";
                                                $express = "<span class='badge badge-danger' title='ด่วนที่สุด'>{$model->_e}</span>";
                                                $name = $model->meeting_name;

                                                $date = $model::dateThFormat($model->meeting_date);
                                                $txt = "ครั้งที่ ".$model->meeting_no . '<br>วันที่ ' . $date . ' เวลา ' . $model->meeting_time . ' น.';
                                                //$html = $model->formatCommand();
                                                return $name . "<br>" . $txt . "<br>" . $express . ' ' . $md . ' ' . $low;
                                                //.'<br>'.$html;
                                            },
                                            'format' => 'raw',
                                        ],

                                        /*[
                                            'label' => 'ประเด็น/ข้อสั่งการ',
                                            'attribute' => '_commands',
                                            'value' => function ($model) {
                                                $html = $model->formatCommand();
                                                return $html;
                                            },
                                            'format' => 'raw',
                                        ],*/

//                                        'meeting_status',
                                        /*[
                                            'attribute' => 'created_user',
                                            'label' => 'Created User',
                                            'value' => function ($model) {
                                                if ($model->createdUser) {
                                                    return $model->createdUser->username;
                                                } else {
                                                    return NULL;
                                                }
                                            },
                                            'filterType' => GridView::FILTER_SELECT2,
                                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                                            'filterWidgetOptions' => [
                                                'pluginOptions' => ['allowClear' => true],
                                            ],
                                            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-meeting-search-created_user'],
                                        ],
                                        [
                                            'attribute' => 'updated_user',
                                            'label' => 'Updated User',
                                            'value' => function ($model) {
                                                if ($model->updatedUser) {
                                                    return $model->updatedUser->username;
                                                } else {
                                                    return NULL;
                                                }
                                            },
                                            'filterType' => GridView::FILTER_SELECT2,
                                            'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                                            'filterWidgetOptions' => [
                                                'pluginOptions' => ['allowClear' => true],
                                            ],
                                            'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-meeting-search-updated_user'],
                                        ],*/
                                        [
                                            'class' => 'kartik\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{print} {view} {update} {delete}</div>',
                                            'header' => 'จัดการ',
                                            'vAlign' => 'top',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'print' => function ($url, $model) {
                                                    return Html::a('<i class="la la-print"></i>', $url, [
                                                        'title' => 'ออกรายงาน',
                                                        'class' => 'btn btn-info',
                                                        'data-pjax' => 0,
                                                    ]);
                                                },
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-search-plus"></i>', $url, [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'update' => function ($url, $model) use ($user) {
                                                    if (Mission::STAG == $user->mission_id) {
                                                        return Html::a('<i class="la la-edit"></i>', $url, [
                                                            'title' => 'แก้ไขข้อมูล',
                                                            'class' => 'btn btn-warning',
                                                            'data-pjax' => 0,
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                }, 'delete' => function ($url, $model) use ($user) {
                                                    if (Mission::STAG == $user->mission_id) {
                                                        return Html::a('<i class="la la-trash"></i>', $url, [
                                                            'title' => 'ลบข้อมูล',
                                                            'class' => 'btn btn-danger deleteItem',
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                },
                                            ],
                                        ],
                                    ];
                                    ?>
                                    <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
                                        'pager' => ['class' => LinkPager::className()],
                                        'columns' => $gridColumn,
                                        'pjax' => true,
                                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting']],
                                        'panel' => [
                                            'type' => GridView::TYPE_DEFAULT,
                                            'heading' => false, //'<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                                            'before' => false,
                                        ],
                                        'export' => false,
                                        // your toolbar can include the additional full export menu
                                        'toolbar' => false //[
                                        //'{export}',
                                        /*ExportMenu::widget([
                                            'dataProvider' => $dataProvider,
                                            'columns' => $gridColumn,
                                            'target' => ExportMenu::TARGET_BLANK,
                                            'fontAwesome' => true,
                                            'dropdownOptions' => [
                                                'label' => 'Full',
                                                'class' => 'btn btn-default',
                                                'itemsBefore' => [
                                                    '<li class="dropdown-header">Export All Data</li>',
                                                ],
                                            ],
                                            'exportConfig' => [
                                                ExportMenu::FORMAT_PDF => false,
                                            ],
                                        ]),*/
                                        //],
                                    ]); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>