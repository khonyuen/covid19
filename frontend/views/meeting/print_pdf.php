<?php

use common\models\MeetingCommand;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $providerMeetingCommand \yii\data\ArrayDataProvider */


$user = Yii::$app->user->identity;

?>

        <?php
        $gridColumn = [
            //'meeting_id',
            'meeting_name',
            'meeting_no',
            [
                'attribute' => 'meeting_date',
                'value' => function ($model) {
                    return $model::dateEngDb($model->meeting_date);
                },
            ],
            'meeting_time',
        ];
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $gridColumn,

        ]);
        ?>


        <table class="display compact table table-hover table-striped table-condensed table-bordered nowrap" autosize="1.6" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th class="text-center">ประเด็น/ข้อสั่งการ</th>
                <th class="text-center">รายละเอียด<br>กลุ่มงาน/กลุ่มภารกิจที่เกี่ยวข้อง<br>ผลการดำเนินงาน/ปัญหาอุปสรรค
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($providerMeetingCommand->getModels() as $command) {
                /**
                 * @var $command MeetingCommand
                 */
                ?>
                <tr>
                    <td class="text-left" width="30%;" style="vertical-align: top;">
                        <?php
                        echo $command->command_name;
                        if ($command->commander_name) {
                            echo "<br>ผู้สั่งการ : " . $command->commander_name;
                        }
                        echo "<br>กำหนด : " . $command->command_enddate;
                        //if($command->commander_name){
                        echo "<br>" . $command->commandUrgencyStyle();
                        ?>
                    </td>
                    <td class="text-left" width="70%;" style="vertical-align: top;">
                        <?php
                        $commandItems = $command->meetingCommandItems;

                        $responsibles = [];
                        foreach ($commandItems as $index => $commandItem) {
                            ?>
                            <div class="row mb-3">
                                <div class="col-sm-7">
                                    <?php
                                    //$responsibles[] = $commandItem->meetingResponsibles;
                                    echo nl2br($commandItem->item_detail) . "<br>";

                                    ?>
                                </div>
                                <div class="col-sm-5">
                                    <?php
                                    $reportItemLasts = null;
                                    foreach ($commandItem->meetingResponsibles as $responsible) {
                                        $reportItemLasts = $responsible->meetingReportLast;
                                        echo $responsible->meetingResponsibleMissionStyle() . " <br>";

                                        if ($reportItemLasts != null) {
                                            echo $reportItemLasts->reportDetailFile(); //"เอกสาร & รุปภาพ : ".
                                        }
                                        ?>

                                        <?php
                                    }
                                    ?>
                                </div>

                                <?php
                                //}
                                ?>
                            </div>
                            <?php
                        }
                        ?>

                    </td>


                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
