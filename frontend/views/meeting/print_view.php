<?php

use common\models\MeetingCommand;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $providerMeetingCommand \yii\data\ArrayDataProvider */

$this->title = $model->meeting_name;
$user = Yii::$app->user->identity;

$this->params['breadcrumbs'][] = ['label' => 'หัวข้อการประชุมทั้งหมด', 'url' => ['index'], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'class' => 'kt-subheader__breadcrumbs-link'];

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,]);
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-information kt-font-brand"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <?= Html::a('<i class="flaticon2-print"></i>', ['meeting/print-report', 'id' => $model->meeting_id], ['class' => 'btn btn-info','target'=>'_blank']) ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    $gridColumn = [
                                        //'meeting_id',
                                        'meeting_name',
                                        'meeting_no',
                                        [
                                            'attribute' => 'meeting_date',
                                            'value' => function ($model) {
                                                return $model::dateEngDb($model->meeting_date);
                                            },
                                        ],
                                        'meeting_time',
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model,
                                        'attributes' => $gridColumn,

                                    ]);
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered table-hover table-striped" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center">ประเด็น/ข้อสั่งการ</th>
                                            <th class="text-center">รายละเอียด<br>กลุ่มงาน/กลุ่มภารกิจที่เกี่ยวข้อง<br>ผลการดำเนินงาน/ปัญหาอุปสรรค
                                            </th>
                                            <!--<th class="text-center">กลุ่มงาน/กลุ่มภารกิจที่เกี่ยวข้อง</th>-->
                                            <!--<th class="text-center">ผลการดำเนินงาน/ปัญหาอุปสรรค</th>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($providerMeetingCommand->getModels() as $command) {
                                            /**
                                             * @var $command MeetingCommand
                                             */
                                            ?>
                                            <tr>
                                                <td class="text-left" width="30%;" style="vertical-align: top;">
                                                    <?php
                                                    echo $command->command_name;
                                                    if($command->commander_name){
                                                        echo "<br>ผู้สั่งการ : ".$command->commander_name;
                                                    }
                                                    echo "<br>กำหนด : ".$command->command_enddate;
                                                    //if($command->commander_name){
                                                        echo "<br>". $command->commandUrgencyStyle();
                                                    //}
                                                    //if($command->commander_name){
                                                    //    echo "<br>".$command->commander_name;
                                                    //}


                                                    ?>
                                                </td>
                                                <td class="text-left" width="70%;" style="vertical-align: top;">
                                                    <?php
                                                    $commandItems = $command->meetingCommandItems;

                                                    $responsibles = [];
                                                    foreach ($commandItems as $index => $commandItem) {
                                                        ?>
                                                        <div class="row mb-3">
                                                            <div class="col-sm-7">
                                                                <?php
                                                                //$responsibles[] = $commandItem->meetingResponsibles;
                                                                echo nl2br($commandItem->item_detail) . "<br>";

                                                                ?>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <?php
                                                                $reportItemLasts = null;
                                                                foreach ($commandItem->meetingResponsibles as $responsible) {
                                                                    $reportItemLasts = $responsible->meetingReportLast;
                                                                    echo $responsible->meetingResponsibleMissionStyle() . " <br>";

                                                                    if($reportItemLasts!=null){
                                                                        echo  $reportItemLasts->reportDetailFile(); //"เอกสาร & รุปภาพ : ".
                                                                    }
                                                                    ?>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>

                                                            <?php
                                                            //}
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>

                                                </td>


                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
