<?php

use common\models\Mission;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

$this->title = $model->meeting_name;
$user = Yii::$app->user->identity;

$this->params['breadcrumbs'][] = ['label' => 'หัวข้อการประชุมทั้งหมด', 'url' => ['index'], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'class' => 'kt-subheader__breadcrumbs-link'];

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,]);
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-information kt-font-brand"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <?php
                            if ($user->mission_id == Mission::STAG) :
                                ?>
                                <?= Html::a('<i class="flaticon2-edit"></i> แก้ไข', ['meeting/update', 'id' => $model->meeting_id], ['class' => 'btn btn-danger']) ?>

                            <?php
                            endif;
                            ?>

                            <?= Html::a('<i class="flaticon2-print"></i>', ['meeting/print-report', 'id' => $model->meeting_id], ['class' => 'btn btn-info']) ?>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    $gridColumn = [
                                        //'meeting_id',
                                        'meeting_name',
                                        'meeting_no',
                                        [
                                            'attribute' => 'meeting_date',
                                            'value' => function ($model) {
                                                return $model::dateEngDb($model->meeting_date);
                                            },
                                        ],
                                        'meeting_time',
                                        //'meeting_status',
                                        [
                                            'attribute' => 'createdUser.username',
                                            'label' => 'บันทึกโดย',
                                        ],
                                        [
                                            'attribute' => 'updatedUser.username',
                                            'label' => 'แก้ไขโดย',
                                        ],
                                    ];
                                    echo DetailView::widget([
                                        'model' => $model,
                                        'attributes' => $gridColumn,

                                    ]);
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?php

                                    if($user->mission_id==Mission::STAG){
                                        $btn = [
                                            'class' => 'kartik\grid\ActionColumn',
                                            'template' => '<div style="width: 180px;"  class="btn-group btn-group-sm text-center" role="group">{assign} {delete}</div>', // approve
                                            //'template' => '{view} {update} {delete}', //{save-as-new}
                                            'header' => 'จัดการ',
                                            //'width' => '200px;',
                                            //                'headerOptions' => [
                                            //                        'style'=>'width:140px;',
                                            //                    'width'=>'140px'
                                            //                ],
                                            //'options' => ['style' => 'width:15%;'],
                                            'vAlign' => GridView::ALIGN_TOP,
                                            'buttonOptions' => ['class' => 'btn btn-default'],
                                            'viewOptions' => [
                                                'class' => 'btn btn-primary',
                                                //'data-toggle' => 'tooltip',
                                            ],
                                            'updateOptions' => [
                                                'class' => 'updateUser btn btn-warning',
                                                //'data-toggle' => 'tooltip',
                                                'title' => 'แก้ไข',
                                            ],
                                            'deleteOptions' => [
                                                'class' => 'btn btn-danger deleteCommandItem',
                                                //'data-toggle' => 'tooltip',
                                                //'data-confirm' => 'คุณต้องการลบข้อมูลอุปกรณ์นี้ ?',
                                                //'data-pjax'=>1
                                            ],
                                            'noWrap' => true,
                                            'contentOptions' => [
                                                'noWrap' => true,
                                                // 'style' => 'width:160px;',
                                            ],
                                            'buttons' => [
                                                'delete' => function ($url, $model) use ($user) {
                                                    if ($user->mission_id == Mission::STAG) {
                                                        return Html::a('<span class="fa fa-trash"></span>', Url::to(['meeting-command/delete', 'id' => $model->command_id]), [
                                                            'title' => 'ลบ',
                                                            'class' => 'btn btn-danger deleteCommandItem',
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                },
                                                'assign' => function ($url, $model) use ($user) {
                                                    if ($user->mission_id == Mission::STAG) {
                                                        return Html::a('<span class="fa fa-chevron-circle-right"></span>', '#', [
                                                            'title' => 'มอบ/เพิ่มรายละเอียด',
                                                            'class' => 'btn btn-primary lovOpen',
                                                            'data-url' => Url::to(['meeting-command-items/create', 'id' => $model->command_id]),
                                                        ]);
                                                    } else {
                                                        return '';
                                                    }
                                                },
//                                                    'update' => function ($url, $model) {
//                                                        //if ($model->prj_status != '1') {
//                                                        return Html::a('<span class="fa fa-tasks"></span>', Url::to(['meeting-report/create', 'id' => $model->command_id]), [
//                                                            'title' => 'รายงานความก้าวหน้า',
//                                                            'class' => 'btn btn-warning',
//                                                            'data-pjax' => 0,
//                                                        ]);
//                                                        //} else {
//                                                        //    return '';
//                                                        //}
//                                                    },
                                                'view' => function ($url, $model) {

                                                    return Html::a('<span class="fa fa-eye"></span>', '#', [
                                                        'title' => 'ดูรายงาน',
                                                        'class' => 'btn btn-info',
                                                        'data-pjax' => 0,
                                                    ]);
                                                    //Url::to(['meeting-command/view', 'id' => $model->command_id])

                                                },
                                            ],
                                        ];
                                    }

                                    //if ($providerMeetingCommand->totalCount) {
                                        $gridColumnMeetingCommand = [
                                            /*[
                                                'class' => 'kartik\grid\ExpandRowColumn',
                                                'width' => '50px',
                                                'vAlign' => 'top',
                                                'value' => function ($model, $key, $index, $column) {
                                                    return GridView::ROW_COLLAPSED;
                                                },
                                                'detail' => function ($model, $key, $index, $column) {
                                                    return Yii::$app->controller->renderPartial('//meeting-command/_expand', ['model' => $model]);
                                                },
                                                'headerOptions' => ['class' => 'kartik-sheet-style'],
                                                'expandOneOnly' => false,
                                                'enableRowClick' => false,
                                                //'header' => 'แสดงทั้งหมด'
                                                //'collapseIcon' => ''
                                                'rowClickExcludedTags' => ['a', 'button'],
                                            ],*/
                                            [
                                                'class' => 'yii\grid\SerialColumn',
                                                'header' => 'ลำดับ',
                                                'headerOptions' => [
                                                    'style' => 'width:50px;',
                                                ],
                                            ],

                                            //'command_id',
                                            [
                                                'attribute' => 'command_name',
                                                'value' => function ($model) {
                                                    $txt1 .= $model->commander_name!='' ? "<br>(" . $model->commander_name . ")" : "";
                                                    $txt.= $model->command_name.$txt1;
                                                    $date = $model::dateEngDb($model->command_enddate);

                                                    $urgency = $model->commandUrgencyStyle();
                                                    $dep =''; //$this->context->getResName($model);

                                                    $table = Yii::$app->controller->renderPartial('//meeting-command/_expand', ['model' => $model]);

                                                    return $txt . "<br>กำหนดส่งภายใน " . $date . ' ' . $urgency.$dep."<hr>".$table;
                                                },
                                                'format' => 'raw',
                                                'width' => '85%',
                                            ],

//                                            [
//                                                'attribute' => 'รายละเอียด',
//                                                'value' => function ($model) {
                                                    /*$detail = $model->meetingCommandItems;
                                                    $li = [];
                                                    foreach ($detail as $item) {
                                                        $li[] = [
                                                            'id' => $item->item_id,
                                                            'detail' => $item->item_detail,
                                                            //'resp'=>$item->meetingResponsibles,
                                                        ];
                                                    }


                                                    //$items = ArrayHelper::getColumn($detail,'item_detail');
                                                    //$items = implode('',$items);

                                                    //print_r($li);

                                                    foreach ($li as $item) {
                                                        $items .= "" . $item['detail']."<br>";
                                                    }*/

                                                    //return $items;

//                                                    $table = Yii::$app->controller->renderPartial('//meeting-command/_expand', ['model' => $model]);
//                                                    return $table;
//                                                },
//                                                'format' => 'raw',
//                                            ],
                                            /*[
                                                'label' => 'กลุ่มงานที่เกี่ยวข้อง',
                                                'value' => function ($model) {
                                                    $dep = $this->context->getResName($model);
                                                    return $dep;
                                                },
                                                'format' => 'raw',
                                            ],*/

                                            //'command_name:ntext',
                                            //'commander_name',
                                            //'command_status',
                                            /*[
                                                'attribute' => 'command_urgency',
                                                'value' => function ($model) {
                                                    if($model->command_urgency==1){
                                                        $style = 'danger';
                                                    }elseif($model->command_urgency==2){
                                                        $style = 'warning';
                                                    }if($model->command_urgency==3){
                                                        $style = 'default';
                                                    }
                                                    return "<div class='badge badge-{$style}'>".$model::getUrgencyItem($model->command_urgency)."</div>";
                                                },
                                                'format'=>'raw'
                                            ],*/
                                            /*[
                                                'attribute' => 'command_enddate',
                                                'value'=>function($model){
                                                    return $model::dateEngDb($model->command_enddate);
                                                }
                                            ],*/
                                            /*[
                                                'attribute' => 'createdUser.username',
                                                'label' => 'Created User',
                                            ],
                                            [
                                                'attribute' => 'updatedUser.username',
                                                'label' => 'Updated User',
                                            ],*/
                                            [
                                                'class' => 'kartik\grid\ActionColumn',
                                                'template' => '<div  class="btn-group btn-group-sm text-center" role="group">{view} {assign} {delete}</div>', // approve
                                                //'template' => '{view} {update} {delete}', //{save-as-new}
                                                'header' => 'จัดการ',
                                                //'width' => '200px;',
                                                //                'headerOptions' => [
                                                //                        'style'=>'width:140px;',
                                                //                    'width'=>'140px'
                                                //                ],
                                                //'options' => ['style' => 'width:15%;'],
                                                'vAlign' => GridView::ALIGN_TOP,
                                                'buttonOptions' => ['class' => 'btn btn-default'],
                                                'viewOptions' => [
                                                    'class' => 'btn btn-primary',
                                                    //'data-toggle' => 'tooltip',
                                                ],
                                                'updateOptions' => [
                                                    'class' => 'updateUser btn btn-warning',
                                                    //'data-toggle' => 'tooltip',
                                                    'title' => 'แก้ไข',
                                                ],
                                                'deleteOptions' => [
                                                    'class' => 'btn btn-danger deleteCommandItem',
                                                    //'data-toggle' => 'tooltip',
                                                    //'data-confirm' => 'คุณต้องการลบข้อมูลอุปกรณ์นี้ ?',
                                                    //'data-pjax'=>1
                                                ],
                                                'noWrap' => true,
                                                'contentOptions' => [
                                                    'noWrap' => true,
                                                    // 'style' => 'width:160px;',
                                                ],
                                                'buttons' => [
                                                    'delete' => function ($url, $model) use ($user) {
                                                        if ($user->mission_id == Mission::STAG) {
                                                            return Html::a('<span class="fa fa-trash"></span>', 'javascript:(void)', [
                                                                'title' => 'ลบ',
                                                                'class' => 'btn btn-danger deleteCommand',
                                                                'data-url'=>Url::to(['meeting-command/delete', 'id' => $model->command_id])
                                                            ]);
                                                            //
                                                        } else {
                                                            return '';
                                                        }
                                                    },
                                                    'assign' => function ($url, $model) use ($user) {
                                                        if ($user->mission_id == Mission::STAG) {
                                                            return Html::a('<span class="fa fa-plus"></span>', '#', [
                                                                'title' => 'มอบภารกิจ/เพิ่มรายละเอียด',
                                                                'class' => 'btn btn-primary lovOpen',
                                                                'data-url' => Url::to(['meeting-command-items/create', 'id' => $model->command_id]),
                                                            ]);
                                                        } else {
                                                            return '';
                                                        }
                                                    },
//                                                    'update' => function ($url, $model) {
//                                                        //if ($model->prj_status != '1') {
//                                                        return Html::a('<span class="fa fa-tasks"></span>', Url::to(['meeting-report/create', 'id' => $model->command_id]), [
//                                                            'title' => 'รายงานความก้าวหน้า',
//                                                            'class' => 'btn btn-warning',
//                                                            'data-pjax' => 0,
//                                                        ]);
//                                                        //} else {
//                                                        //    return '';
//                                                        //}
//                                                    },
                                                    'view' => function ($url, $model) use ($user) {
                                                        if ($user->mission_id == Mission::STAG) {
                                                            return Html::a('<span class="fa fa-eye"></span>', 'javascript:(void);', [
                                                                'title' => 'ดูรายงาน',
                                                                'class' => 'btn btn-info',
                                                                'data-pjax' => 0,
                                                            ]);
                                                            //Url::to(['meeting-command/view', 'id' => $model->command_id])
                                                        }
                                                    },
                                                ],
                                            ]

                                        ];
                                        echo Gridview::widget([
                                            'dataProvider' => $providerMeetingCommand,
                                            'pjax' => true,
                                            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-command']],
                                            'panel' => [
                                                'type' => GridView::TYPE_PRIMARY,
                                                'heading' => '<span class="fa fa-tasks"></span> ' . Html::encode('ประเด็น/ข้อสั่งการ'),
                                                'before'=>'สีแสดงสถานะการดำเนินการตามประเด็น/ข้อสั่งการ : 
<span class="badge badge-danger mr-2 mb-2">ยังไม่ดำเนินการ</span>
<span class="badge badge-warning mr-2 mb-2">กำลังดำเนินการ</span>
<span class="badge badge-success mr-2 mb-2">เสร็จสิ้น</span>
<span class="badge badge-secondary text-danger mr-2 mb-2">แจ้งแก้ไข</span>
<span class="badge badge-dark text-line-through mr-2 mb-2">ยกเลิก</span>'
                                            ],
                                            'export' => false,
                                            'columns' => $gridColumnMeetingCommand,
                                            /*'rowOptions' => function ($model, $index, $widget, $grid) {
                                                if ($model->command_urgency == 1) {
                                                    return ['class' => ''.GridView::TYPE_DANGER];
                                                } elseif ($model->command_urgency == 2) {
                                                    return ['class' => 'warning'];
                                                } elseif ($model->command_urgency == 3) {
                                                    return ['class' => 'info'];
                                                }
                                            },*/

                                        ]);
                                    //}
                                    ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
