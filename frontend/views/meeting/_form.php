<?php

use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $form yii\widgets\ActiveForm */

/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MeetingCommand',
        'relID' => 'meeting-command',
        'value' => \yii\helpers\Json::encode($model->meetingCommands),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);*/
?>

<?php $form = \yii\bootstrap4\ActiveForm::begin([
//        'layout' => '',
    'id' => 'create-meeting',
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'enableAjaxValidation' => true,
    'validationUrl' => Url::to(['validate-create-meeting']),
    'options' => [
        'class' => 'kt-form kt-form--fit',
        'enctype' => 'multipart/form-data',
    ],
    // 'layout' => 'horizontal'
]);

//echo Html::activeHiddenInput($model, 'files_path');
//echo Html::activeHiddenInput($model, 'mission_id');

$model->meeting_status = 1;

?>
<div class="kt-portlet__body">
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'meeting_name')->textarea(['maxlength' => true, 'placeholder' => '']) ?>
    <div class="form-group row form-group-marginless">
        <div class="col-lg-3">
            <?= $form->field($model, 'meeting_no')->textInput(['maxlength' => true, 'placeholder' => '']) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'meeting_date')->widget(DatePicker::classname(), [
                //'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                //'saveFormat' => 'php:Y-m-d',
                //'ajaxConversion' => true,
                'options' => [
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'size' => 'xs',
                    'autoclose'=>true
                ]
            ]); ?>
        </div>
        <div class="col-lg-3">
            <? /*= $form->field($model, 'meeting_time')->widget(\kartik\datecontrol\DateControl::className(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_TIME,
                'saveFormat' => 'php:H:i',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => '',
                        'autoclose' => true,
                    ],
                ],
            ]); */ ?>

            <?= $form->field($model, 'meeting_time')->widget(TimePicker::className(), [
                'options' => [

                ],
                'pluginOptions' => [
                    'placeholder' => '',
                    'autoclose' => true,
                    'showMeridian' => false,
                    //'showInputs'=>false,
                ],
            ]); ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'meeting_status')->dropDownList($model::getMeetingStatus()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">ประเด็น/ข้อสั่งการ</div>

                <div class="panel-body">
                    <?php
                    /*$forms = [
                        [
                            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ประเด็น/ข้อสั่งการ'),
                            'content' => $this->render('_formMeetingCommand', [
                                'model' => $model,
                                'form'=>$form
                            ]),
                        ],
                        [
                            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ประเด็น/ข้อสั่งการ'),
                            'content' => $this->render('_formMeetingCommand', [
                                'row' => \yii\helpers\ArrayHelper::toArray($model->meetingCommands),
                            ]),
                        ],
                    ];*/
                    /*echo kartik\tabs\TabsX::widget([
                        'items' => $forms,
                        'position' => kartik\tabs\TabsX::POS_ABOVE,
                        'encodeLabels' => false,
                        'pluginOptions' => [
                            'bordered' => true,
                            'sideways' => true,
                            'enableCache' => false,
                        ],
                    ]);*/


                    echo $this->render('_formMeetingCommand', [
                        'model' => $model,
                        'form' => $form,
                    ]);
                    ?>

                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'บันทึกข้อมูล' : 'แก้ไขข้อมูล', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
