<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

$this->title = 'แก้ไขข้อมูล: ' . ' ' . $model->meeting_name;
$this->params['breadcrumbs'][] = ['label' => 'หัวข้อการประชุมทั้งหมด', 'url' => ['index'], 'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'class' => 'kt-subheader__breadcrumbs-link'];
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                </div>

                <?= $this->render('_formUpdate', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>
