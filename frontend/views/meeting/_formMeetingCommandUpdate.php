<div class="form-group" id="add-meeting-command">
    <?php

    /*use kartik\builder\TabularForm;
    use kartik\grid\GridView;
    use yii\data\ArrayDataProvider;
    use yii\helpers\Html;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $row,
        'pagination' => [
            'pageSize' => -1,
        ],
    ]);
    echo TabularForm::widget([
        'dataProvider' => $dataProvider,
        'formName' => 'MeetingCommand',
        'checkboxColumn' => false,
        'actionColumn' => false,
        'attributeDefaults' => [
            'type' => TabularForm::INPUT_TEXT,
        ],
        'attributes' => [
//        'command_id' => ['type' => TabularForm::INPUT_HIDDEN],
            'command_name' => ['type' => TabularForm::INPUT_TEXTAREA, 'label' => 'ประเด็น/ข้อสั่งการ'],
            'commander_name' => ['type' => TabularForm::INPUT_TEXT, 'label' => 'ประเด็น/ข้อสั่งการ'],
            'command_status' => ['type' => TabularForm::INPUT_TEXT, 'label' => 'ประเด็น/ข้อสั่งการ'],
            'command_urgency' => ['type' => TabularForm::INPUT_TEXT, 'label' => 'ประเด็น/ข้อสั่งการ'],
            'command_enddate' => ['type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\datecontrol\DateControl::classname(),
                'options' => [
                    'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                    'saveFormat' => 'php:Y-m-d',
                    'ajaxConversion' => true,
                    'options' => [
                        'pluginOptions' => [
                            'placeholder' => 'กำหนดส่ง',
                            'autoclose' => true,
                        ],
                    ],
                ],
            ],
            'created_user' => [
                'label' => 'User',
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('username')->asArray()->all(), 'id', 'username'),
                    'options' => ['placeholder' => 'Choose User'],
                ],
                'columnOptions' => ['width' => '200px']
            ],
            'updated_user' => [
                'label' => 'User',
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Select2::className(),
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('username')->asArray()->all(), 'id', 'username'),
                    'options' => ['placeholder' => 'Choose User'],
                ],
                'columnOptions' => ['width' => '200px']
            ],
            'del' => [
                'type' => 'raw',
                'label' => 'จัดการ',
                'value' => function ($model, $key) {
                    return
                        Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                        Html::a('<i class="fa fa-trash"></i>', '#', ['class' => 'btn btn-sm btn-danger', 'title' => 'ลบ', 'onClick' => 'delRowMeetingCommand(' . $key . '); return false;', 'id' => 'meeting-command-del-btn']);
                },
            ],
        ],
        'gridSettings' => [
            'panel' => [
                'heading' => false,
                'type' => GridView::TYPE_DEFAULT,
                'before' => false,
                'footer' => false,
                'after' => Html::button('<i class="fa fa-plus-circle"></i>' , ['type' => 'button', 'class' => 'btn btn-primary kv-batch-create', 'onClick' => 'addRowMeetingCommand()']),
            ],
        ],
    ]);
    echo "    </div>\n\n";*/


    use common\models\BaseActiveRecord;
    use common\models\Meeting;
    use unclead\multipleinput\MultipleInput;
    use yii\helpers\ArrayHelper;

    //    $newMap = ArrayHelper::toArray($model->_meeting_command, [
    //            //'common\models\MeetingCommand'=>[
    //                    'command_id',
    //                    'command_name',
    //                    'commander_name',
    //                    'command_status',
    //                    'command_urgency',
    //                    'command_enddate',
    //                    'meeting_id',
    //            //]
    //    ]);
    //
    //    $newMap = ArrayHelper::index($newMap,'command_id');

    $newMap = ArrayHelper::index($model->_meeting_command, 'command_id');
    //var_dump($newMap);

    echo $form->field($model, '_meeting_command')->widget(MultipleInput::className(), [
        //'max' => 30,
        'cloneButton' => false,
        //'enableError'=>false,
        'iconMap' => [
            MultipleInput::ICONS_SOURCE_FONTAWESOME => [
                'drag-handle' => 'fa fa-bars',
                'remove' => 'fa fa-times',
                'add' => 'fa fa-plus',
                'clone' => 'fa fa-file',
            ],
        ],
        'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
        'min' => 1, // should be at least 2 rows
        'sortable' => true,
        'allowEmptyList' => false,
        'enableGuessTitle' => true,
        //'id' => 'dtbg_',
//        'data' =>
//            $newMap
//        ,
        'columns' => [
            [
                'name' => 'command_id',
                'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
                //'enableError'=>false
            ],
            [
                'name' => 'command_name',
                'type' => 'textarea',
                'enableError' => true,
                'title' => 'ประเด็น/ข้อสั่งการ',
                'options' => [
                    'class' => 'form-control',
                ],
            ],
            [
                'name' => 'commander_name',
                'title' => 'ผู้สั่งการ',
                'enableError' => false,
                'options' => [
                    'class' => 'form-control',
                    //'style' => 'width:50%',
                ],
            ],
            [
                'name' => 'command_urgency',
                'type' => 'radioList',
                'title' => 'ความเร่งด่วน',
                'enableError' => true,
                //'defaultValue' => 1,
                'items' => Meeting::getUrgency(),
                'options' => [
                    //'type' => 'number',
                    //'class' => 'form-check-inline',
                    //'style'=>'padding-right:5px;'
                ],
            ],
            [
                'name' => 'command_enddate',
                'type' => \kartik\widgets\DatePicker::className(),
                'title' => 'กำหนดส่ง',
                'enableError' => true,
                //'defaultValue' => 1,
                //'items' => $budgetSourceList,
                'value'=>function($model){
                    return BaseActiveRecord::dateEngDb($model->command_enddate);
                },
                'options' => [
                    //'type' => 'number',
                    'class' => 'form-control',
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true,
                        'size' => 'xs',
                        'autoclose' => true,
                    ],
                    'options'=>[
                        'autocomplete' => "off",
                    ]
                ],
            ],
        ],

        'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
        /*'rowOptions' => function ($model) {
            $options = [];

            if ($model['meeting_status'] >= 1) {
                $options['class'] = 'danger';
            }
            return $options;
        },*/
        'addButtonPosition' => MultipleInput::POS_FOOTER, // show add button inside of the row
        'layoutConfig' => [
            'offsetClass' => 'col-md-offset-2',
            'labelClass' => 'col-md-2',
            'wrapperClass' => 'col-md-10',
            'errorClass' => 'col-md-offset-2 col-md-10',
            //'buttonActionClass' => 'col-md-offset-1 col-md-2',
        ],
    ])

    ?>

