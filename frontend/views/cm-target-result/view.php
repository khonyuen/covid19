<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTargetResult */

$this->title = $model->tg_id;
$this->params['breadcrumbs'][] = ['label' => 'Cm Target Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cm-target-result-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tg_id',
            'tg_date',
            'tg_amphur',
            'tg_num',
            'tg_rs',
            'tg_l1',
            'tg_l2',
            'tg_l3',
            'tg_l4',
            'tg_l5',
            'created_at',
            'created_user',
        ],
    ]) ?>

</div>
