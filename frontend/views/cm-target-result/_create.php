<?php
/* @var $targetRsModels frontend\models\CmTargetResult[] */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$form = \yii\bootstrap4\ActiveForm::begin([
//        'layout' => '',
    'id' => 'create-cm-target',
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'enableAjaxValidation' => true,
    //'layout' => 'horizontal',
    'validationUrl' => Url::to(['validate-create-target']),
    'options' => [
        'class' => 'kt-form kt-form--fit kt-form--label-right',
        //'enctype' => 'multipart/form-data',
    ],
]);


?>

<table id="cmtargettable" class="table table-hover table-bordered table-striped">
    <thead>
    <tr>
        <th style="white-space: nowrap; min-width: 250px;">กลุ่มเป้าหมาย</th>
        <th style="white-space: nowrap; min-width: 70px;">เป้าหมาย</th>
        <th style="white-space: nowrap; min-width: 70px;">ได้รับการประเมิน</th>
        <th style="white-space: nowrap; min-width: 70px;">เครียดมาก</th>
        <th style="white-space: nowrap; min-width: 70px;">มีแนวโน้มซึมเศร้า</th>
        <th style="white-space: nowrap; min-width: 70px;">ซึมเศร้า</th>
        <th style="white-space: nowrap; min-width: 70px;">เสี่ยงฆ่าตัวตาย</th>
        <th style="white-space: nowrap; min-width: 70px;">ติดตาม</th>
    </tr>
    </thead>
    <tbody>
    <?php
    echo Html::hiddenInput('flag','new',['id'=>'flag']);
    echo Html::hiddenInput('CmTargetResult[tg_date]',$d,['id'=>'CmTargetResult[tg_date]']);
    echo Html::hiddenInput('CmTargetResult[tg_amphur]',$a,['id'=>'CmTargetResult[tg_amphur]']);

    foreach ($targetRsModels as $index => $targetRsModel) {
        ?>
        <?= Html::activeHiddenInput($targetRsModel, "[$index]tg_target_id") ?>
        <?= Html::activeHiddenInput($targetRsModel, "[$index]tg_id") ?>
        <tr>
            <td width="30%;"><?= $form->field($targetRsModel, "[$index]_tggroupname")->textInput(['disabled' => true])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_num")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_rs")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_l1")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_l2")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_l3")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_l4")->textInput(['type' => 'number'])->label(false) ?></td>
            <td><?= $form->field($targetRsModel, "[$index]tg_l5")->textInput(['type' => 'number'])->label(false) ?></td>
        </tr>

        <?php echo Html::activeHiddenInput($targetRsModel, "[$index]created_user"); ?>
        <?php
    }
    ?>
    </tbody>
</table>

<div class="form-group">
    <div class="col-md-10 offset-2">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
        <?= Html::resetButton('ยกเลิก', ['class' => 'btn btn-danger']) ?>
    </div>
</div>
<?php
ActiveForm::end();
?>