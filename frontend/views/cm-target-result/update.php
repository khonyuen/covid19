<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTargetResult */

$this->title = 'Update Cm Target Result: ' . $model->tg_id;
$this->params['breadcrumbs'][] = ['label' => 'Cm Target Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tg_id, 'url' => ['view', 'id' => $model->tg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cm-target-result-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
