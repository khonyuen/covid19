<?php

use common\models\BaseActiveRecord;
use frontend\models\Amphur;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CmTargetResultSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'คัดกรองเยียวยาด้านจิตใจ';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];
$user = Yii::$app->user->identity;
?>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-information kt-font-brand"></i></div>
                <div class="alert-text">
                    แสดงข้อมูลการปฏิบัติงานคัดกรองเยียวยาด้านจิตใจ จังหวัดมุกดาหาร
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::begin([
        'id' => 'search-tg-pjax',
        'timeout' => 10000,
    ]); ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="text-success fa fa-grin "></i> <i class="text-danger fa fa-grin-beam "></i> <i
                                    class="text-primary fa fa-grin-squint "></i> <?= Html::encode($this->title) ?>
                        </h3>

                    </div>
                    <?php
                    $user = Yii::$app->session->get('user_session');
                    if ($user['mission_id'] == BaseActiveRecord::CM) {
                        ?>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <?= Html::a('บันทึกผลคัดกรอง', ['create'], ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">
                        </span>
                        <div class="kt-section__content">
                            <div class="cm-target-result-index">
                                <?php echo $this->render('_search', ['model' => $searchModel, 'amp' => $amp]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">
                            <?php
                            echo "<div class='well-sm h3'>ผลการคัดกรองเยียวยาด้านจิตใจ";
                            ?>
                            <?php
                            //echo $searchModel->tg_amphur;
                            $am = in_array($searchModel->tg_amphur, ['49', '']) ? "ระดับจังหวัด" : $searchModel->tgAmphur->ampurname;
                            $date = $searchModel->tg_date == "" ? "ทั้งหมด" : $searchModel->tg_date;
                            echo "<p class='h4'>ระดับ : " . $am . " ช่วงวันที่ : " . $date . "</p>" . "</div>";
                            ?>
                        </span>
                        <div class="kt-section__content">
                            <div class="cm-target-result-index">
                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout' => '{items}',
                                    //'filterModel' => $searchModel,
                                    //'pager' => ['class' => LinkPager::className()],

                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        //'targetgroup_id',
                                        [
                                            'attribute' => 'targetnames',
                                            'value' => function ($model) {
                                                return $model->targetnames;
                                            },
                                        ],
                                        'tg_num',
                                        'tg_rs',
                                        'tg_l1',
                                        'tg_l2',
                                        'tg_l3',
                                        'tg_l4',
                                        'tg_l5',
                                        //['class' => 'yii\grid\ActionColumn'],
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        if ($searchModel->tg_amphur == '49' || $searchModel->tg_amphur == '') :
            $amphurList = Amphur::getMukAmphurList();
            foreach ($amphurList as $index => $list) :
                ?>
                <?php
                $code = $index;
                $name = $list;

                $params = [
                ];
                ?>
                <div class="col-xl-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body">
                            <!--begin::Section-->
                            <div class="kt-section">
                        <span class="kt-section__info">
                            <?php
                            $searchModel->tg_amphur = $code;
                            $date = $searchModel->tg_date == "" ? "ทั้งหมด" : $searchModel->tg_date;
                            echo "<p class='h3'>".$searchModel->tgAmphur->ampurname." ช่วงวันที่ ". $date . "</p>";
                            ?>
                        </span>
                                <div class="kt-section__content">
                                    <div class="cm-target-result-index">

                                        <?= GridView::widget([
                                            'id' => 'grid-amphur-' . $code,
                                            'layout' => '{items}',
                                            'dataProvider' => $searchModel->searchReportPv($params),
                                            //'filterModel' => $searchModel,
                                            //'pager' => ['class' => LinkPager::className()],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],

                                                //'targetgroup_id',
                                                [
                                                    'attribute' => 'targetnames',
                                                    'value' => function ($model) {
                                                        return $model->targetnames;
                                                    },
                                                ],
                                                'tg_num',
                                                'tg_rs',
                                                'tg_l1',
                                                'tg_l2',
                                                'tg_l3',
                                                'tg_l4',
                                                'tg_l5',
                                                //['class' => 'yii\grid\ActionColumn'],
                                            ],
                                        ]); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        <?php
        endif;
        ?>
    </div>
    <?php Pjax::end(); ?>
</div>
