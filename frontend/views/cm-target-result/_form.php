<?php

use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTargetResult */
/* @var $amphurModel frontend\models\Amphur[] */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $targetRsModels frontend\models\CmTargetResult[] */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,]);

?>


<div class="kt-portlet__body">

    <?php $form = \yii\bootstrap4\ActiveForm::begin([
//        'layout' => '',
        'id' => 'create-cm-target-search',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        //'layout' => 'horizontal',
        'validationUrl' => Url::to(['validate-create-target']),
        'options' => [
            'class' => 'kt-form kt-form--fit kt-form--label-right',
            //'enctype' => 'multipart/form-data',
        ],
    ]);

    //var_dump($targetRsModels);
    ?>
    <div class="form-group row form-group-marginless kt-margin-t-20">
        <div class="col-lg-4">
            <?= $form->field($model, 'tg_date')->widget(
                DatePicker::className(), [
                'language' => 'th',
                'options' => ['placeholder' => '','autocomplete'=>'off'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'autoclose' => true,

                ],
            ]); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'tg_amphur')->dropDownList($amphurModel) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="form-group row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <div id="data">

                </div>
            </div>
        </div>
    </div>
</div>