<?php

use yii\helpers\Url; ?>
<script>
    var body = $(document);
    var submitForm = function () {
        body.on("beforeSubmit", "form#create-cm-target", function (e) {
            e.preventDefault();
            e.isImmediatePropagationStopped();

            //var form = document.getElementById('baseForm');
            var formData = new FormData($(this)[0]);
            //var flag = $("#flag").val();


            swal.fire({
                title: 'ยืนยันการบันทึกข้อมูล',
                text: "ตรวจสอบความถูกต้องของข้อมูล!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ยืนยันการบันทึก'
            }).then(function (result) {
                if (result.value) {
                    KTApp.blockPage();
                    $.ajax({
                        cache: false,
                        contentType: false,
                        processData: false,
                        url: $(this).attr("action"),
                        type: "post",
                        data: formData,//form.serialize(),
                        success: function (response) {
                            KTApp.unblockPage();
                            // do something with response
                            console.log(response);
                            //response = JSON.parse(response);
                            swal.fire(
                                'ผลการบันทึก!',
                                response.message,
                                (response.status == true) ? "success" : "warning"
                            );
                            if (response.status == true) {
                                //$("form#create-cm-target")[0].reset();
                            }
                        },
                        complete: function () {
                            KTApp.unblockPage();
                        },
                        error: function (error) {
                            KTApp.unblockPage();
                        }
                    });
                }
            });
            return false;
        });
    };

    var checkUpdate = function () {
        body.on("change", "#cmtargetresult-tg_date", function (e) {
            e.preventDefault();
            e.isImmediatePropagationStopped();

            var d = $("#cmtargetresult-tg_date").val();
            var amp = $("#cmtargetresult-tg_amphur").val();
            console.log(d, amp);
            if(amp!=''){
                //swal.fire('แจ้งเตือน','load data','warning')
                loadData(d,amp);
            }
        });

        body.on("change", "#cmtargetresult-tg_amphur", function (e) {
            e.preventDefault();
            e.isImmediatePropagationStopped();

            var d = $("#cmtargetresult-tg_date").val();
            var amp = $(this).val();
            console.log(d, amp);
            if(d==''){
                swal.fire('แจ้งเตือน','กรุณาเลือกวันที่','warning')
            }else{
                //swal.fire('แจ้งเตือน','load data','success')
                loadData(d,amp);
            }
        });
    };

    function loadData(d,a){
        KTApp.blockPage();
        $.ajax({
            url: "<?=Url::to(['cm-target-result/check-update','d'=>''])?>"+d+"&a="+a,
            type: "get",
            success: function (response) {
                KTApp.unblockPage();
                if(response.status==true){
                    swal.fire("แจ้งเตือน",'พบข้อมูลเดิมที่เคยบันทึกแล้ว หากกดบันทึกจะทำการแก้ไขข้อมูลเก่าที่เคยรายงาน','warning');
                    $("#data").html(response.message);
                }else{
                    $("#data").html(response.message);
                }
            },
            complete: function () {
                KTApp.unblockPage();
            },
            error: function (error) {
                KTApp.unblockPage();
            }

        });
    };

    $(function () {
        submitForm();
        checkUpdate();
    });
</script>
