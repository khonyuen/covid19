<?php

use kartik\date\DatePicker;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTargetResultSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="cm-target-result-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        //'layout' => 'horizontal',
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <?= $form->field($model, 'tg_amphur')->dropDownList($amp) ?>
        </div>
        <div class="col-lg-4 col-md-6">
            <?= $form->field($model, 'tg_date',[
                //'addon'=>['prepend'=>['content'=>'<i class="fas fa-calendar-alt"></i>']],
                'options'=>['class'=>'drp-container form-group',]
            ])->widget(
                DateRangePicker::classname(), [
                'pluginOptions'=>[
                    'locale'=>[
                        'separator'=>' ถึง ',
                    ],
                    'opens'=>'left'
                ],
                'options'=>[
                    'autocomplete'=>'off'
                ]
                //'useWithAddon'=>true
            ])->label('ช่วงวันที่คัดกรอง'); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('ดูรายงาน', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ยกเลิก',Url::to(['index']), ['class' => 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
