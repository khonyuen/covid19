<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CmTargetResult */
/* @var $amphurModel frontend\models\Amphur[] */
/* @var $targetRsModels frontend\models\CmTargetResult[] */

$this->title = 'รายงานผลการคัดกรอง';
$this->params['breadcrumbs'][] = ['label' => 'คัดกรองเยียวยาด้านจิตใจ', 'url' => ['index'],'class' => 'kt-subheader__breadcrumbs-link'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' =>'#' ,'class' => 'kt-subheader__breadcrumbs-link'];
?>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->

                <?= $this->render('_form', [
                    'model' => $model,
                    'targetRsModels'=>$targetRsModels,
                    'amphurModel'=>$amphurModel
                ]) ?>

                <!--end::Form-->
            </div>
        </div>

    </div>
</div>
