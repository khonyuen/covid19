<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-covid-situations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'covid_id')->textInput(['placeholder' => 'Covid']) ?>

    <?= $form->field($model, 'covid_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Covid Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'pui_tot')->textInput(['placeholder' => 'Pui Tot']) ?>

    <?= $form->field($model, 'pui_active')->textInput(['placeholder' => 'Pui Active']) ?>

    <?= $form->field($model, 'high_risk')->textInput(['placeholder' => 'High Risk']) ?>

    <?php /* echo $form->field($model, 'detail')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'no_infection')->textInput(['placeholder' => 'No Infection']) */ ?>

    <?php /* echo $form->field($model, 'infection_tot')->textInput(['placeholder' => 'Infection Tot']) */ ?>

    <?php /* echo $form->field($model, 'wait_result')->textInput(['placeholder' => 'Wait Result']) */ ?>

    <?php /* echo $form->field($model, 'last_infection_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Last Infection Date',
                'autoclose' => true
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'rs_in_hosp')->textInput(['placeholder' => 'Rs In Hosp']) */ ?>

    <?php /* echo $form->field($model, 'rs_cure')->textInput(['placeholder' => 'Rs Cure']) */ ?>

    <?php /* echo $form->field($model, 'risk_country')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'risk_country_tot')->textInput(['placeholder' => 'Risk Country Tot']) */ ?>

    <?php /* echo $form->field($model, 'risk_country_due')->textInput(['placeholder' => 'Risk Country Due']) */ ?>

    <?php /* echo $form->field($model, 'risk_country_remain')->textInput(['placeholder' => 'Risk Country Remain']) */ ?>

    <?php /* echo $form->field($model, 'epidemic_country_due')->textInput(['placeholder' => 'Epidemic Country Due']) */ ?>

    <?php /* echo $form->field($model, 'epidemic_country_remain')->textInput(['placeholder' => 'Epidemic Country Remain']) */ ?>

    <?php /* echo $form->field($model, 'epidemic_country_hq')->textInput(['placeholder' => 'Epidemic Country Hq']) */ ?>

    <?php /* echo $form->field($model, 'epidemic_country_lq')->textInput(['placeholder' => 'Epidemic Country Lq']) */ ?>

    <?php /* echo $form->field($model, 'from_bangkok_other_tot')->textInput(['placeholder' => 'From Bangkok Other Tot']) */ ?>

    <?php /* echo $form->field($model, 'from_bangkok_other_due')->textInput(['placeholder' => 'From Bangkok Other Due']) */ ?>

    <?php /* echo $form->field($model, 'from_bangkok_other_remain')->textInput(['placeholder' => 'From Bangkok Other Remain']) */ ?>

    <?php /* echo $form->field($model, 'from_bangkok_other_lq15')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'from_bangkok_other_hq')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'labor_tot')->textInput(['placeholder' => 'Labor Tot']) */ ?>

    <?php /* echo $form->field($model, 'labor_due')->textInput(['placeholder' => 'Labor Due']) */ ?>

    <?php /* echo $form->field($model, 'labor_remain')->textInput(['placeholder' => 'Labor Remain']) */ ?>

    <?php /* echo $form->field($model, 'labor_lao')->textInput(['placeholder' => 'Labor Lao']) */ ?>

    <?php /* echo $form->field($model, 'lq_all')->textInput(['placeholder' => 'Lq All']) */ ?>

    <?php /* echo $form->field($model, 'hq_all')->textInput(['placeholder' => 'Hq All']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
