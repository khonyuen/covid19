<?php

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CovidSituationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\BaseActiveRecord;
use frontend\models\Amphur;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'สถานการณ์ COVID-19';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];
$user = Yii::$app->user->identity;
?>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-information kt-font-brand"></i></div>
                <div class="alert-text">
                    แสดงข้อมูลการปฏิบัติงานคัดกรองเยียวยาด้านจิตใจ จังหวัดมุกดาหาร
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="text-success fa fa-grin "></i> <i class="text-danger fa fa-grin-beam "></i> <i
                                    class="text-primary fa fa-grin-squint "></i> <?= Html::encode($this->title) ?>
                        </h3>

                    </div>
                    <?php
                    $user = Yii::$app->session->get('user_session');
                    if ($user['mission_id'] == BaseActiveRecord::CM) {
                        ?>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <?= Html::a('บันทึกผลคัดกรอง', ['create'], ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">
                        </span>
                        <div class="kt-section__content">
                            <div class="cm-target-result-index">
                                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">

                        </span>
                        <div class="kt-section__content">
                            <?php
                            $gridColumn = [
                                ['class' => 'yii\grid\SerialColumn'],
                                'covid_id',
                                'covid_date',
                                'pui_tot',
                                'pui_active',
                                'high_risk',
                                'detail:ntext',
                                'no_infection',
                                'infection_tot',
                                'wait_result',
                                'last_infection_date',
                                'rs_in_hosp',
                                'rs_cure',
                                'risk_country:ntext',
                                'risk_country_tot',
                                'risk_country_due',
                                'risk_country_remain',
                                'epidemic_country_due',
                                'epidemic_country_remain',
                                'epidemic_country_hq',
                                'epidemic_country_lq',
                                'from_bangkok_other_tot',
                                'from_bangkok_other_due',
                                'from_bangkok_other_remain',
                                'from_bangkok_other_lq15:ntext',
                                'from_bangkok_other_hq:ntext',
                                'labor_tot',
                                'labor_due',
                                'labor_remain',
                                'labor_lao',
                                'lq_all',
                                'hq_all',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{save-as-new} {view} {update} {delete}',
                                    'buttons' => [
                                        'save-as-new' => function ($url) {
                                            return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                                        },
                                    ],
                                ],
                            ];
                            ?>
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => $gridColumn,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-covid-situations']],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                                ],
                                // your toolbar can include the additional full export menu
                                'toolbar' => [
                                    '{export}',
                                    ExportMenu::widget([
                                        'dataProvider' => $dataProvider,
                                        'columns' => $gridColumn,
                                        'target' => ExportMenu::TARGET_BLANK,
                                        'fontAwesome' => true,
                                        'dropdownOptions' => [
                                            'label' => 'Full',
                                            'class' => 'btn btn-default',
                                            'itemsBefore' => [
                                                '<li class="dropdown-header">Export All Data</li>',
                                            ],
                                        ],
                                    ]) ,
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
