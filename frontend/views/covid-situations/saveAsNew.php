<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituations */

$this->title = 'Save As New Covid Situations: '. ' ' . $model->covid_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Situations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->covid_id, 'url' => ['view', 'id' => $model->covid_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="covid-situations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
