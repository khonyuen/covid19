<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituations */

$this->title = 'Update Covid Situations: ' . ' ' . $model->covid_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Situations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->covid_id, 'url' => ['view', 'id' => $model->covid_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="covid-situations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
