<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituations */

$this->title = 'Create Covid Situations';
$this->params['breadcrumbs'][] = ['label' => 'Covid Situations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-situations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
