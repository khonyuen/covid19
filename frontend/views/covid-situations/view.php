<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituations */

$this->title = $model->covid_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Situations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-situations-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Covid Situations'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->covid_id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->covid_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->covid_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->covid_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'covid_id',
        'covid_date',
        'pui_tot',
        'pui_active',
        'high_risk',
        'detail:ntext',
        'no_infection',
        'infection_tot',
        'wait_result',
        'last_infection_date',
        'rs_in_hosp',
        'rs_cure',
        'risk_country:ntext',
        'risk_country_tot',
        'risk_country_due',
        'risk_country_remain',
        'epidemic_country_due',
        'epidemic_country_remain',
        'epidemic_country_hq',
        'epidemic_country_lq',
        'from_bangkok_other_tot',
        'from_bangkok_other_due',
        'from_bangkok_other_remain',
        'from_bangkok_other_lq15:ntext',
        'from_bangkok_other_hq:ntext',
        'labor_tot',
        'labor_due',
        'labor_remain',
        'labor_lao',
        'lq_all',
        'hq_all',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
