<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidSituations */

$this->title = $model->covid_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Situations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-situations-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Covid Situations'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'covid_id',
        'covid_date',
        'pui_tot',
        'pui_active',
        'high_risk',
        'detail:ntext',
        'no_infection',
        'infection_tot',
        'wait_result',
        'last_infection_date',
        'rs_in_hosp',
        'rs_cure',
        'risk_country:ntext',
        'risk_country_tot',
        'risk_country_due',
        'risk_country_remain',
        'epidemic_country_due',
        'epidemic_country_remain',
        'epidemic_country_hq',
        'epidemic_country_lq',
        'from_bangkok_other_tot',
        'from_bangkok_other_due',
        'from_bangkok_other_remain',
        'from_bangkok_other_lq15:ntext',
        'from_bangkok_other_hq:ntext',
        'labor_tot',
        'labor_due',
        'labor_remain',
        'labor_lao',
        'lq_all',
        'hq_all',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
