<?php
/**
 * @var $data array
 * @var $color array
 */

use common\models\Mission;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

//print_r($data);
//$data_filter = ArrayHelper::remove($data,'bg');
//$data_filter = Json::encode($data_filter);
//print_r($data_filter);
$cl = ArrayHelper::getColumn($data,'bg');
//var_dump($cl);
$string = [];
foreach ($cl as $item) {
    $string[] = "KTApp.getStateColor('{$item}')";
}

?>

<script>
    "use strict";

    // Class definition
    var KTDashboard = function () {
        // Support Tickets Chart.
        // Based on Morris plugin - http://morrisjs.github.io/morris.js/
        var supportCases = function () {
            if ($('#kt_chart_support_tickets').length == 0) {
                return;
            }

            Morris.Donut({
                element: 'kt_chart_support_tickets',
                data: <?=Json::encode($data)?>,
                resize: true,
                labelColor: '#a7a7c2',
                colors: [
                    <?=implode(',',$string)?>
                ]
                //<?//=Json::encode($cl)?>
                //formatter: function (x) { return x + "%"}
            });
        }

        // Support Tickets Chart.
        // Based on Chartjs plugin - http://www.chartjs.org/
        var supportRequests = function () {
            var container = KTUtil.getByID('kt_chart_support_requests');

            if (!container) {
                return;
            }

            var randomScalingFactor = function () {
                return Math.round(Math.random() * 100);
            };

            var config = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [
                            35, 30, 35
                        ],
                        backgroundColor: [
                            KTApp.getStateColor('success'),
                            KTApp.getStateColor('danger'),
                            KTApp.getStateColor('brand')
                        ]
                    }],
                    labels: [
                        'Angular',
                        'CSS',
                        'HTML'
                    ]
                },
                options: {
                    cutoutPercentage: 75,
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false,
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Technology'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    },
                    tooltips: {
                        enabled: true,
                        intersect: false,
                        mode: 'nearest',
                        bodySpacing: 5,
                        yPadding: 10,
                        xPadding: 10,
                        caretPadding: 0,
                        displayColors: false,
                        backgroundColor: KTApp.getStateColor('brand'),
                        titleFontColor: '#ffffff',
                        cornerRadius: 4,
                        footerSpacing: 0,
                        titleSpacing: 0
                    }
                }
            };

            var ctx = container.getContext('2d');
            var myDoughnut = new Chart(ctx, config);
        }

        return {
            // Init demos
            init: function () {
                // init charts
                supportCases();
                //supportRequests();

                // demo loading
                var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
                loading.show();

                setTimeout(function () {
                    loading.hide();
                }, 3000);
            }
        };
    }();

    // Class initialization on page load
    jQuery(document).ready(function () {
        KTDashboard.init();
    });
</script>
