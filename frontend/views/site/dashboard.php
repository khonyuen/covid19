<?php

/* @var $this yii\web\View */
/* @var $missionProvider Mission */
/* @var $dataProvider ActiveDataProvider */

/* @var $searchModel DocumentSearch */

use common\models\Document;
use common\models\Mission;
use frontend\models\DocumentSearch;
use yii\bootstrap4\LinkPager;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


$this->title = 'เอกสารเผยแพร่ COVID-19';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');
$dataProvider->pagination = false;
/*$this->params['breadcrumbs'][] = [
    'label' => 'เอกสารทั้งหมด',
    #'url'=>'',
    'class' => 'kt-subheader__breadcrumbs-link',
];*/

//print_r($dataProvider);
$missions = ArrayHelper::getColumn($missionProvider, 'mission_id');

//print_r($missions);

$count = count($missions) / 2;
$mission_sub = (array_chunk($missions, $count));

//var_dump($mission_sub);
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <?php

            //$docType =  DocumentType::getList();
            //        foreach ($missionProvider as $mission) {
            //            $name = $mission->mission_name;
            //$modelIndex = ArrayHelper::index($dataProvider->getModels(),'document_id','mission_id');
            //var_dump($modelIndex);
            foreach ($mission_sub[0] as $item) {
                // filter model each of mission group
                //$docsModel = ArrayHelper::filter($modelIndex,[$item]);
                $dtPv = $searchModel->searchDocMission($item);
                $mis = ArrayHelper::index($missionProvider, 'mission_id');
                ?>

                <!--begin:: Widgets/Download Files-->
                <div class="kt-portlet kt-portlet--bordered">
                    <div class="kt-portlet__head kt-bg-light-success ">
                        <div class="kt-portlet__head-label">
                            <div class="kt-media">
                                <!--<div class="kt-media kt-media--sm kt-media--circle">-->
                                <img src="<?= $dir ?>/media/users/<?= $mis[$item]['mission_pic'] ?>" alt="" style="margin: 5px;">
                            </div>
                            <h2 class="kt-portlet__head-title">
                                <?= mb_strtoupper($mis[$item]['mission_name']) ?>
                            </h2>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                ตัวเลือก
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">

                                <!--begin::Nav-->
                                <ul class="kt-nav">
                                    <li class="kt-nav__head">
                                        รายการเอกสาร
                                    </li>
                                    <li class="kt-nav__separator"></li>
                                    <li class="kt-nav__item">
                                        <a href="<?= Url::to(['site/index-sub', 'id' => $item]) ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-document"></i>
                                            <span class="kt-nav__link-text">ดูทั้งหมด</span>
                                        </a>
                                    </li>
                                </ul>

                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin::k-widget4-->
                        <div class="kt-widget4">
                            <div class="table-responsive">
                                <?php Pjax::begin([
                                    'id' => 'document-pjax-container-' . $item,
                                ]); ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                                <?php
                                //$searchModel->mission_id = $item;
                                ?>
                                <?= GridView::widget([
                                    'dataProvider' => $dtPv,
                                    'id' => 'document-pjax-container' . $item,
                                    //'filterModel' => $searchModel,
                                    'tableOptions' => [
                                        'class' => 'table table-hover table-striped',
                                    ],
//                                    'layout' => '{items}{pager}',
                                    'pager' => ['class'=>LinkPager::className()],
                                    'columns' => [
//                                    ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'documentTypeIcon',

                                            'header' => false,
                                            'format' => 'raw',
                                            'headerOptions' => ['class' => 'width:100px;'],
                                            'value' => function ($model) use ($dir) {
                                                $url = $model->getFileLink();
                                                if ($model->documentTypeIcon != '') {
                                                    if ($model->documentTypeIcon == Document::pdf) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerDoc' href='{$url}'>
														</div>";
                                                    }
                                                    elseif ($model->documentTypeIcon == Document::doc) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerDoc' href='{$url}'>
														</div>";
                                                    }elseif ($model->documentTypeIcon == Document::jpg) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerImg' href='{$url}'>
														</div>";
                                                    }elseif ($model->documentTypeIcon == Document::zip) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" class=''>
														</div>";
                                                    }

                                                } else {
//                                                    return $model->documentTypeIcon;
                                                    return "";
                                                }
                                            },
                                        ],
                                        [
                                            'attribute' => 'documentTypeName',
                                            //'filter' => $docType,
                                            'header' => false,
                                            'format' => 'raw',
                                        ],
//                                    [
//                                        'attribute' => 'missionName',
//                                        'filter' => Mission::getList(),
//                                    ],
//                                    [
//                                        'attribute' => 'created_at',
//                                        'format' => 'datetime',
//                                        'filter' => false,
//                                    ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{view} {download}</div>',
                                            'header' => '',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-eye"></i>', Url::to(['document/view', 'id' => $model->document_id]), [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'download' => function ($url, $model) {
                                                    return Html::a('<i class="la la-download"></i>', Url::to(['document/download-zip', 'id' => $model->document_id]), [
                                                        'title' => 'ดาวน์โหลดข้อมูลทั้งหมด',
                                                        'class' => 'btn btn-success',
                                                        'target' => '_blank',
                                                        'data-pjax' => 0,
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>

                        <!--end::Widget 9-->
                    </div>
                </div>

                <!--end:: Widgets/Download Files-->


                <?php
            }
            ?>
        </div>

        <div class="col-xl-6 col-lg-6">
            <?php

            //$docType =  DocumentType::getList();
            //        foreach ($missionProvider as $mission) {
            //            $name = $mission->mission_name;
            //$modelIndex = ArrayHelper::index($dataProvider->getModels(),'document_id','mission_id');
            //var_dump($modelIndex);
            foreach ($mission_sub[1] as $item) {
                // filter model each of mission group
                //$docsModel = ArrayHelper::filter($modelIndex,[$item]);
                $dtPv = $searchModel->searchDocMission($item);
                $mis = ArrayHelper::index($missionProvider, 'mission_id');
                //var_dump($mis);
                //var_dump($item);
                //var_dump($mis[$item]);
                ?>

                <!--begin:: Widgets/Download Files-->
                <div class="kt-portlet kt-portlet--bordered">
                    <div class="kt-portlet__head kt-bg-light-success ">
                        <div class="kt-portlet__head-label">
                            <div class="kt-media">
                                <!--<div class="kt-media kt-media--sm kt-media--circle">-->
                                <img src="<?= $dir ?>/media/users/<?= $mis[$item]['mission_pic'] ?>" alt="" style="margin: 5px;">
                            </div>
                            <h2 class="kt-portlet__head-title">
                                <?= mb_strtoupper($mis[$item]['mission_name']) ?>
                            </h2>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                ตัวเลือก
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">

                                <!--begin::Nav-->
                                <ul class="kt-nav">
                                    <li class="kt-nav__head">
                                        รายการเอกสาร
                                    </li>
                                    <li class="kt-nav__separator"></li>
                                    <li class="kt-nav__item">
                                        <a href="<?= Url::to(['site/index-sub', 'id' => $item]) ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-document"></i>
                                            <span class="kt-nav__link-text">ดูทั้งหมด</span>
                                        </a>
                                    </li>
                                </ul>

                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin::k-widget4-->
                        <div class="kt-widget4">
                            <div class="table-responsive">
                                <?php Pjax::begin([
                                    'id' => 'document-pjax-container-' . $item,
                                ]); ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                                <?php
                                //$searchModel->mission_id = $item;
                                ?>
                                <?= GridView::widget([
                                    'dataProvider' => $dtPv,
                                    //'id' => 'document-pjax-container' . $item,
                                    //'filterModel' => $searchModel,
                                    'tableOptions' => [
                                        'class' => 'table table-hover table-striped',
                                    ],
                                    //'layout' => '{items}{pager}',
                                    'pager' => ['class'=>LinkPager::className()],
                                    'columns' => [
//                                    ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'documentTypeIcon',
                                            'header' => false,
                                            'format' => 'raw',
                                            'headerOptions' => ['class' => 'width:100px;'],
                                            'value' => function ($model) use ($dir) {
                                                $url = $model->getFileLink();
                                                if ($model->documentTypeIcon != '') {
                                                    if ($model->documentTypeIcon == Document::pdf) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerDoc' href='{$url}'>
														</div>";
                                                    }
                                                    elseif ($model->documentTypeIcon == Document::doc) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerDoc' href='{$url}'>
														</div>";
                                                    }elseif ($model->documentTypeIcon == Document::jpg) {
                                                        //return $model->documentTypeIcon;
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" style='cursor: pointer' class='showModalViewerImg' href='{$url}'>
														</div>";

                                                    }elseif ($model->documentTypeIcon == Document::zip) {
                                                        return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\" class=''>
														</div>";
                                                    }

                                                } else {
//                                                    return $model->documentTypeIcon;
                                                    return "";
                                                }
                                            },
                                        ],
                                        [
                                            'attribute' => 'documentTypeName',
                                            //'filter' => $docType,
                                            'header' => false,
                                            'format' => 'raw',
                                        ],
//                                    [
//                                        'attribute' => 'missionName',
//                                        'filter' => Mission::getList(),
//                                    ],
//                                    [
//                                        'attribute' => 'created_at',
//                                        'format' => 'datetime',
//                                        'filter' => false,
//                                    ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{view} {download}</div>',
                                            'header' => '',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-eye"></i>', Url::to(['document/view', 'id' => $model->document_id]), [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'download' => function ($url, $model) {
                                                    return Html::a('<i class="la la-download"></i>', Url::to(['document/download-zip', 'id' => $model->document_id]), [
                                                        'title' => 'ดาวน์โหลดข้อมูลทั้งหมด',
                                                        'class' => 'btn btn-success',
                                                        'target' => '_blank',
                                                        'data-pjax' => 0,
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                        <!--end::Widget 9-->
                    </div>
                </div>
                <!--end:: Widgets/Download Files-->
                <?php
            }
            ?>
        </div>
    </div>

    <?php
    Modal::begin([
        'title' => 'รายละเอียด',
        'id' => 'modalViewerDoc',
        'size' => Modal::SIZE_EXTRA_LARGE,
        'options' => [
                'max-width'=>'98%'
        ]
        //'toggleButton' => ['label' => 'click me'],
    ]);

    echo '';

    Modal::end();
    ?>


    <?php
    $this->registerJs('
$(document).on("ready pjax:success", function(){
    init();
});

$(function() {
    init();
});

function init(){
    $(".showModalViewerDoc").on("click",function(e){
            var pdf_link = $(this).attr("href");
            var iframe = "<div class=\"iframe-container\"><iframe width=\"100%\" height=\"600px\" src="+pdf_link+"></iframe></div>"
            console.log($(this).attr("href"));
            $("#modalViewerDoc").modal("show").find(".modal-body").html(iframe);
        
        });
        
        $(".showModalViewerImg").on("click",function(e){
            var img_link = $(this).attr("href");
            var iframe = "<div class=\"iframe-container \"><iframe class=\"embed-responsive-item\" width=\"100%\" height=\"600px\" src="+img_link+"></iframe></div>"
            console.log($(this).attr("href"));
            $("#modalViewerDoc").modal("show").find(".modal-body").html(iframe);
        
        });
}
        
    ',View::POS_READY);
    ?>
