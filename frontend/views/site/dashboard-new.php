<?php

/* @var $this yii\web\View */
/* @var $missionProvider Mission */
/* @var $dataProvider ActiveDataProvider */

/* @var $searchModel DocumentSearch */

/**
 * @var $docByMissionPvd ActiveDataProvider
 * @var $docByTypePvd ActiveDataProvider
 * @var $data1 array
 */

use common\models\Mission;
use frontend\models\DocumentSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;


//var_dump($data1);

$this->title = 'ข้อมูลสถานการณ์ COVID-19';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');
//$dataProvider->pagination = false;
/*$this->params['breadcrumbs'][] = [
    'label' => 'เอกสารทั้งหมด',
    #'url'=>'',
    'class' => 'kt-subheader__breadcrumbs-link',
];*/

//print_r($dataProvider);
//$missions = ArrayHelper::getColumn($missionProvider, 'mission_id');

//print_r($missions);

//$count = count($missions) / 2;
//$mission_sub = (array_chunk($missions, $count));

//var_dump($mission_sub);

?>
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <div class="kt-portlet__body  kt-portlet__body--fit">
                <div class="row row-no-padding row-col-separator-xl">
                    <!--<div class="col-xl-12">
                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="text-danger">
                                        <i class="fa fa-sad-cry"></i>  สถิติผู้ติดเชื้อโควิด-19 ในไทย
                                    </h3>
                                </div>
                                <div class="row">
                                    <div class="col-xl-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-warning">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title">
                                                        ติดเชื้อเพิ่มขึ้น
                                                    </h3>
                                                    <span class="kt-widget14__desc"></span>
                                                </div>
                                                <div class="kt-widget14__content">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-xl-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-success">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title">
                                                        หายแล้ว
                                                    </h3>
                                                    <span class="kt-widget14__desc"></span>
                                                </div>
                                                <div class="kt-widget14__content">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-xl-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-warning">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title">
                                                        รักษาตัว
                                                    </h3>
                                                    <span class="kt-widget14__desc"></span>
                                                </div>
                                                <div class="kt-widget14__content">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-xl-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-danger">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title">
                                                        เสียชีวิต
                                                    </h3>
                                                    <span class="kt-widget14__desc"></span>
                                                </div>
                                                <div class="kt-widget14__content">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>-->
                    <div class="col-xl-12">
                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light" style='background: url("<?=$dir?>/media/bg/covid-bg.png")'>
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="text-danger">
                                        สถิติผู้ติดเชื้อโควิด-19 ในประเทศไทย
                                        <span class="pull-right"><a href="https://covid19.ddc.moph.go.th/en" target="_blank" class="btn btn-primary">ข้อมูล Covid-19 ทั้งหมด <i class="fa fa-chevron-circle-right"></i> </a></span>
                                    </h3>
                                </div>
                                <div class="clearfix mb-md-1"></div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-danger">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title text-center text-dark">
                                                        ติดเชื้อเพิ่มขึ้น
                                                    </h3>
                                                </div>
                                                <div class="">
                                                    <h2 class="text-danger text-center" style="font-weight: 900; font-size: 4.5rem;"
                                                        id="NewConfirmed"></h2>
                                                    <h4 class="kt-widget14__desc text-center text-muted" style="font-weight: 900; font-size: 1.2rem;"
                                                        id="Confirmed">
                                                        ติดเชื้อสะสม
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-info">
                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title text-center text-dark">
                                                        หายแล้ว
                                                    </h3>
                                                </div>
                                                <div class="">
                                                    <h2 class="text-info text-center" style="font-weight: 900; font-size: 4.5rem;"
                                                        id="Recovered"></h2>
                                                    <h4 class="kt-widget14__desc text-center text-muted" style="font-weight: 900; font-size: 1.2rem;"
                                                        id="NewRecovered">
                                                        เพิ่มขึ้น
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-warning">

                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title text-center text-dark">
                                                        รักษาตัว
                                                    </h3>
                                                </div>
                                                <div class="">
                                                    <h2 class="text-warning text-center" style="font-weight: 900; font-size: 4.5rem;"
                                                        id="Hospitalized"></h2>
                                                    <h4 class="kt-widget14__desc text-center text-muted" style="font-weight: 900; font-size: 1.2rem;"
                                                        id="NewHospitalized">
                                                        เพิ่มขึ้น
                                                    </h4>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-danger">

                                            <div class="kt-widget14">
                                                <div class="kt-widget14__header">
                                                    <h3 class="kt-widget14__title text-center text-dark">
                                                        เสียชีวิต
                                                    </h3>
                                                </div>
                                                <div class="">
                                                    <h2 class="text-danger text-center" style="font-weight: 900; font-size: 4.5rem;"
                                                        id="Deaths"></h2>
                                                    <h4 class="kt-widget14__desc text-center text-muted" style="font-weight: 900; font-size: 1.2rem;"
                                                        id="NewDeaths">
                                                        เพิ่มขึ้น
                                                    </h4>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3" id="UpdateDate">ข้อมูล ณ </div>
                            </div>
                        </div>
                    </div>

                    <!--<div class="col-xl-12">
                        <iframe width="100%" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiNmU0NjdiNjEtZmU1Mi00NzBiLWEyMzAtOWMzNjNhMGM3NDM0IiwidCI6IjcyYTY4ZjEzLTRhOWMtNGM0MC1iMzE3LTg1NzAyNTE1ZDE2YSIsImMiOjEwfQ%3D%3D" frameborder="0" allowFullScreen="true"></iframe>
                    </div>-->

                </div>
            </div>
        </div>

        <!--<div class="kt-portlet">
            <div class="kt-portlet__body  kt-portlet__body--fit">
                <div class="row row-no-padding row-col-separator-xl">
                    <div class="col-xl-6">
                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-info">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header kt-margin-b-30">
                                    <h3 class="kt-widget14__title">
                                        <i class="fa fa-smile"></i>  ข้อมูลการคัดกรองเยียวยาจิตใจ จังหวัดมุกดาหาร
                                    </h3>

                                    <span class="kt-widget14__desc">แสดงข้อมูลรายเดือน (เป้าหมาย และผลงานการคัดกรอง)</span>
                                </div>
                                <div class="kt-widget14__chart" style="height:200px;">
                                    <canvas id="kt_chart_daily_sales"></canvas>

                                </div>
                                <div class="pull-right kt-margin-t-10">
                                    <a href="<?/*=Url::to(['cm-target-result/index']) */ ?>" class="btn btn-info btn-sm">ดูทั้งหมด</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-warning">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        ข้อมูลผลการคัดการ (เครียดมาก)
                                    </h3>
                                    <span class="kt-widget14__desc"></span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <div id="kt_chart_revenue_change" style="height: 150px; width: 150px;"></div>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-success"></span>
                                            <span class="kt-widget14__stats">คัดกรองทั้งหมด</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-danger"></span>
                                            <span class="kt-widget14__stats">เครียดมาก</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="kt-portlet kt-portlet--height-fluid kt-bg-light-danger">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        ข้อมูลผลการคัดการ (เสี่ยงฆ่าตัวตาย)
                                    </h3>
                                    <span class="kt-widget14__desc">

															</span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <div id="chartreport02" style="height: 150px; width: 150px;"></div>
                                    </div>
                                    <div class="kt-widget14__legends">
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-success"></span>
                                            <span class="kt-widget14__stats">คัดกรองทั้งหมด</span>
                                        </div>
                                        <div class="kt-widget14__legend">
                                            <span class="kt-widget14__bullet kt-bg-danger"></span>
                                            <span class="kt-widget14__stats">เสี่ยงฆ่าตัวตาย</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="row">
            <div class="col-xl-6">

                <!--begin:: Widgets/Support Cases-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fa fa-book-reader"></i> สรุปเอกสารตามกลุ่มภารกิจ
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget16">
                            <div class="kt-widget16__items">
                                <div class="kt-widget16__item">
                                    <span class="kt-widget16__scheduled">กล่องภารกิจ</span>
                                    <span class="kt-widget16__amount">จำนวนเอกสาร</span>
                                </div>
                                <?php
                                $modelByMission = $docByMissionPvd->getModels();
                                $color = ["info", "success", "danger", "brand", "light-primary", "dark", "primary", "warning", "info", "success", "danger"];
                                $legends = [];

                                foreach ($modelByMission as $index => $item) {
                                    if ($item->tot_doc > 0) {
                                        $legends[$index] = [
                                            "bg" => $color[$index],
                                            "label" => $item->mission_name,
                                            "value" => $item->tot_doc,
                                        ];
                                    }
                                    ?>
                                    <div class="kt-widget16__item">
                                        <span class="kt-widget16__date"><?= $item->mission_name ?></span>
                                        <span class="kt-widget16__price  kt-font-brand"><a
                                                    href="<?= Url::to(['site/index-sub', 'id' => $item->mission_id]) ?>"
                                                    target="_blank"><?= $item->tot_doc ?></a></span>
                                    </div>
                                    <?php
                                }

                                /*mootensai\components\JsBlock::widget(['viewFile' => '_script_dashboard', 'viewParams' => [
                                    'data' => $legends,
                                    'color'=>$color,
                                ], 'pos' => \yii\web\View::POS_READY]);*/
                                ?>
                            </div>
                            <div class="kt-widget16__stats">

                                <div class="kt-widget16__visual">
                                    <div id="kt_chart_support_tickets" style="height: 160px; width: 160px;">
                                    </div>
                                </div>

                                <div class="kt-widget16__legends">
                                    <?php
                                    foreach ($legends as $legend) {
                                        ?>
                                        <div class="kt-widget16__legend">
                                            <span class="kt-widget16__bullet kt-bg-<?= $legend['bg'] ?>"></span>
                                            <span class="kt-widget16__stat"><?= $legend['label'] . " (" . $legend['value'] . ")" ?></span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <!--<div class="row"><div class="col-sm-12">-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/Support Stats-->
            </div>
            <div class="col-xl-6">

                <!--begin:: Widgets/Support Cases-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <i class="fa fa-file-code"></i> สรุปเอกสารตามประเภทเอกสาร
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget16">
                            <div class="kt-widget16__items">
                                <div class="kt-widget16__item">
                                    <span class="kt-widget16__scheduled">ประเภทเอกสาร</span>
                                    <span class="kt-widget16__amount">จำนวนเอกสาร</span>
                                </div>
                                <?php
                                $modelByType = $docByTypePvd->getModels();
                                //$color = ["info", "success", "danger", "brand", "light-primary", "dark", "primary", "warning", "info", "success", "danger"];
                                $legends2 = [];

                                foreach ($modelByType as $index => $item) {
                                    if ($item->tot_doc > 0) {
                                        $legends2[$index] = [
                                            "bg" => $color[$index],
                                            "label" => $item->doctype_name,
                                            "value" => $item->tot_doc,
                                        ];
                                    }
                                    ?>
                                    <div class="kt-widget16__item">
                                        <span class="kt-widget16__date"><?= $item->doctype_name ?></span>
                                        <span class="kt-widget16__price  kt-font-brand"><a
                                                    href="<?= Url::to(['site/index-sub', 'type_id' => $item->doctype_id]) ?>"
                                                    target="_blank"><?= $item->tot_doc ?></a></span>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="kt-widget16__stats">

                                <div class="kt-widget16__visual">
                                    <div id="kt_chart_support_requests" style="height: 160px; width: 160px;">
                                    </div>
                                </div>

                                <div class="kt-widget16__legends">
                                    <?php
                                    foreach ($legends2 as $legend) {
                                        ?>
                                        <div class="kt-widget16__legend">
                                            <span class="kt-widget16__bullet kt-bg-<?= $legend['bg'] ?>"></span>
                                            <span class="kt-widget16__stat"><?= $legend['label'] . " (" . $legend['value'] . ")" ?></span>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <!--<div class="row"><div class="col-sm-12">-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/Support Stats-->
            </div>
        </div>
    </div>


<?php
mootensai\components\JsBlock::widget(['viewFile' => '_script_dashboard', 'viewParams' => [
    'data' => $legends,
    'data2' => $legends2,
    'data3' => $data1,
    'color' => $color,
], 'pos' => \yii\web\View::POS_READY]);
?>

<?php
/*Modal::begin([
    'title' => 'รายละเอียด',
    'id' => 'modalViewerDoc',
    'size' => Modal::SIZE_EXTRA_LARGE,
    'options' => [
        'max-width' => '98%',
    ]
    //'toggleButton' => ['label' => 'click me'],
]);

echo '';

Modal::end();*/
?>


<?php
/*$this->registerJs('
$(document).on("ready pjax:success", function(){
    init();
});

$(function() {
    init();
});

function init(){
    $(".showModalViewerDoc").on("click",function(e){
            var pdf_link = $(this).attr("href");
            var iframe = "<div class=\"iframe-container\"><iframe width=\"100%\" height=\"600px\" src="+pdf_link+"></iframe></div>"
            console.log($(this).attr("href"));
            $("#modalViewerDoc").modal("show").find(".modal-body").html(iframe);
        
        });
        
        $(".showModalViewerImg").on("click",function(e){
            var img_link = $(this).attr("href");
            var iframe = "<div class=\"iframe-container \"><iframe class=\"embed-responsive-item\" width=\"100%\" height=\"600px\" src="+img_link+"></iframe></div>"
            console.log($(this).attr("href"));
            $("#modalViewerDoc").modal("show").find(".modal-body").html(iframe);
        
        });
}
        
    ', View::POS_READY);*/
?>


<?php
