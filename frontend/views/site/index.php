<?php

/* @var $this yii\web\View */

use Codeception\Module\Yii2;
use common\models\DocumentType;
use common\models\Mission;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'เอกสารทั้งหมด';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');

/*$this->params['breadcrumbs'][] = [
    'label' => 'เอกสารทั้งหมด',
    #'url'=>'',
    'class' => 'kt-subheader__breadcrumbs-link',
];*/


?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col">
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-information kt-font-brand"></i></div>
                <div class="alert-text">
                    ระบบจัดเก็บและเผยแพร่เอกสารเกี่ยวกับกรณีโรคระบาด COVID-19 สสจ.มุกดาหาร
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <i class="flaticon2-document kt-font-brand"></i> <?= Html::encode($this->title) ?>
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <!--begin::Section-->
                    <div class="kt-section">
                        <span class="kt-section__info">

                        </span>
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <?php Pjax::begin([
                                    'id' => 'document-pjax-container',
                                ]); ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        //'document_id',
                                        [
                                            'attribute' => 'documentTypeIcon',
                                            'filter' => false,
                                            'format' => 'raw',
                                            'headerOptions' => ['class'=>'width:60px;'],
                                            'value'=> function($model) use ($dir){
                                                if($model->documentTypeIcon!=''){
                                                    return "<div class=\"kt-media\">
															<img src=\"{$dir}/media/files/{$model->documentTypeIcon}\" alt=\"\">
														</div>";
                                                }else{
                                                    return "-";
                                                }
                                            }
                                        ],
                                        [
                                            'attribute' => 'documentTypeTitle',
                                            'filter' => DocumentType::getList(),
                                            'format' => 'raw'
                                        ],
                                        'title',
                                        //'document_name',
                                        //'description:raw',

                                        /*[
                                            'attribute' => 'documentType',
                                            'value' => function ($model){

                                            }
                                        ],*/
                                        //'tags',
                                        //'status',
                                        //'has_thumbnail',


                                        [
                                            'attribute' => 'missionName',
                                            'filter' => Mission::getList(),
                                        ],
                                        [
                                            'attribute' => 'createBy',
                                        ],
                                        [
                                            'attribute' => 'created_at',
                                            'format' => 'datetime',
                                            'filter' => false,
                                        ],
                                        //'updated_by',
                                        //'updated_at',

                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{view} {download}</div>',
                                            'header' => 'จัดการ',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-search-plus"></i>', Url::to(['document/view', 'id' => $model->document_id]), [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'download' => function ($url, $model) {
                                                    return Html::a('<i class="la la-download"></i>', Url::to(['document/download-zip', 'id' => $model->document_id]), [
                                                        'title' => 'ดาวน์โหลดข้อมูลทั้งหมด',
                                                        'class' => 'btn btn-success',
                                                        'target' => '_blank',
                                                        'data-pjax' => 0,
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>