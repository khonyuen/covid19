<?php
/**
 * @var $data array
 * @var $data2 array
 * @var $data3 array
 * @var $color array
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

//print_r($data);
//$data_filter = ArrayHelper::remove($data,'bg');
//$data_filter = Json::encode($data_filter);
//print_r($data_filter);
$cl = ArrayHelper::getColumn($data, 'bg');
$cl2 = ArrayHelper::getColumn($data2, 'bg');
$cl3 = ArrayHelper::getColumn($data3, 'label', false);
//var_dump($cl);
$string = [];
$string2 = [];
foreach ($cl as $item) {
    $string[] = "KTApp.getStateColor('{$item}')";
}

foreach ($cl2 as $item) {
    $string2[] = "KTApp.getStateColor('{$item}')";
}

$target = ArrayHelper::getColumn($data3, 'tot', false);
$targetRs = ArrayHelper::getColumn($data3, 'rs', false);


$columnL = ["เป้าหมายทั้งหมด/ผล","เครียดมาก","แนวโน้มซึมเศร้า","ซึมเศร้า","เสี่ยงฆ่าตัวตาย","ติดตาม"];

?>

<script>
    "use strict";

    // Class definition
    var KTDashboard = function () {
        // Support Tickets Chart.
        // Based on Morris plugin - http://morrisjs.github.io/morris.js/
        var supportCases = function () {
            if ($('#kt_chart_support_tickets').length == 0) {
                return;
            }

            Morris.Donut({
                element: 'kt_chart_support_tickets',
                data: <?=Json::encode($data)?>,
                resize: true,
                labelColor: '#a7a7c2',
                colors: [
                    <?=implode(',', $string)?>
                ]
                //<?//=Json::encode($cl)?>
                //formatter: function (x) { return x + "%"}
            });
        };

        var dailySales = function () {
            var chartContainer = KTUtil.getByID('kt_chart_daily_sales');

            if (!chartContainer) {
                return;
            }

            var chartData = {
                labels: <?=Json::encode($columnL)?>,
                datasets: [{
                    label: 'เป้าหมายคัดกรอง',
                    backgroundColor: KTApp.getStateColor('info'),
                    data: [
                        <?=$data3['sum']['tot']?>,
                        <?=$data3['sum']['rs']?>,
                        <?=$data3['sum']['rs']?>,
                        <?=$data3['sum']['rs']?>,
                        <?=$data3['sum']['rs']?>,
                        <?=$data3['sum']['rs']?>,
                    ]
                }, {
                    label: 'ผลคัดกรอง',
                    //categoryPercentage : 1,
                    backgroundColor: KTApp.getStateColor('danger'),
                    data: [
                        <?=$data3['sum']['rs']?>,
                        <?=$data3['sum']['l1']?>,
                        <?=$data3['sum']['l2']?>,
                        <?=$data3['sum']['l3']?>,
                        <?=$data3['sum']['l4']?>,
                        <?=$data3['sum']['l5']?>
                    ]
                }
                ]
            };

            var chart = new Chart(chartContainer, {
                type: 'bar',
                data: chartData,
                options: {
                    title: {
                        display: false,
                    },
                    tooltips: {
                        intersect: false,
                        mode: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: true,
                        position: 'right'
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    barRadius: 4,
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: true,
                            stacked: true,
                            ticks: {
                                autoSkip: false,
                                maxRotation: 20,
                                minRotation: 20,
                                padding: 10
                            }
                        }],
                        yAxes: [{
                            display: false,
                            stacked: true,
                            gridLines: true
                        }]
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    }
                }
            });
        };

        var reports1 = function () {
            if ($('#chartreport02').length == 0) {
                return;
            }

            Morris.Donut({
                element: 'chartreport02',
                data: [
                    {"label": "คัดกรองทั้งหมด", "value":<?=$data3['sum']['rs']?>},
                    {"label": "เสี่ยงฆ่าตัวตาย", "value":<?=$data3['sum']['l4']?>}],
                resize: true,
                labelColor: '#a7a7c2',
                colors: [
                    "#34bfa3", "#fd3995"
                ]
                //<?//=Json::encode($cl)?>
                //formatter: function (x) { return x + "%"}
            });
        }

        var reports2 = function () {
            if ($('#kt_chart_revenue_change').length == 0) {
                return;
            }

            Morris.Donut({
                element: 'kt_chart_revenue_change',
                data: [
                    {"label": "คัดกรองทั้งหมด", "value":<?=$data3['sum']['rs']?>},
                    {"label": "เครียดมาก", "value":<?=$data3['sum']['l1']?>}],
                resize: true,
                labelColor: '#a7a7c2',
                colors: [
                    "#34bfa3", "#fd3995"
                ]
                //<?//=Json::encode($cl)?>
                //formatter: function (x) { return x + "%"}
            });
        }

        // Support Tickets Chart.
        // Based on Chartjs plugin - http://www.chartjs.org/
        var supportRequests = function () {
            if ($('#kt_chart_support_requests').length == 0) {
                return;
            }

            Morris.Donut({
                element: 'kt_chart_support_requests',
                data: <?=Json::encode($data2)?>,
                resize: true,
                labelColor: '#a7a7c2',
                colors: [
                    <?=implode(',', $string2)?>
                ]
                //<?//=Json::encode($cl)?>
                //formatter: function (x) { return x + "%"}
            });
        }

        var getCovidSituation = function () {
            var url = "https://covid19.th-stat.com/api/open/today";
            var data = $.ajax({
                url: url,
                dataType: 'json',
                success: function (data, status, jqXhr) {
                    console.log(data)
                    $("#NewConfirmed").html(numberWithCommas(data.NewConfirmed)); $("#Confirmed").append(numberWithCommas(data.Confirmed));
                    $("#Recovered").html(numberWithCommas(data.Recovered)); $("#NewRecovered").append(numberWithCommas(data.NewRecovered));
                    $("#Hospitalized").html(numberWithCommas(data.Hospitalized)); $("#NewHospitalized").append(numberWithCommas(data.NewHospitalized));
                    $("#Deaths").html(numberWithCommas(data.Deaths)); $("#NewDeaths").append(numberWithCommas(data.NewDeaths));
                    $("#UpdateDate").append(data.UpdateDate+"<br>แหล่งที่มา : "+data.Source);
                }   ,
                error: function (v, k) {
                    console.log(v,k)
                }
            });
        }

        function numberWithCommas(number) {
            var parts = number.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        return {
            // Init demos
            init: function () {
                // init charts
                supportCases();
                supportRequests();
                dailySales();
                getCovidSituation();
                //reports1();
                //reports2();
                // demo loading
                var loading = new KTDialog({'type': 'loader', 'placement': 'top center', 'message': 'Loading ...'});
                loading.show();

                setTimeout(function () {
                    loading.hide();
                }, 3000);
            }
        };
    }();

    // Class initialization on page load
    jQuery(document).ready(function () {
        KTDashboard.init();
    });
</script>
