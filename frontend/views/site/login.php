<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $signup \frontend\models\SignupForm */
/* @var $mission \common\models\Mission[] */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@t01/dist');


use common\widgets\Alert;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

?>

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
             style="background-image: url(<?= $directoryAsset ?>/media/bg/bg-1.jpg);">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">

                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="<?= $directoryAsset ?>/media/logos/logo-big1.png">
                        </a>
                    </div>
                    <?= Alert::widget() ?>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title"><?=Yii::$app->name;?><br><br>กรุณา Sign In เพื่อจัดการข้อมูล</h3>
                        </div>
                        <!--<form class="kt-form-1" action="">-->
                        <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => [
                            'class' => 'kt-form',
                        ]]); ?>

                        <?= $form->field($model, 'username', [

                        ])->textInput(['placeholder' => 'Username'])->label(false) ?>
                        <?= $form->field($model, 'password', [])->passwordInput(['placeholder' => 'Password'])->label(false) ?>


                        <div class="row kt-login__extra">
                            <div class="col">
                                <?= $form->field($model, 'rememberMe')->checkbox(['name' => 'remember']) ?>
                            </div>
                            <div class="col kt-align-right">
                                <a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link">ลืมรหัสผ่าน ?</a>
                            </div>
                        </div>
                        <div class="kt-login__actions">
                            <!--<button id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">Sign In</button>-->
                            <?= Html::submitButton('Sign In', ['class' => 'btn btn-pill kt-login__btn-primary']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>

                    <div class="kt-login__signup">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">ลงทะเบียน</h3>
                            <div class="kt-login__desc">กรอกข้อมูลในการลงทะเบียนให้สมบูรณ์ :</div>
                        </div>
                        <!--<form class="kt-login__form kt-form" action="">-->
                        <?php $form = ActiveForm::begin([
                            'id' => 'sign-up-form',
                            'action' => Url::to(['site/signup']),
                            'validationUrl' => Url::to(['site/signup']),
                            'enableAjaxValidation' => true,
                            'validateOnSubmit' => true,
                            'validateOnBlur' => false,
                            'validateOnChange' => false,
                            'scrollToError' => true,
                            'options' => [
                                'class' => 'kt-login__form kt-form',
                            ]]); ?>

                        <?= $form->field($signup, 'username', [])->textInput(['placeholder' => 'Username'])->label(false) ?>
                        <?= $form->field($signup, 'email', [])->textInput(['placeholder' => 'Email'])->label(false) ?>

                        <!--<input class="form-control" type="text" placeholder="Username" name="rusername">-->

                        <?= $form->field($signup, 'password', [])->passwordInput(['placeholder' => 'รหัสผ่าน'])->label(false) ?>
                        <!--<input class="form-control" type="password" placeholder="Password" name="password">-->

                        <?= $form->field($signup, 'repeat_password', [])->passwordInput(['placeholder' => 'ยืนยันรหัสผ่าน'])->label(false) ?>
                        <?= $form->field($signup, 'title', [])->textInput(['placeholder' => 'คำนำหน้า'])->label(false) ?>
                        <?= $form->field($signup, 'firstname', [])->textInput(['placeholder' => 'ชื่อ'])->label(false) ?>
                        <?= $form->field($signup, 'lastname', [])->textInput(['placeholder' => 'สกุล'])->label(false) ?>
                        <?= $form->field($signup, 'position', [])->textInput(['placeholder' => 'ตำแหน่ง'])->label(false) ?>
                        <?= $form->field($signup, 'mission_id', [])->dropDownList($mission, [])->label(false) ?>

                        <!--<div class="row kt-login__extra">
                            <div class="col kt-align-left">
                                <label class="kt-checkbox">
                                    <input type="checkbox" name="agree">ยอมรับ <a href="#" class="kt-link kt-login__link kt-font-bold">ข้อตกลงและเงื่อนไขการใช้ระบบ</a>.
                                    <span></span>
                                </label>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>-->
                        <div class="kt-login__actions">
                            <?= Html::submitButton('Sign Up', ['class' => 'btn btn-pill kt-login__btn-primary']) ?>
                            <?= Html::button('ยกเลิก', ['id' => 'kt_login_signup_cancel', 'class' => 'btn btn-pill kt-login__btn-secondary']) ?>

                            <!--<button id="kt_login_signup_submit" class="btn btn-pill kt-login__btn-primary">ลงทะเบียน</button>&nbsp;&nbsp;
                            <button id="kt_login_signup_cancel" class="btn btn-pill kt-login__btn-secondary">ยกเลิก</button>-->
                        </div>
                        <?php ActiveForm::end(); ?>
                        <!--</form>-->
                    </div>
                    <div class="kt-login__forgot">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">ลืมรหัสผ่าน ?</h3>
                            <div class="kt-login__desc">กรุณาใส่ Email ที่ใช้งานในการลงทะเบียนครั้งแรก :</div>
                        </div>
                        <form class="kt-form" action="">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_forgot_submit" class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                                <button id="kt_login_forgot_cancel" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                            </div>
                        </form>
                    </div>
                    <div class="kt-login__account">
								<span class="kt-login__account-msg">
									ยังไม่มีบัญชีผู้ใช้งาน ?
								</span>&nbsp;&nbsp;
                        <a href="javascript:;" id="kt_login_signup" class="kt-link kt-link--light kt-login__account-link">ลงทะเบียน</a>
                    </div>
                    <div class="kt-login__account">
								<span class="kt-login__account-msg">
									ดาวน์โหลดเอกสารที่เกี่ยวข้อง
								</span>&nbsp;&nbsp;
                        <a href="<?= \yii\helpers\Url::to(['site/index']) ?>" id="kt_login_signup1"
                           class="kt-link kt-link--light kt-login__account-link">คลิ๊ก</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<!--<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#366cf3",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>-->

<!-- end::Global Config -->

<script>

</script>

<?php



?>
