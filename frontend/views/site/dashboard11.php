<?php

/* @var $this yii\web\View */
/* @var $missionProvider Mission */
/* @var $dataProvider ActiveDataProvider */

/* @var $searchModel DocumentSearch */

use common\models\DocumentType;
use common\models\Mission;
use frontend\models\DocumentSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'เอกสารเผยแพร่ COVID-19';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => 'kt-subheader__breadcrumbs-link',
];

$dir = Yii::$app->assetManager->getPublishedUrl('@t01/dist');

/*$this->params['breadcrumbs'][] = [
    'label' => 'เอกสารทั้งหมด',
    #'url'=>'',
    'class' => 'kt-subheader__breadcrumbs-link',
];*/

$missionIds = ArrayHelper::getColumn($missionProvider, 'mission_id');
?>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <?php
        //$docType = DocumentType::getList();
        foreach ($missionProvider as $mission) {
            $name = $mission->mission_name;
            $dt = $searchModel->searchDocMission($mission->mission_id);
            ?>
            <div class="col-xl-6 col-lg-6">
                <!--begin:: Widgets/Download Files-->
                <div class="kt-portlet"> <!--kt-portlet--height-fluid kt-portlet--bordered-->
                    <div class="kt-portlet__head kt-bg-light-danger">
                        <div class="kt-portlet__head-label">
                            <div class="kt-media">
                                <!--<div class="kt-media kt-media--sm kt-media--circle">-->
                                <img src="<?= $dir ?>/media/users/<?= $mission->mission_pic ?>" alt="" style="margin: 5px;">
                            </div>
                            <h2 class="kt-portlet__head-title">
                                <?= mb_strtoupper($name) ?>
                            </h2>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                ตัวเลือก
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">

                                <!--begin::Nav-->
                                <ul class="kt-nav">
                                    <li class="kt-nav__head">
                                        รายการเอกสาร
                                    </li>
                                    <li class="kt-nav__separator"></li>
                                    <li class="kt-nav__item">
                                        <a href="<?= Url::to(['document/index', 'id' => $mission->mission_id]) ?>" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-document"></i>
                                            <span class="kt-nav__link-text">ดูทั้งหมด</span>
                                        </a>
                                    </li>
                                </ul>

                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <!--begin::k-widget4-->
                        <div class="kt-widget4">
                            <!--<div class="kt-widget4__item">
                                <div class="kt-widget4__pic kt-widget4__pic--icon">
                                    <img src="assets/media/files/doc.svg" alt="">
                                </div>
                                <a href="#" class="kt-widget4__title">
                                    Metronic Documentation
                                </a>
                                <div class="kt-widget4__tools">
                                    <a href="#" class="btn btn-clean btn-icon btn-sm">
                                        <i class="flaticon2-download-symbol-of-down-arrow-in-a-rectangle"></i>
                                    </a>
                                </div>
                            </div>-->
                            <div class="table-responsive">
                                <?php Pjax::begin([
                                    'id' => 'document-pjax-container-' . $mission->mission_id,
                                ]); ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                                <?php
                                $searchModel->mission_id = $mission->mission_id;
                                ?>
                                <?= GridView::widget([
                                    'dataProvider' => $dt,
                                    //'filterModel' => $searchModel,
                                    'tableOptions' => [
                                        'class' => 'table',
                                    ],
                                    'layout' => '{items}{pager}',
                                    'columns' => [
//                                    ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'attribute' => 'documentTypeName',
                                            //'filter' => $docType,
                                            'header' => '',
                                        ],
                                        'title',
//                                    [
//                                        'attribute' => 'missionName',
//                                        'filter' => Mission::getList(),
//                                    ],
//                                    [
//                                        'attribute' => 'created_at',
//                                        'format' => 'datetime',
//                                        'filter' => false,
//                                    ],
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<div class="btn btn-group text-center" role="group">{view} {download}</div>',
                                            'header' => 'จัดการ',
                                            'headerOptions' => [
                                                'style' => 'align:center',
                                            ],
                                            'options' => [
                                                'width' => '120px',
                                            ],
                                            'buttonOptions' => [
                                                'class' => 'btn btn-default btn-sm',
                                            ],
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<i class="la la-search-plus"></i>', Url::to(['document/view', 'id' => $model->document_id]), [
                                                        'title' => 'ดูข้อมูล',
                                                        'class' => 'btn btn-primary',
                                                        'data-pjax' => 0,
                                                    ]);
                                                }, 'download' => function ($url, $model) {
                                                    return Html::a('<i class="la la-download"></i>', Url::to(['document/download-zip', 'id' => $model->document_id]), [
                                                        'title' => 'ดาวน์โหลดข้อมูลทั้งหมด',
                                                        'class' => 'btn btn-success',
                                                        'target' => '_blank',
                                                        'data-pjax' => 0,
                                                    ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>

                        <!--end::Widget 9-->
                    </div>
                </div>

                <!--end:: Widgets/Download Files-->
            </div>

            <?php
        }
        ?>
    </div>