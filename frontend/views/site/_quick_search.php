<?php
/**
 * @var $searchModel DocumentSearch
 * @var $dataProvider ActiveDataProvider
 */

use frontend\models\DocumentSearch;
use http\Url;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;


?>


<div class="quick-search-result">
    <!--begin::Message-->
    <div class="text-muted d-none">
        No record found
    </div>
    <!--end::Message-->

    <!--begin::Section-->
    <div class="font-size-sm text-primary font-weight-bolder text-uppercase mb-2">
        ผลการค้นหา <?=$query?>
    </div>
    <div class="mb-10">
        <?php
        foreach ($dataProvider->getModels() as $model) {

            ?>
            <div class="d-flex align-items-center flex-grow-1 mb-2">
                <div class="symbol symbol-30 bg-transparent flex-shrink-0">
                    <img src="<?=Yii::$app->assetManager->getPublishedUrl('@t01')?>/media/svg/files/doc.svg" alt=""/>
                </div>
                <div class="d-flex flex-column ml-3 mt-2 mb-2">
                    <a href="<?=\yii\helpers\Url::to(['document/view','id'=>$model->document_id])?>" class="font-weight-bold text-dark text-hover-primary">
                        <?=$model->title?>
                    </a>
                    <span class="font-size-sm font-weight-bold text-muted">
                        <?=$model->description?>
                </span>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <!--end::Section-->
</div>
