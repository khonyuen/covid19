<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CovidProvince */

$this->title = 'Create Covid Province';
$this->params['breadcrumbs'][] = ['label' => 'Covid Province', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
