<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\CovidProvince */

$this->title = 'Save As New Covid Province: '. ' ' . $model->province_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Province', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->province_id, 'url' => ['view', 'id' => $model->province_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="covid-province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
