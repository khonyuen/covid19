<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidProvince */

$this->title = $model->province_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Province', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="covid-province-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Covid Province'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'province_id',
        'province_name_detail',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
