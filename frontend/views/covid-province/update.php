<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CovidProvince */

$this->title = 'Update Covid Province: ' . ' ' . $model->province_id;
$this->params['breadcrumbs'][] = ['label' => 'Covid Province', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->province_id, 'url' => ['view', 'id' => $model->province_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="covid-province-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
