<?php

use kartik\date\DatePicker;
use marqu3s\summernote\Summernote;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $reportModel common\models\MeetingReport */
/* @var $model common\models\MeetingResponsible */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    /*'viewParams' => [
        'class' => 'MeetingReportComment', 
        'relID' => 'meeting-report-comment', 
        'value' => \yii\helpers\Json::encode($model->meetingReportComments),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]*/
]);
?>

<div class="meeting-responsible-form">

    <?php $form = \yii\bootstrap4\ActiveForm::begin([
        'id' => 'update-meeting-report',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validationUrl' => Url::to(['validate-meeting-report']),
        'options' => [
            'class' => 'kt-form kt-form--fit kt-form--label-right',
            'enctype' => 'multipart/form-data',
        ],
    ]);

    echo Html::activeHiddenInput($reportModel, 'report_id');
    echo Html::activeHiddenInput($reportModel, 'report_path_temp');
    //echo Html::activeHiddenInput($reportModel, 'images_tmp');
    ?>

    <div class="well-sm"><h4><?=$reportModel->responsible->item->item_detail?></h4></div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($reportModel, 'report_detail')->widget(Summernote::className(),[]) ?>
        </div>

        <div class="col-lg-6 col-xs-12">
            <?= $form->field($reportModel, 'report_file[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'initialPreview' => $reportModel->initialPreview($reportModel->report_file, 'report_file', 'files'),
                    'previewFileType' => ['pdf', 'html', 'text', 'video', 'audio', 'flash', 'object','rar','zip'], //any
                    //'allowedFileExtensions' => ['jpg', 'png'],
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => false,
                    'autoReplace' => true,
                    'maxFileCount' => 20,
                    'maxFileSize'=>28000,
                    'purifyHtml' => true,
                    //'deleteUrl'=>Url::to(['area/deletefile','id'=>$model->area_id, 'field'=>'area_pic','fileName'=>'']),
                    'initialPreviewAsData' => true,
                    'initialPreviewConfig' => $reportModel->initialPreview($reportModel->report_file, 'report_file', 'config'),
                    'filepredelete' => new \yii\web\JsExpression(
                        'function(jqXHR) {
                                        var abort = true;
                                        if (confirm("ต้องการลบข้อมูลนี้ ?")) {
                                            abort = false;
                                        }
                                        return abort;
                                    }'
                    ),
                ],
            ]) ?>
        </div>

        <div class="col-lg-6 col-xs-12">
            <?= $form->field($reportModel, 'report_images[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'initialPreview' => $reportModel->initialPreview($reportModel->report_images, 'report_images', 'files'),
                    'previewFileType' => ['image', 'html', 'text', 'video', 'audio', 'flash', 'object','rar','zip'], //any
                    //'allowedFileExtensions' => ['jpg', 'png'],
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => false,
                    'autoReplace' => true,
                    'maxFileCount' => 20,
                    'maxFileSize'=>28000,
                    'purifyHtml' => true,
                    //'deleteUrl'=>Url::to(['area/deletefile','id'=>$model->area_id, 'field'=>'area_pic','fileName'=>'']),
                    'initialPreviewAsData' => true,
                    'initialPreviewConfig' => $reportModel->initialPreview($reportModel->report_images, 'report_images', 'config'),
                    'filepredelete' => new \yii\web\JsExpression(
                        'function(jqXHR) {
                                        var abort = true;
                                        if (confirm("ต้องการลบข้อมูลนี้ ?")) {
                                            abort = false;
                                        }
                                        return abort;
                                    }'
                    ),
                ],
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($reportModel, 'report_date')->widget(DatePicker::className(), [
                'options' => [
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'size' => 'xs',
                    'autoclose' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'responsible_status')->dropDownList($model::getResponsibleStatusList()) ?>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-sm-12">
        <?php
    /*        $forms = [
                [
                    'label' => '<i class="fa fa-file"></i> ' . Html::encode('ประวัติการรายงาน'),
                    'content' => $this->render('_viewMeetingReport', [
                        'row' => \yii\helpers\ArrayHelper::toArray($model->meetingReports),
                    ]),
                ],
            ];
            echo kartik\tabs\TabsX::widget([
                'items' => $forms,
                'position' => kartik\tabs\TabsX::POS_ABOVE,
                'encodeLabels' => false,
                'pluginOptions' => [
                    'bordered' => true,
                    'sideways' => true,
                    'enableCache' => false,
                ],
            ]);
            */?>
        </div>
    </div>-->

    <?= $form->errorSummary($reportModel); ?>

    <div class="form-group">
        <?= Html::submitButton($reportModel->isNewRecord ? 'บันทึก' : 'แก้ไข', ['class' => $reportModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php \yii\bootstrap4\ActiveForm::end(); ?>

</div>
