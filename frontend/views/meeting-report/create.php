<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MeetingReport */

$this->title = 'Create Meeting Report';
$this->params['breadcrumbs'][] = ['label' => 'Meeting Report', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
