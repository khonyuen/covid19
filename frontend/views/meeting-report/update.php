<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $reportModel common\models\MeetingReport */
/* @var $model common\models\MeetingResponsible */

$this->title = 'แก้ไขข้อมูลรายงาน' . ' ';

?>
<div class="row">
    <div class="col-sm-12">
        <div class="meeting-report-update">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'reportModel' => $reportModel,
                'model' => $model,
            ]) ?>

        </div>

    </div>
</div>
