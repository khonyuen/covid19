<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\MeetingReport */

$this->title = $model->report_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting Report', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-report-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting Report'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->report_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->report_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'report_id',
        [
            'attribute' => 'responsible.responsible_id',
            'label' => 'Responsible',
        ],
        'report_detail:ntext',
        'report_date',
        'report_file:ntext',
        'report_images:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MeetingResponsible<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMeetingResponsible = [
        'mission_id',
        'item_id',
        'responsible_status',
        'responsible_user',
        'updated_date',
    ];
    echo DetailView::widget([
        'model' => $model->responsible,
        'attributes' => $gridColumnMeetingResponsible    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->createdBy,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email',
        'title',
        'firstname',
        'lastname',
        'mission_id',
        'position',
        'status',
        'verification_token',
    ];
    echo DetailView::widget([
        'model' => $model->updatedBy,
        'attributes' => $gridColumnUser    ]);
    ?>
    
    <div class="row">
<?php
if($providerMeetingReportComment->totalCount){
    $gridColumnMeetingReportComment = [
        ['class' => 'yii\grid\SerialColumn'],
            'comment_id',
                        'comment_detail:ntext',
            'comment_date',
            'comment_name',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMeetingReportComment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting-report-comment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Meeting Report Comment'),
        ],
        'export' => false,
        'columns' => $gridColumnMeetingReportComment
    ]);
}
?>

    </div>
</div>
