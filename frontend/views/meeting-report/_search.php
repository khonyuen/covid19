<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MeetingReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-meeting-report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'report_id')->textInput(['placeholder' => 'Report']) ?>

    <?= $form->field($model, 'responsible_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\MeetingResponsible::find()->orderBy('responsible_id')->asArray()->all(), 'responsible_id', 'responsible_id'),
        'options' => ['placeholder' => 'Choose Meeting responsible'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'report_detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'report_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Report Date',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'report_file')->textarea(['rows' => 6]) ?>

    <?php /* echo $form->field($model, 'report_images')->textarea(['rows' => 6]) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
